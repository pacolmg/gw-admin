<?php

namespace AppBundle\Twig;

class AppExtension extends \Twig_Extension {

    protected $service;
    protected $beinterface;

    public function __construct($_service, $_beinterface) {
        $this->service = $_service;
        $this->beinterface = $_beinterface;
    }

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('getProjectAvatar', array($this, 'getProjectAvatar')),
            new \Twig_SimpleFilter('getProjectAvatar2', array($this, 'getProjectAvatar2')),
            new \Twig_SimpleFilter('getProjectname', array($this, 'getProjectname')),
            new \Twig_SimpleFilter('getModulename', array($this, 'getModulename')),
            new \Twig_SimpleFilter('getSectionname', array($this, 'getSectionname')),
            new \Twig_SimpleFilter('getSystemusername', array($this, 'getSystemusername')),
            new \Twig_SimpleFilter('getEventname', array($this, 'getEventname')),
            new \Twig_SimpleFilter('utf8', array($this, 'utf8')),
            new \Twig_SimpleFilter('spaceencodeurl', array($this, 'spaceencodeurl')),
            new \Twig_SimpleFilter('spacedecodeurl', array($this, 'spacedecodeurl')),
            new \Twig_SimpleFilter('summarytextbywords', array($this, 'summarytextbywords')),
            new \Twig_SimpleFilter('summarytextbychars', array($this, 'summarytextbychars')),
        );
    }

    public function utf8($string, $decode=false){
        if ($decode) return utf8_decode ($string);
        return utf8_encode($string);
    }
    
    public function getProjectAvatar($id_project, $path = false, $withroot = false) {
        return $this->service->getProjectAvatar($id_project, 0, $path, $withroot);
    }

    public function getProjectAvatar2($id_project, $path = false, $withroot = false) {
        return $this->service->getProjectAvatar($id_project, 1, $path, $withroot);
    }

    public function getProjectname($id_project) {
        return $this->service->getProjectname($id_project);
    }

    public function getSystemusername($id_user, $complete = false) {
        return $this->service->getSystemusername($id_user, $complete);
    }

    public function getModulename($id_module) {
        return $this->service->getModulename($id_module);
    }

    public function getEventname($id_event) {
        return $this->service->getEventname($id_event);
    }

    public function getSectionname($id_section) {
        return $this->service->getSectionname($id_section);
    }

    /*     * ****** public web ********** */

    public function spaceencodeurl($url, $notilde=false) {
        return $this->beinterface->spaceencodeurl($url, $notilde);
    }

    public function spacedecodeurl($url) {
        return $this->beinterface->spacedecodeurl($url);
    }

    public function summarytextbywords($text, $numwords) {
        return $this->beinterface->summarytextbywords($text, $numwords);
    }

    public function summarytextbychars($text, $numchars) {
        return $this->beinterface->summarytextbychars($text, $numchars);
    }

    public function getName() {
        return 'app_extension';
    }

}
