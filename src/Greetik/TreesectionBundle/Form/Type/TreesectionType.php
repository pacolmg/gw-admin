<?php
    namespace Greetik\TreesectionBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of TreesectionType
 *
 * @author Paco
 */
class TreesectionType extends AbstractType{
   
    private $project;
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        
        $this->project = $options['_project'];
        
        if (!$options['_notparent']){
            $builder
                ->add('parent', EntityType::class, array(
                'required' => $options['_uniqueroot'],
                'label' => 'Padre',
                'class' => 'TreesectionBundle:Treesection',
                'empty_data' => (($options['_uniqueroot']) ? '':'Raíz'),
                'choice_label' => 'indentedName',
                'multiple' => false,
                'expanded' => false ,
                'query_builder' => function ($r)
                    {
                        return $r->getChildrenQueryBuilderProject($this->project);
                    }
                ));
            }
            $builder
                ->add('title', TextType::class)
                ->add('body',  TextareaType::class, array('required'=>false))
                ->add('tags', TextType::class, array('required'=>false))
                ->add('metatitle', TextType::class, array('required'=>false))
                ->add('metadescription', TextType::class, array('required'=>false))
                ->add('isprivate')
                ->add('hidemenu')
                ->add('protected')
                ->add('externallink', TextType::class, array('required'=>false))
                ->add('extra', TextType::class, array('required'=>false));
                            
    }
    
    public function getName(){
        return 'Treesection';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\TreesectionBundle\Entity\Treesection',
            '_project' => null,
            '_uniqueroot' => null,
            '_notparent' => false
        ));
    }
    
}

