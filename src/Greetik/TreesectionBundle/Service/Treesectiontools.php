<?php

namespace Greetik\TreesectionBundle\Service;

use Greetik\TreesectionBundle\Entity\Treesection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Treesectiontools {

    private $em;
    private $dataimage;
    private $ytvideo;
    private $interfacetools;

    public function __construct($_entityManager, $_dataimage, $_ytvideo, $_interfacetools) {
        $this->em = $_entityManager;
        $this->dataimage = $_dataimage;
        $this->ytvideo = $_ytvideo;
        $this->interfacetools = $_interfacetools;
    }

    public function getUniqueRoot($idproject, $simple=false) {
        if ($simple){
            $query = $this->em
                    ->createQueryBuilder()
                    ->select('node.id')
                    ->from('TreesectionBundle:Treesection', 'node')
                    ->where('node.parent is NULL AND node.project='.$idproject)
                    ->getQuery()
            ;
            
            $result = $query->getArrayResult();
            if (count($result)>1) throw new \Exception('Hubo un error al extraer el árbol de secciones');
            return $result[0];
        }
        
        //if we want a tree with a unique root we use this method
        $rootnodes = $this->em->getRepository('TreesectionBundle:Treesection')->getRootNodes();
        foreach ($rootnodes as $node)
            if ($node->getProject() == $idproject)
                return $node;

        return '';
    }

    public function moveTreesections($newtreesections, $idproject, $node = '') {
        $root = $this->getUniqueRoot($idproject);
        $move = $this->searchTreesectionsForMove($newtreesections, $root->getId(), $root);

        if ($move == 'parent')
            $this->searchTreesectionsForMove($newtreesections, $root->getId(), $root);

        $this->em->flush();
        //$originaltreesections = $this->getTreesectionsByProject($idproject);
    }

    protected function searchTreesectionsForMove($newtreesections, $oldparent, $root) {
        $i = 0;
        foreach ($newtreesections as $k => $v) {
            //get the original section
            $originaltreesection = $this->getTreesection($v['id']);

            //compare the parents
            if ($originaltreesection->getParent()->getId() != $oldparent) {
                $originaltreesection->setParent($this->getTreesection($oldparent));
                $this->em->persist($originaltreesection);
                return 'parent';
            }

            //
            //compare the position
            $originalposition = $this->getTreesectionPosition($originaltreesection);
            if ($originalposition != $i) {
                if ($originalposition > $i) {
                    $this->em->getRepository('TreesectionBundle:Treesection')->moveUp($originaltreesection, $originalposition - $i);
                } else {
                    $this->em->getRepository('TreesectionBundle:Treesection')->moveDown($originaltreesection, $i - $originalposition);
                }

                $this->em->persist($originaltreesection);
                return 'position';
            }

            if (isset($v['children'])) {
                $move = $this->searchTreesectionsForMove($v['children'], $v['id'], $root);
                if ($move == 'parent')
                    $this->searchTreesectionsForMove($v['children'], $v['id'], $root);
            }

            $i++;
        }


        return 0;
    }

    protected function getTreesectionPosition($treesection) {
        if (is_numeric($treesection))
            $treesection = $this->getTreesection($treesection);

        $treesections = $this->getTreesectionsByProject($treesection->getProject(), $treesection->getParent());

        $i = 0;
        foreach ($treesections as $k => $v) {
            if ($v['id'] == $treesection->getId())
                return $i;
            $i++;
        }

        return $i;
    }

    /*
      public function generateAlltree($sections, $idproject, $node = '', $keepimages=false) {
      //generate new
      $this->recursivegenerateTree($sections);

      //remove old
      $this->removeAllBranch($this->getTreesectionsByProject($idproject, $node), $keepimages);

      $this->recursivesaveTree($sections, $node);
      }
     */

    protected function removeAllBranch($treesection, $keepimages = false) {
        foreach ($treesection as $v) {
            $section = $this->getTreesection($v['id']);
            if (isset($v['__children']) && count($v['__children']) > 0)
                $this->removeAllBranch($v['__children']);

            $this->em->getRepository('TreesectionBundle:Treesection')->removeFromTree($section);
            if ($keepimages !== true)
                $this->deleteVideosandImagesTreeSection($section);
        }
    }

    /*
      protected function cloneTreesection($section) {
      $newsection = new Treesection();
      $newsection->setTitle($section->getTitle());
      $newsection->setBody($section->getBody());
      $newsection->setMetadescription($section->getMetadescription());
      $newsection->setMetatitle($section->getMetatitle());
      $newsection->setTags($section->getTags());
      $newsection->setUser($section->getUser());
      $newsection->setCreatedat($section->getCreatedat());
      $newsection->setProject($section->getProject());
      $newsection->setId($section->getId());

      return $newsection;
      }

      protected function cleanTreesection($section) {
      $newsection = new Treesection();
      $section->setLft($newsection->getLft());
      $section->setRgt($newsection->getRgt());
      $section->setRoot($newsection->getRoot());
      $section->setLevel($newsection->getLevel());
      $section->setParent($newsection->getParent());

      return $section;
      }

      protected function recursivesaveTree($sections, $parent = '') {
      $i = 0;
      foreach ($sections as $section) {
      $newSection = $this->cloneTreesection($section['section']);

      if (!$parent) {
      $this->em->getRepository('TreesectionBundle:Treesection')->persistAsLastChild($newSection);
      } else {
      $this->em->getRepository('TreesectionBundle:Treesection')->persistAsLastChildOf($newSection, $parent);
      }
      //force the id
      $this->em->getClassMetaData(get_class($newSection))->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
      $this->em->flush();

      if (isset($section['children']) && count($section['children']))
      $this->recursivesaveTree($section['children'], $newSection);
      }
      }

      protected function recursivegenerateTree(&$sections) {
      foreach ($sections as $k => $v) {
      $sections[$k]['section'] = $this->getTreesection($v['id']);
      if (isset($v['children']) && count($v['children']) > 0)
      $this->recursivegenerateTree($sections[$k]['children']);
      }
      }
     */

    public function getSimpleTreesectionsByProject($project, $node = '', $withimages = true) {
        $repo = $this->em->getRepository('TreesectionBundle:Treesection');
        $query = $this->em
                ->createQueryBuilder()
                ->select('node.id, node.title, node.level, node.project, node.hidemenu')
                ->from('TreesectionBundle:Treesection', 'node')
                ->orderBy('node.root, node.lft', 'ASC')
                ->where((empty($node)?'node.root = 1':'node.id='.(is_array($node)? $node['id']:$node->getId())).' AND node.project='.$project)
                ->getQuery()
        ;
        
        $result = $query->getArrayResult();
        $completetreesection = $repo->buildTree($result);
        return $completetreesection;        
    }

    public function getTreesectionsByProject($project, $node = '', $withimages = true) {
        $completetreesection = $this->em->getRepository('TreesectionBundle:Treesection')->childrenHierarchy($node);
        foreach ($completetreesection as $keytree => $treesection) {
            if ($treesection['project'] != $project)
                unset($completetreesection[$keytree]);
            //else $completetreesection[$keytree]['name'] = $this->getTreesection ($treesection['id'])->getIndentedName();
        }
        if ($withimages) {
            $this->fillTreesectionImages($completetreesection);
        }
        return $completetreesection;
    }

    protected function fillTreesectionImages(&$completetreesection) {
        foreach ($completetreesection as $k => $treesection) {
            $completetreesection[$k]['images'] = $this->getTreesectionImages($treesection['id'])['files'];
            if (isset($treesection['__children']) && count($treesection['__children']) > 0)
                $this->fillTreesectionImages($completetreesection[$k]['__children']);
        }
    }

    /*
      public function getPublishedTreesections($project=''){
      return $this->em->getRepository('TreesectionBundle:Treesection')->findPublishedTreesections($project);
      }
     */

    public function getTreesection($id) {
        $treesection = $this->em->getRepository('TreesectionBundle:Treesection')->findOneById($id);

        if (!$treesection)
            throw new NotFoundHttpException('No se encuentra la sección');
        $treesection->setYtvideos($this->ytvideo->getVideos($treesection->getId(), 'treesection'));
        $treesection->setImages($this->getTreesectionImages($id));

        return $treesection;
    }

    public function modifyTreesection($treesection) {
        $treesection->setLastedit(new \DateTime());

        $this->em->persist($treesection);
        $this->em->flush();
    }

    public function insertTreesection($treesection, $user, $project, $parent = '') {
        $treesection->setCreatedat(new \DateTime());
        $treesection->setProject($project);
        $treesection->setUser($user);

        if (!$parent)
            $this->em->getRepository('TreesectionBundle:Treesection')->persistAsLastChild($treesection);
        else {
            if (is_numeric($parent)) {
                $parent = $this->getTreesection($parent);
            }
            $this->em->getRepository('TreesectionBundle:Treesection')->persistAsLastChildOf($treesection, $parent);
        }

        //$this->em->getClassMetaData(get_class($treesection))->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $this->em->flush();
    }

    protected function deleteVideosandImagesTreeSection($treesection) {
        if (is_numeric($treesection))
            $treesection = $this->getTreesection($treesection);

        $ytvideos = $this->ytvideo->getVideos($treesection->getId(), 'treesection');
        foreach ($ytvideos as $ytvideo)
            $this->ytvideo->dropVideo($ytvideo->getId());
        $dataimages = $this->dataimage->getImages($treesection->getId(), 'treesection');

        $_SERVER['REQUEST_METHOD'] = 'POST';
        foreach ($dataimages['files'] as $image) {
            $this->dataimage->dropImage($image['id']);
        }
    }

    public function deleteTreesection($treesection) {

        $this->removeAllBranch($treesection);

        $this->deleteVideosandImagesTreeSection($treesection);

        $this->em->remove($treesection);
        $this->em->flush();
    }

    public function getPath($treesection) {
        $path = $this->em->getRepository('TreesectionBundle:Treesection')->getPath($treesection);
        //delete the root
        unset($path[0]);
        return $path;
    }

    public function getTreesectionImages($section) {
        if (!is_numeric($section))
            $section = $section->getId();
        return $this->dataimage->getImages($section, 'treesection');
    }

    public function getTreesectionsForSelect($idproject) {
        return $this->em->getRepository('TreesectionBundle:Treesection')->getChildrenQueryBuilderProject($idproject)->getQuery()->getResult();
    }

    public function getTreesectionDocuments($section) {
        if (!is_numeric($section))
            $section = $section->getId();
        return $this->dataimage->getDocuments($section, 'treesection');
    }

}
