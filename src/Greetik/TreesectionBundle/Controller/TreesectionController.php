<?php

namespace Greetik\TreesectionBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\TreesectionBundle\Entity\Treesection;
use Greetik\TreesectionBundle\Form\Type\TreesectionType;

class TreesectionController extends Controller
{
    
    /**
    * Render all the treesections of the user
    * 
    * @author Pacolmg
    */
    public function indexAction($idproject)
     {
         
         return $this->render('TreesectionBundle:Treesection:index.html.twig', array(
                     'data' => $this->get('treesection.tools')->getTreesectionsByProject($idproject),
                     'insertAllow' => true
                 ));
     }

    /**
    * View an individual treesection, if it doesn't belong to the connected user or doesn't exist launch an exception
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function viewAction($id, $editForm='')
     {
        $treesection = $this->get('treesection.tools')->getTreesection($id);
        if (!$editForm) $editForm = $this->createForm(new TreesectionType(), $treesection);
        return $this->render('TreesectionBundle:Treesection:view.html.twig', array(
                     'item' => $treesection,
                     'new_form' => $editForm->createView(),
                     'option' => $this->getRequest()->get('option'),
                     'modifyAllow' => 1
                 ));
     }

    /**
    * Get the data of a new Treesection by Treesection and persis it
    * 
    * @param Treesection $item is received by Treesection Request
    * @author Pacolmg
    */
     public function insertAction()
     {
        $request = $this->getRequest();
        $treesection=new Treesection();
        $newForm = $this->createForm(new TreesectionType(), $treesection);

         if ($request->getMethod() == "POST") {
             $newForm->bind($request);             
             if ($newForm->isValid()) {
                 $this->get('treesection.tools')->insertTreesection($treesection,1, 1);
                 return $this->redirect($this->generateUrl('treesection_listtreesections'));
             }
         }

         return $this
           ->render('TreesectionBundle:Treesection:insert.html.twig',array('new_form' => $newForm->createView()));
     }

    /**
    * Edit the data of an treesection
    * 
    * @param int $id is received by Get Request
    * @param Treesection $item is received by Treesection Request
    * @author Pacolmg
    */
     public function modifyAction($id){
        $request = $this->getRequest();

        $treesection = $this->get('treesection.tools')->getTreesection($id);
        
        $editForm = $this->createForm(new TreesectionType(), $treesection);
        
        
        if ($request->getMethod() == "POST") {
            $editForm->bind($request);
            if ($editForm->isValid()) {
                $this->get('treesection.tools')->modifyTreesection($treesection);
                return $this->redirect($this->generateUrl('treesection_viewtreesection', array('id'=>$id)));
            }
         }
         return $this->viewAction($id, $editForm);
     }
     
     
      /**
    * Delete a section from the data of a treesection
    * 
    * @param int $id is received by Get Request
    * @param Treesection $item is received by Treesection Request
    */
     public function deleteAction($id, $idproject){

        $treesection = $this->get('gwadmin.treesections')->getTreesection($id);
        $this->get('gwadmin.treesections')->deleteTreesection($treesection);
        return $this->redirect($this->generateUrl('treesection_listtreesections',$idproject));            
     }


}
