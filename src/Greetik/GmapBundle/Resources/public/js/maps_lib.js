(function (window, undefined) {
    var MapsLib = function (options) {
        var self = this;

        options = options || {};

        this.canvas_map = options.canvas_map || "map_canvas";

        this.imagescluster = options.imagescluster || "/bundles/gmap/images/img_cluster/cluster";
        this.urlfiles = options.urlfiles || "/bundles/gmap/files/";

        this.arraymarkers = new Array();
        this.environment = options.environment || "prod";

        this.idproject = options.idproject;
        this.urlgetmarkers = options.urlgetmarkers;
        this.urlgetgpxs = options.urlgetgpxs;
        this.urlmovemarker = options.urlmovemarker;

        this.publicmap = options.publicmap || false;


        this.recordName = options.recordName || "result"; //for showing a count of results
        this.recordNamePlural = options.recordNamePlural || "results";
        this.searchRadius = options.searchRadius || 805; //in meters ~ 1/2 mile


        // appends to all address searches if not present
        this.locationScope = options.locationScope || "Cáceres";

        // zoom level when map is loaded (bigger is more zoomed in)
        this.defaultZoom = options.defaultZoom || 11;

        // center that your map defaults to
        this.map_centroid = new google.maps.LatLng(options.map_center[0], options.map_center[1]);

        // marker image for your searched address
        this.addrMarkerImage = options.addrMarkerImage || 'images/blue-pushpin.png';

        this.currentPinpoint = null;
        $("#result_count").html("");

        this.myOptions = {
            zoom: this.defaultZoom,
            center: this.map_centroid,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.geocoder = new google.maps.Geocoder();
        this.map = new google.maps.Map($("#"+this.canvas_map)[0], this.myOptions);



        google.maps.event.addDomListener(self.map, 'zoom_changed', function () {
            this.changeparamsinterface();
        }.bind(this));
        google.maps.event.addDomListener(self.map, 'maptypeid_changed', function () {
            this.changeparamsinterface();
        }.bind(this));
        google.maps.event.addDomListener(self.map, 'center_changed', function () {
            this.changeparamsinterface();
        }.bind(this));
        // maintains map centerpoint for responsive design
        google.maps.event.addDomListener(self.map, 'idle', function () {
            center = self.map.getCenter();
        });
        google.maps.event.addDomListener(window, 'resize', function () {
            self.map.setCenter(self.map_centroid);
        });

        this.getMarkers();
        this.getGpxs();

        self.searchrecords = null;

    };

    MapsLib.prototype.changeparamsinterface = function () {
        $('#info_zoom').html(this.map.getZoom());
        $('#info_lat').html(this.map.getCenter().lat());
        $('#info_lon').html(this.map.getCenter().lng());
        $('#info_type').html(this.map.getMapTypeId());
    }

    MapsLib.prototype.getMarkers = function () {
        $.post(this.urlgetmarkers, {}, function (r) {
            if (!r) {
                alert('Error Desconocido');
                return;
            }
            if (r.errorCode) {
                alert(r.errorDescription);
                return;
            }

            for (var i in r.data) {
                this.drawMarker(r.data[i]['id'], new google.maps.LatLng(r.data[i]['lat'], r.data[i]['lon']), r.data[i]['title'], r.data[i]['content'], r.data[i]['color']);
            }

        }.bind(this));
    }

    MapsLib.prototype.getGpxs = function () {

        $.post(this.urlgetgpxs, {}, function (r) {
            if (!r) {
                alert('Error Desconocido');
                return;
            }
            if (r.errorCode) {
                alert(r.errorDescription);
                return;
            }

            for (var i in r.data) {
           /*     var ctaLayer = new google.maps.KmlLayer(r.data[i]['pathfile']);
                ctaLayer.setMap(this.map);*/
                this.drawGpx(r.data[i]['pathfile']);
                
            }

        }.bind(this));
    }


    MapsLib.prototype.drawGpx = function (pathfile) {
        self = this;
        $.ajax({
            type: "GET",
            url: pathfile,
            dataType: "xml",
            success: function (xml) {
                var points = [];
                var bounds = new google.maps.LatLngBounds();
                $(xml).find("trkpt").each(function () {
                    var lat = $(this).attr("lat");
                    var lon = $(this).attr("lon");
                    var p = new google.maps.LatLng(lat, lon);
                    points.push(p);
                    bounds.extend(p);
                });

                var poly = new google.maps.Polyline({
                    // use your own style here
                    path: points,
                    strokeColor: "#FF00AA",
                    strokeOpacity: .7,
                    strokeWeight: 4
                });
                poly.setMap(self.map);

            }
        });
        
    }


    MapsLib.prototype.drawMarker = function (id, latlng, title, content, pinColor, pinShadow, pinImage) {

        var infowindow = new google.maps.InfoWindow({
            content: content
        });

        var marker = new google.maps.Marker({
            id: id,
            position: latlng,
            map: this.map,
            title: title,
            content: content,
            draggable: !this.publicmap,
            icon: ((pinColor != '' && typeof (pinColor) != 'undefined') ? this.getPinImage(pinColor) : ((pinImage != '' && typeof (pinImage) != 'undefined') ? pinImage : '')),
            shadow: ((pinShadow) ? this.getPinShadow() : '')
        });


        if (this.publicmap !== true) {
            google.maps.event.addListener(marker, 'dragend', function () {
                $.post(this.urlmovemarker, {'id': marker.id, 'lat': marker.getPosition().lat(), 'lon': marker.getPosition().lng()}, function (r) {
                    if (!r) {
                        alert('Error Desconocido');
                        return;
                    }
                    if (r.errorCode) {
                        alert(r.errorDescription);
                        return;
                    }

                    $("#info_infosave").html("<i class='icon-map-marker'></i> ¡Nueva posición de marcador actualizada!");
                }.bind(this));
            }.bind(this));
        }

        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(this.map, marker);
        });

        return marker;
    }

    MapsLib.prototype.openInfoWindow = function (_idmarker) {
        self = this;

        var marker = self.arraymarkers[_idmarker];

        //center the map
        var bound = new google.maps.LatLngBounds();
        bound.extend(marker.position);
        self.map.setCenter(bound.getCenter());
        self.map.fitBounds(bound);

        var infowindow = new google.maps.InfoWindow({content: marker.content});
        infowindow.open(self.map, marker);
    }

    MapsLib.prototype.getPinImage = function (pinColor) {
        return new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
                new google.maps.Size(21, 34),
                new google.maps.Point(0, 0),
                new google.maps.Point(10, 34));
    }

    MapsLib.prototype.getPinShadow = function () {
        return new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
                new google.maps.Size(40, 37),
                new google.maps.Point(0, 0),
                new google.maps.Point(12, 35));
    }
























    MapsLib.prototype.makeProcesionRoute = function (procesion) {
        self = this;
        $.post(((this.environment == 'dev') ? '/app_dev.php' : '') + '/procesion-semana-santa-de-caceres/' + procesion + '/json', {}, function (data) {

            for (var i in data.points)
                self.makeatrack(data.points[i], data.procesion.fromBuilding, data.procesion.toBuilding, data.procesion.path);
            //alert(data.name);
        }.bind(this));
    }

    MapsLib.prototype.makeReligiousbuildingsmap = function () {
        self = this;
        var bound = new google.maps.LatLngBounds();
        $.post(((this.environment == 'dev') ? '/app_dev.php' : '') + '/templos-y-iglesias-de-caceres/json', {}, function (data) {

            for (var i in data.data) {
                var latlng = new google.maps.LatLng(data.data[i]['lat'], data.data[i]['lng']);
                var marker = self.drawMarker(
                        latlng,
                        data.data[i].name,
                        '<p><strong>' + data.data[i].name + '</strong></p>' + ((data.data[i].images.length > 0) ? '<p><img style="max-width:200px; margin-rigth:auto; margin-left:auto;" src="' + this.urlfiles + 'Religiousbuilding/' + data.data[i].images[0] + '" /></p>' : ''),
                        '762e88',
                        false
                        );
                self.arraymarkers[data.data[i].id] = marker;
                bound.extend(latlng);
            }

            this.map.setCenter(bound.getCenter());
            this.map.fitBounds(bound);


            //set style options for marker clusters (these are the default styles)
            mcOptions = {styles: [{
                        height: 48,
                        url: this.imagescluster + "1.png",
                        width: 48
                    },
                    {
                        height: 56,
                        url: this.imagescluster + "2.png",
                        width: 56
                    },
                    {
                        height: 66,
                        url: this.imagescluster + "3.png",
                        width: 66
                    },
                    {
                        height: 78,
                        url: this.imagescluster + "4.png",
                        width: 78
                    },
                    {
                        height: 90,
                        url: this.imagescluster + "5.png",
                        width: 90
                    }]};

            //init clusterer with your options
            var markerCluster = new MarkerClusterer(self.map, self.arraymarkers, mcOptions);

        }.bind(this));
    }




    MapsLib.prototype.makeatrack = function (arrayPoints, fromB, toB, pathtext) {
        var bound = new google.maps.LatLngBounds();
        var total = arrayPoints.length;

        fromequalto = (typeof (fromB) != 'undefined' && typeof (toB) != 'undefined' && fromB.id == toB.id);

        for (i = 0; i < total; i++) {
            var latlng = new google.maps.LatLng(arrayPoints[i]['lat'], arrayPoints[i]['lng']);
            if (i == 0 && typeof (fromB) != 'undefined') {
                //if (fromequalto) var auxLatlng = new google.maps.LatLng(fromB.lat, fromB.lng);
                //this.drawMarker(((fromequalto)?auxLatlng:latlng), 'Salida', '<p><strong>Salida'+((fromequalto)?' y Llegada':'')+': </strong>'+fromB.name+'</p><p><strong>Itinerario: </strong>'+pathtext+'</p>', '762e88', true)
                latlng = new google.maps.LatLng(fromB.lat, fromB.lng);
                arrayPoints[i]['lat'] = fromB.lat;
                arrayPoints[i]['lng'] = fromB.lng;
                this.drawMarker(latlng, 'Salida', '<p><strong>Salida' + ((fromequalto) ? ' y Llegada' : '') + ': </strong>' + fromB.name + '</p><p><strong>Itinerario: </strong>' + pathtext + '</p>', '762e88', true)
            }
            if (i == eval(total - 1) && typeof (toB) != 'undefined') {
                if (fromequalto) {
                    arrayPoints[i]['lat'] = fromB.lat;
                    arrayPoints[i]['lng'] = fromB.lng;
                } else
                    this.drawMarker(latlng, 'Llegada', '<p><strong>Llegada: </strong>' + toB.name + '</p><p><strong>Itinerario: </strong>' + pathtext + '</p>', 'eb8f00', false)
            }
            bound.extend(latlng);
        }
        this.map.setCenter(bound.getCenter());
        this.map.fitBounds(bound);

        this.makeatrackSimple(arrayPoints);


        //this.makeatrackComplex(arrayPoints);
    }
    MapsLib.prototype.makeatrackSimple = function (arrayPoints) {
        var self = this;


        var arrayLatLngs = new Array();
        var i = 0;
        $.each(arrayPoints, function (index, value) {
            //if (i<3) alert(value['lat']+' '+value['lng']);
            arrayLatLngs.push(new google.maps.LatLng(value['lat'], value['lng']));
            i++;
        });
        this.drawPoliline(arrayLatLngs);

    }

    var contrack = 0;
    MapsLib.prototype.makeatrackComplex = function (arrayPoints) {
        var self = this;

        var multiarray = new Array();
        var i = 0;
        var total = arrayPoints.length;
        var cont = 0;
        while (i < total) {
            var arrayLatLngs = new Array();
            var j = 0;
            for (j = 0; j < 10; j++) {

                value = arrayPoints[i + j];
                if (typeof (value) == 'undefined' || !value) {
                    i = total;
                    break;
                }
                else
                    arrayLatLngs.push(new google.maps.LatLng(value['lat'], value['lng']));

                i++;
            }

            multiarray[cont] = arrayLatLngs;

            setTimeout(function () {
                this.drawTrackLine(multiarray);
            }.bind(this), eval(cont * 1500));

            /*if (cont==8){
             alert(1);
             setTimeout(function(){alert('Hello')},eval(cont*1000));
             }*/

            cont++;
        }


    }



    MapsLib.prototype.drawTrackLine = function (arrayTracks) {
        contrack++;
        if (typeof (arrayTracks[contrack]) == 'undefined')
            return;
        this.drawPoliline(arrayTracks[contrack]);
    }

    MapsLib.prototype.drawPoliline = function (arrayLL) {
        self = this;

        var flightPath = new google.maps.Polyline({
            path: arrayLL,
            geodesic: true,
            strokeColor: '#762e88',
            strokeOpacity: 1.0,
            strokeWeight: 2,
            map: self.map
        });
    }


    MapsLib.prototype.createRoute = function (start, end, points, mode) {
        self = this;

        if (!mode || typeof (mode) == 'undefined') {
            mode = "WALKING";
        }

        directionsService = new google.maps.DirectionsService();

        start = self.arraymarkers[start].position;
        end = self.arraymarkers[end].position;

        var waypts = [];
        for (var i = 0; i < points.length; i++) {
            waypts.push({
                location: self.arraymarkers[points[i]['id']].position,
                stopover: true});
        }

        var travelmode = '';
        switch (mode) {
            case 'DRIVING':
                travelmode = google.maps.TravelMode.DRIVING;
                break;
            default:
                travelmode = google.maps.TravelMode.WALKING;
                break;
        }

        var request = {
            origin: start,
            destination: end,
            waypoints: waypts,
            optimizeWaypoints: true,
            travelMode: travelmode
        };
        directionsService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay = new google.maps.DirectionsRenderer();
                directionsDisplay.setMap(self.map);


                var route = response.routes[0];
                var summaryPanel = document.getElementById('directions_panel');
                summaryPanel.innerHTML = '';

                directionsDisplay.setPanel(summaryPanel);
                directionsDisplay.setDirections(response);
                /* For each route, display summary information.
                 for (var i = 0; i < route.legs.length; i++) {
                 var routeSegment = i + 1;
                 summaryPanel.innerHTML += '<strong>Segmento de la Ruta' + routeSegment + ': </strong><br>';
                 summaryPanel.innerHTML += route.legs[i].start_address + ' a ';
                 summaryPanel.innerHTML += route.legs[i].end_address + '<br>';
                 summaryPanel.innerHTML += route.legs[i].distance.text + '<br><br>';
                 }*/
            }
        }.bind(this));

    }

    //-----custom functions-----
    //-----end of custom functions-----

    if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') {
        module.exports = MapsLib;
    } else if (typeof define === 'function' && define.amd) {
        define(function () {
            return MapsLib;
        });
    } else {
        window.MapsLib = MapsLib;
    }


})(window);