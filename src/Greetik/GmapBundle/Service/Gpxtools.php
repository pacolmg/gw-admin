<?php

namespace Greetik\GmapBundle\Service;
use Greetik\GmapBundle\Entity\Gpx;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */

class Gpxtools {
    
    private $em;
    
    public function __construct($_entityManager)
    {
        $this->em = $_entityManager;
    }

    public function getGpxsByProject($project){
        return $this->em->getRepository('GmapBundle:Gpx')->findByProject($project);
    }
    
    public function getGpx($id){
       $gpx = $this->em->getRepository('GmapBundle:Gpx')->findOneById($id);
     
       if (!$gpx) throw new NotFoundHttpException('No se encuentra el marcador');
       
       return $gpx; 
    }
    
    public function modifyGpx($gpx){       
       $this->em->persist($gpx);
       $this->em->flush();
    }
    
    public function insertGpx($gpx, $project){
        $gpx->setProject($project);
        
        $this->em->persist($gpx);
        $this->em->flush();
    }
    
    
    public function deleteGpx($gpx){
        $this->em->remove($gpx);
        $this->em->flush();
    }
    
}
