<?php

namespace Greetik\GmapBundle\Service;
use Greetik\GmapBundle\Entity\Marker;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */

class Markertools {
    
    private $em;
    
    public function __construct($_entityManager)
    {
        $this->em = $_entityManager;
    }

    public function getMarkersByProject($project){
        return $this->em->getRepository('GmapBundle:Marker')->findByProject($project);
    }
    
    public function getMarker($id){
       $marker = $this->em->getRepository('GmapBundle:Marker')->findOneById($id);
     
       if (!$marker) throw new NotFoundHttpException('No se encuentra el marcador');
       
       return $marker; 
    }
    
    public function modifyMarker($marker){       
       $this->em->persist($marker);
       $this->em->flush();
    }
    
    public function insertMarker($marker, $user, $project){
        $marker->setProject($project);
        
        $this->em->persist($marker);
        $this->em->flush();
    }
    
    
    public function deleteMarker($marker){
        $this->em->remove($marker);
        $this->em->flush();
    }
    
}
