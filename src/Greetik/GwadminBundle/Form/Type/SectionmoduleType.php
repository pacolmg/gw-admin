<?php
    namespace Greetik\GwadminBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SectionmoduleType
 *
 * @author Paco
 */
class SectionmoduleType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        
            $builder
            ->add('module', EntityType::class, array(
                'class' => 'GwadminBundle:Module',
                'choice_label' => 'nameforselect',
                'required'=>true,
                'expanded' => false,
                'multiple' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.id', 'DESC');}));                            
    }
    
    public function getName(){
        return 'Sectionmodule';
    }
    
    public function getDefaultOptions(array $options){
        return array( 'data_class' => 'Greetik\BlogBundle\Entity\Sectionmodule');
    }
}

