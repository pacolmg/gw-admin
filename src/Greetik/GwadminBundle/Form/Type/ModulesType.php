<?php
    namespace Greetik\GwadminBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\Extension\Core\ChoiceList\ChoiceList;
    use Greetik\GwadminBundle\Entity\Moduletype;
    use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModulesType
 *
 * @author Paco
 */
class ModulesType extends AbstractType{
    
    public function __construct() {
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        
            $builder
            ->add('moduletype', EntityType::class, array(
                'class' => 'GwadminBundle:Moduletype',
                'choice_label' => 'name',
                'required'=>true,
                'expanded' => false,
                'multiple' => false,
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('g')
                        ->orderBy('g.id', 'DESC');}))        
            ->add('name');
                            
    }
    
    public function getName(){
        return 'Module';
    }
    
    public function getDefaultOptions(array $options){
        return array( 'data_class' => 'Greetik\BlogBundle\Entity\Module');
    }
}

