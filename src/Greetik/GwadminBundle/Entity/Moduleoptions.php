<?php

namespace Greetik\GwadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Moduleoptions
 *
 * @ORM\Table(name="moduleoptions")
 * @ORM\Entity
 */
class Moduleoptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="namevar", type="string", length=255)
     */
    private $namevar;

    /**
     * @var string
     *
     * @ORM\Column(name="valuevar", type="text")
     */
    private $valuevar;

    /**
    * @ORM\ManyToOne(targetEntity="Module", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="module", referencedColumnName="id", onDelete="CASCADE")
    */
    private $module;    
        

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set namevar
     *
     * @param string $namevar
     *
     * @return Moduleoptions
     */
    public function setNamevar($namevar)
    {
        $this->namevar = $namevar;

        return $this;
    }

    /**
     * Get namevar
     *
     * @return string
     */
    public function getNamevar()
    {
        return $this->namevar;
    }

    /**
     * Set valuevar
     *
     * @param string $valuevar
     *
     * @return Moduleoptions
     */
    public function setValuevar($valuevar)
    {
        $this->valuevar = $valuevar;

        return $this;
    }

    /**
     * Get valuevar
     *
     * @return string
     */
    public function getValuevar()
    {
        return $this->valuevar;
    }

    /**
     * Set module
     *
     * @param \Greetik\GwadminBundle\Entity\Module $module
     *
     * @return Moduleoptions
     */
    public function setModule(\Greetik\GwadminBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \Greetik\GwadminBundle\Entity\Module
     */
    public function getModule()
    {
        return $this->module;
    }
}
