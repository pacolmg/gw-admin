<?php

namespace Greetik\GwadminBundle\Entity;

/**
 * ItemlinkRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ItemlinkRepository extends \Doctrine\ORM\EntityRepository
{
    public function getNumelemsByItem($id, $type) {
        $value = $this->getEntityManager()
                ->createQuery("SELECT COUNT(il) AS num_result FROM GwadminBundle:Itemlink il WHERE il.itemid=:itemid AND il.itemtype=:itemtype")
                ->setParameters(array('itemtype'=>$type, 'itemid'=>$id))
                ->getResult();
        return $value[0]["num_result"];
    }

    public function findOneByNumorderAndElem($numorder, $item_type, $item_id, $id) {
        $query = $this
                ->getEntityManager()
                ->createQuery(
                        "SELECT il FROM GwadminBundle:Itemlink il
             WHERE il.itemid=:itemid AND il.itemtype=:itemtype AND il.numorder=:numorder AND il.id!=:id"
                )
                ->setParameters(array(
            'numorder' => $numorder,
            'itemtype' => $item_type,
            'itemid'=>$item_id,
            'id' => $id)
        );

        return $query->getSingleResult();
    }
 
}
