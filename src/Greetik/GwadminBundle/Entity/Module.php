<?php

namespace Greetik\GwadminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Module
 *
 * @ORM\Table(name="module")
 * @ORM\Entity(repositoryClass="Greetik\GwadminBundle\Entity\ModuleRepository")
 */
class Module
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
    * @ORM\ManyToOne(targetEntity="Moduletype")
    */
    private $moduletype;    
    
    /**
     * @ORM\OneToMany(targetEntity="Moduleoptions", mappedBy="module")
     */
    private $moduleoptions;  
    
    /**
     * @ORM\OneToMany(targetEntity="Sectionmodule", mappedBy="module", cascade={"remove"})
     */
    private $sectionmodules;  
    
    
    private $nameforselect;
    
    public function __construct() {
        $this->moduleoptions = new ArrayCollection();
        $this->sectionmodules = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Module
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set moduletype
     *
     * @param \Greetik\GwadminBundle\Entity\Moduletype $moduletype
     *
     * @return Module
     */
    public function setModuletype(\Greetik\GwadminBundle\Entity\Moduletype $moduletype = null)
    {
        $this->moduletype = $moduletype;

        return $this;
    }

    /**
     * Get moduletype
     *
     * @return \Greetik\GwadminBundle\Entity\Moduletype
     */
    public function getModuletype()
    {
        return $this->moduletype;
    }

    /**
     * Add moduleoption
     *
     * @param \Greetik\GwadminBundle\Entity\Moduleoptions $moduleoption
     *
     * @return Module
     */
    public function addModuleoption(\Greetik\GwadminBundle\Entity\Moduleoptions $moduleoption)
    {
        $this->moduleoptions[] = $moduleoption;

        return $this;
    }

    /**
     * Remove moduleoption
     *
     * @param \Greetik\GwadminBundle\Entity\Moduleoptions $moduleoption
     */
    public function removeModuleoption(\Greetik\GwadminBundle\Entity\Moduleoptions $moduleoption)
    {
        $this->moduleoptions->removeElement($moduleoption);
    }

    /**
     * Get moduleoptions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModuleoptions()
    {
        return $this->moduleoptions;
    }

    /**
     * Add sectionmodule
     *
     * @param \Greetik\GwadminBundle\Entity\Sectionmodule $sectionmodule
     *
     * @return Module
     */
    public function addSectionmodule(\Greetik\GwadminBundle\Entity\Sectionmodule $sectionmodule)
    {
        $this->sectionmodules[] = $sectionmodule;

        return $this;
    }

    /**
     * Remove sectionmodule
     *
     * @param \Greetik\GwadminBundle\Entity\Sectionmodule $sectionmodule
     */
    public function removeSectionmodule(\Greetik\GwadminBundle\Entity\Sectionmodule $sectionmodule)
    {
        $this->sectionmodules->removeElement($sectionmodule);
    }

    /**
     * Get sectionmodules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSectionmodules()
    {
        return $this->sectionmodules;
    }
    
    public function getNameforselect(){
        return $this->name."-".$this->moduletype->getName();
    }
}
