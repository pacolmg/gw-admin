<?php

namespace Greetik\GwadminBundle\Service;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Formfield Tools
 *
 * @author Pacolmg
 */
class Contactforms {
    private $__context;
    private $formfields;
    private $formfieldoptions;
    private $modules;
    private $mailer;
    private $env;
    
    
    public function __construct($_context, $_formfields, $_formfieldoptions, $_modules, $_mailer, $_env){
        $this->__context = $_context;
        $this->formfields = $_formfields;
        $this->formfieldoptions = $_formfieldoptions;
        $this->modules = $_modules;
        $this->mailer = $_mailer;
        $this->env = $_env;
    }
    
    public function getContactformOptions($idmodule){
        $mailto = $this->modules->getOptionModule($idmodule, 'mailto'); if ($mailto) $mailto = $mailto->getValuevar();
        
        return array('mailto'=>$mailto);
    }
    
    public function getFormfieldsByModule($idmodule){
        return $this->formfields->getFormfieldsByProject($idmodule);
    }
    
    public function getFormfield($id){
       $formfield =$this->formfields->getFormfield($id);
       if (!$this->getFormfieldPerm($formfield, 'view')) throw new AccessDeniedException('No tienes permiso para ver el campo');
       
       return $formfield;
    }
    
    public function modifyFormfield($formfield, $oldposition){
        if (!$this->getFormfieldPerm($formfield, 'modify')) throw new AccessDeniedException('No tienes permiso para eliminar el formulario');
        $this->formfields->modifyFormfield($formfield, $oldposition);
    }
    
    public function insertFormfield($formfield, $module){
        if (!$this->getFormfieldPerm($formfield, 'insert')) throw new AccessDeniedException('No tienes permiso para insertar el formulario');
        $this->formfields->insertFormfield($formfield, $module);
    }
    
    public function deleteFormfield($formfield){
        if (!$this->getFormfieldPerm($formfield, 'modify')) throw new AccessDeniedException('No tienes permiso para eliminar el formulario');
        $this->formfields->deleteFormfield($formfield);
    }

    public function getFormfieldPerm($formfield, $type=''){
        if ($type=='view') return true;
        return $this->__context->isGranted('ROLE_ADMIN');
    }     
    
    public function insertFormfieldoption($formfieldoption){
        if (!$this->getFormfieldPerm($formfieldoption->getFormfield(), 'modify')) throw new AccessDeniedException('No tienes permiso para insertar la opción');
        $this->formfieldoptions->insertFormfieldoption($formfieldoption);
    }
    
    public function modifyFormfieldoption($formfieldoption){
        if (!$this->getFormfieldPerm($formfieldoption->getFormfield(), 'modify')) throw new AccessDeniedException('No tienes permiso para insertar la opción');
        $this->formfieldoptions->modifyFormfieldoption($formfieldoption);
    }
    
    public function getFormfieldoption($id){
        $formfieldoption = $this->formfieldoptions->getFormfieldoption($id);
        if (!$this->getFormfieldPerm($formfieldoption->getFormfield(), 'view')) throw new AccessDeniedException('No tienes permiso para acceder a la opción');
        return $formfieldoption;
    }
    
    public function deleteFormfieldoption($formfieldoption){
        if (!$this->getFormfieldPerm($formfieldoption->getFormfield(), 'modify'))
            throw new AccessDeniedException('No tienes permiso para eliminar la opción');
        $this->formfieldoptions->deleteFormfieldoption($formfieldoption);
    }
    
  public function sendmail($id, $params, $userecaptcha=false) {

        if ($userecaptcha==true){
            $recaptcha  = $params->get('g-recaptcha-response');
            $google_url="https://www.google.com/recaptcha/api/siteverify";
            $secret= $this->modules->getOptionModule(null, 'recaptchasecret')->getValuevar();
            $ip=$params->getClientIp();
            $url=$google_url."?secret=".$secret."&response=".$recaptcha."&remoteip=".$ip;
            
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-type: application/json')); // Assuming you're requesting JSON
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            
            $res=curl_exec($ch);
            $res= json_decode($res, true);
            //reCaptcha success check 
            if(!isset($res['success']) || !$res['success']){
                throw new \Exception('Captcha Incorrecto');
            }

        }
        
        $fieldforms = $this->getFormfieldsByModule($id);
        $module = $this->modules->getModule($id);
        $subject = 'Email recibido desde ' . $module->getName();
        $message = 'Desde la url: '.$params->getUri().'<br />'.$subject . ': <br />';

        /* $pos = strpos($params->getSchemeAndHttpHost(), 'http://'); */ $pos = 7;
        //dump($pos);
        $emailfrom = 'info@' . substr($params->getSchemeAndHttpHost(), $pos);
        $value = '';
        foreach ($fieldforms as $fieldform) {
            $value = $params->get('field_' . $fieldform->getId());
            if ($value=='' && $fieldform->getOblig()) throw new \Exception('El campo ' . $fieldform->getName() . ' es obligatorio');
            switch ($fieldform->getFormfieldtype()->getCode()) {
                case 4:
                    $value = (($value == 1) ? 'Sí' : 'No');
                    break;
                case 5:
                    $value = $value->getName();
                    break;

                case 6:
                    $value = '';
                    $values = $params->get('field_' . $fieldform->getId(), array());

                    $i = 0;
                    foreach ($values as $v) {
                        $v = $this->getFormfieldoption($v);
                        $value.=(($value == '') ? '' : ((count($values) == $i + 1) ? ' y ' : ', ')) . $v->getName();
                        $i++;
                    }

                    break;

                case 1: //$emailfrom = $value;
            }

            $message .= '<strong>' . $fieldform->getName() . '</strong>: ' . $value . '<br />';
        }

        $emailto = $this->modules->getOptionModule($id, 'email')->getValuevar();
        
        if (!$emailto || $emailto == '')
            throw new \Exception('No hay configurado ningún email para este formulario de contacto');
        
        if ($this->env=='dev'){
            $swiftmessage = \Swift_Message::newInstance($subject)
            ->setTo($emailto)
            ->setFrom($emailfrom)
            ->setBody($message, 'text/html')
            ;

            $data = $this->mailer->send($swiftmessage);
            if ($data>0) return $data;
        }else{
            // Para enviar correo HTML, la cabecera Content-type debe definirse
            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= "From: ".$emailfrom."\r\n";

            if (mail($emailto, $subject, $message, $headers)) return true;
        }
        
        throw new \Exception('No se pudo enviar el email, póngase en contacto con nosotros para que podamos solventar el error');
         
    }    
    
}
