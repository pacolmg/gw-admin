<?php

namespace Greetik\GwadminBundle\Service;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Greetik\CatalogBundle\Entity\Product;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Product Tools
 *
 * @author Pacolmg
 */
class Catalog {

    private $__context;
    private $catalog;
    private $forms;
    private $sections;
    private $modules;

    public function __construct($_context, $_catalog, $_forms, $_sections, $_modules) {
        $this->__context = $_context;
        $this->catalog = $_catalog;
        $this->forms = $_forms;
        $this->sections = $_sections;
        $this->modules = $_modules;
    }

    public function getFriendlyProductsByProject($project='', $categories = array(), $orderby = '') {
        $data = $this->catalog->getAllProductsByProject($project, $categories);
        $friendlydata = array();
        foreach ($data as $k => $v) {
            $aux = $this->getProductAndData($v->getId());
            array_push($friendlydata, array_merge(array('id' => $v->getId(), 'images' => $this->getProductImages($v)['files']), $aux['fields']));
        }

        if ($orderby) {
            $this->usortByArrayKey($friendlydata, $orderby);
        }


        return $friendlydata;
    }

    public function getProductsByProject($project = '', $page = '', $productperpage = 10) {
        if (empty($page))
            $data = $this->catalog->getAllProductsByProject($project);
        else
            $data = $this->catalog->getProductsByPage($project, $page, $productperpage);

        foreach ($data as $k => $v) {
            $data[$k] = $this->getProductAndData($v->getId());
        }

        return $data;
    }

    public function getCatalogForm($idmodule, $onlyid = false) {
        if ($onlyid)
            return $this->modules->getOptionModule($idmodule, 'catalog_form')->getValuevar();
        return $this->forms->getFormfieldsByModule($this->modules->getOptionModule($idmodule, 'catalog_form')->getValuevar());
    }

    public function getSectionForm($idmodule, $category = '', $onlyid = false) {
        if ($onlyid)
            return $this->modules->getOptionModule($idmodule, 'catalog_section')->getValuevar();
        return $this->sections->getTreesectionsByModule($this->modules->getOptionModule($idmodule, 'catalog_section')->getValuevar(), $category);
    }

    public function getSectionFormForSelect($idmodule) {
        return $this->sections->getTreesectionsForSelect($this->modules->getOptionModule($idmodule, 'catalog_section')->getValuevar());
    }

    public function getAmountOfPublishedProductsByProject($project) {
        return $this->catalog->getNumberOfProductsByProject($project);
    }

    public function getProduct($id) {
        $product = $this->catalog->getProduct($id);
        if (!$this->getProductPerm($product, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver el producto');

        return $product;
    }

    public function getProductAndData($id) {
        $product = $this->getProduct($id);
        $fields = $this->getCatalogForm($product->getProject());

        $fieldsarray = $data = $rawdata = array();

        foreach ($fields as $field)
            $fieldsarray['field_' . $field->getId()] = true;

        $i = 0;
        $data['id'] = $rawdata['id'] = $product->getId();
        $data['category'] = $this->sections->getTreesection($product->getSection())->getTitle();
        $data['categoryid'] = $product->getSection();
        $rawdata['category'] = $product->getSection();
        $data['metatitle'] = $rawdata['metatitle'] = $product->getMetatitle();
        $data['metadescription'] = $rawdata['metadescription'] = $product->getMetadescription();
        $data['tags'] = $rawdata['tags'] = $product->getTags();
        foreach ($product->getProductvalues() as $productvalue) {
            $data['field_' . $productvalue->getProductfield()] = $this->getValueFromProductvalue($productvalue);
            $rawdata['field_' . $productvalue->getProductfield()] = $productvalue->getProductvalue();
            unset($fieldsarray['field_' . $productvalue->getProductField()]);

            if ($i == 0 || $i == 1) {
                $data[(($i == 0) ? 'main' : 'second') . 'field'] = $data['field_' . $productvalue->getProductfield()];
                $rawdata[(($i == 0) ? 'main' : 'second') . 'field'] = $rawdata['field_' . $productvalue->getProductfield()];
            }

            $i++;
        }

        //fill the fields without a row in database
        foreach ($fieldsarray as $k => $v) {
            $data[$k] = $rawdata[$k] = '';
        }

        return array('product' => $product, 'fields' => $data, 'raw_fields' => $rawdata);
    }

    protected function getValueFromProductvalue($productvalue) {
        $field = $this->forms->getFormfield($productvalue->getProductfield());
        $value = $productvalue->getProductvalue();
        switch ($field->getFormfieldtype()->getCode()) {
            case 4: return (($value == 1) ? 'Sí' : 'No');
            case 5: if ($value != '')
                    return $this->forms->getFormfieldoption($value)->getName();
                return '';
            case 6:
                $values = explode(',', $value);
                $value = '';
                $i = 0;
                $totalvalues = count($values);
                foreach ($values as $v) {
                    $value.=(($value == '') ? '' : (($totalvalues == $i + 1) ? ' y ' : ', ')) . ((!$v) ? '' : $this->forms->getFormfieldoption($v)->getName());
                    $i++;
                }
                return $value;
        }

        return $value;
    }

    public function modifyProduct($product, $form) {
        if (!$this->getProductPerm($product, 'modify'))
            throw new AccessDeniedException('No tienes permiso para eliminar el producto');
        if (is_array($form))
            $this->catalog->modifyProduct($product, $this->getDataProductFromArray($product->getProject(), $form));
        else
            $this->catalog->modifyProduct($product, $this->getDataProductFromForm($product->getProject(), $form));
    }

    protected function getDataProductFromForm($idmodule, $form) {
        $fields = $this->getCatalogForm($idmodule);
        $data = array();
        foreach ($fields as $field) {
            if ($field->getFormfieldtype()->getCode() == 6)
                $value = implode(',', $form->get('field_' . $field->getId())->getData());
            else
                $value = $form->get('field_' . $field->getId())->getData();
            $data[] = array('field' => $field->getId(), 'value' => $value);
        }
        return $data;
    }

    protected function getDataProductFromArray($idmodule, $form) {
        $fields = $this->getCatalogForm($idmodule);
        $data = array();
        foreach ($fields as $field) {
            if (!isset($form['field_' . $field->getId()]))
                continue;
            if ($field->getFormfieldtype()->getCode() == 6)
                $value = implode(',', $form['field_' . $field->getId()]);
            else
                $value = $form['field_' . $field->getId()];
            $data[] = array('field' => $field->getId(), 'value' => $value);
        }
        return $data;
    }

    public function insertProduct($product, $idmodule, $form) {
        if (!$this->getProductPerm($product, 'insert'))
            throw new AccessDeniedException('No tienes permiso para insertar el producto');
        $product->setNumorder(0);

        if (is_array($form))
            $this->catalog->insertProduct($product,$idmodule, $this->getDataProductFromArray($idmodule, $form));
        else
            $this->catalog->insertProduct($product, $idmodule, $this->getDataProductFromForm($idmodule, $form));
    }

    public function masivedeleteProduct($ids){
        $data = array('num'=>0, 'warnings'=>'');
        foreach($ids as $id){
            try{
                $this->deleteProduct($this->getProduct($id));
                $data['num']++;
            }catch(\Exception $e){
                $data['warnings'].=(($data['warnings']=='')?'':', ').'No se pudo eliminar el elemento con id '.$id;
            }
        }
        
        return $data;
    }
    
    public function deleteProduct($product) {
        if (!$this->getProductPerm($product, 'modify'))
            throw new AccessDeniedException('No tienes permiso para eliminar el producto');
        $this->catalog->deleteProduct($product);
    }

    public function getProductPerm($product, $type = '') {
        if ($type == 'view')
            return true;
        return $this->__context->isGranted('ROLE_ADMIN');
    }

    public function getProductImages($product) {
        if (!is_numeric($product))
            $product = $product->getId();
        return $this->catalog->getProductImages($product);
    }

    public function getProductDocuments($product) {
        if (!is_numeric($product))
            $product = $product->getId();
        return $this->catalog->getProductDocuments($product);
    }

    private function usortByArrayKey(&$array, $key, $asc = SORT_ASC) {
        $sort_flags = array(SORT_ASC, SORT_DESC);
        if (!in_array($asc, $sort_flags))
            throw new InvalidArgumentException('sort flag only accepts SORT_ASC or SORT_DESC');
        $cmp = function(array $a, array $b) use ($key, $asc, $sort_flags) {
            if (!is_array($key)) { //just one key and sort direction 
                if (!isset($a[$key]) || !isset($b[$key])) {
                    return 0;
                    throw new \Exception('attempting to sort on non-existent keys -> ' . $key);
                }
                if ($a[$key] == $b[$key])
                    return 0;
                return ($asc == SORT_ASC xor $a[$key] < $b[$key]) ? 1 : -1;
            } else { //using multiple keys for sort and sub-sort 
                foreach ($key as $sub_key => $sub_asc) {
                    //array can come as 'sort_key'=>SORT_ASC|SORT_DESC or just 'sort_key', so need to detect which 
                    if (!in_array($sub_asc, $sort_flags)) {
                        $sub_key = $sub_asc;
                        $sub_asc = $asc;
                    }
                    //just like above, except 'continue' in place of return 0 
                    if (!isset($a[$sub_key]) || !isset($b[$sub_key])) {
                        return 0;
                        throw new \Exception('attempting to sort on non-existent keys -> ' . $sub_key);
                    }
                    if ($a[$sub_key] == $b[$sub_key])
                        continue;
                    return ($sub_asc == SORT_ASC xor $a[$sub_key] < $b[$sub_key]) ? 1 : -1;
                }
                return 0;
            }
        };
        @usort($array, $cmp);
    }

    protected function importLine($line, $i, $idmodule, $fields, $totalfields) {
        if (trim($line[0]) == '')
            $inserting = true;
        else if (!is_numeric(trim($line[0])))
            return false;

        if (count($line) != $totalfields + 1)
            throw new \Exception('El número de celdas de la línea ' . $i . ' es incorrecto. Tiene ' . count($line) . ' celdas y se esperaban ' . $totalfields . '.');
        try {
            if (isset($inserting) && $inserting == true) {
                $product = new Product();
                $inserting = true;
            } else
                $product = $this->catalog->getProduct($line[0]);

            if (trim($line[1]) != '')
                $product->setSection($line[1]);
            if (trim($line[3]) != '')
                $product->setMetatitle($line[3]);
            if (trim($line[4]) != '')
                $product->setMetadescription($line[4]);
            if (trim($line[5]) != '')
                $product->setTags($line[5]);

            $data = array();
            $j = 6;
            foreach ($fields as $k => $v) {
                $data['field_' . $v->getId()] = $line[$j];
                $j++;
            }

            if (isset($inserting) && $inserting == true)
                $this->insertProduct($product, $idmodule, $data);
            else
                $this->modifyProduct($product, $data);
        } catch (\Exception $e) {
            //throw $e;
            return false;
        }
        return true;
    }

    public function importcatalog($filename, $idproject) {
        if (!$this->getProductPerm('', 'insert'))
            throw new \Exception('No tiene permiso para hacer esta operación');

        $productfields = $this->getCatalogForm($idproject);
        $totalfields = 6;
        foreach ($productfields as $k => $pf) {
            if ($pf->getFormfieldtype()->getCode() != 11)
                $totalfields++;
            else
                unset($productfields[$k]);
        }

        try {
            $content = file_get_contents($filename);
            if (!$content)
                throw new \Exception('Ha ocurrido un error cargando el fichero, cárguelo de nuevo, por favor.');
        } catch (\Exception $e) {
            throw new \Exception('Ha ocurrido un error cargando el fichero, cárguelo de nuevo, por favor.');
        }

        if (!mb_detect_encoding($content, 'UTF-8', true))
            throw new \Exception('El fichero debe estar guardado en UTF-8');

        $data = array('numimported' => 0, 'warnings' => '');
        $lines = explode("\n", $content);

        $i = 0;
        $j = 0;
        $type = '';
        foreach ($lines as $line) {
            if ($i > 10000)
                throw new \Exception('El límite del fichero son 10000 líneas');

            $i++;
            $fields = explode(';', $line);
            if ($i <= 1)
                continue;

            if (count($fields)<=1) continue;
            
            if (!$this->importLine($fields, $i, $idproject, $productfields, $totalfields))
                $data['warnings'].=$data['warnings'] == "" ? "" : "\n" . "La fila " . $i . " no se pudo importar";
            else
                $data['numimported'] ++;
        }

        //$this->em->flush();

        return $data;
    }

}
