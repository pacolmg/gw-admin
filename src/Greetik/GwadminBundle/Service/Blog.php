<?php

namespace Greetik\GwadminBundle\Service;
use Symfony\Component\Finder\Exception\AccessDeniedException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Post Tools
 *
 * @author Pacolmg
 */
class Blog {
    private $__context;
    private $posts;

    
    public function __construct($_context, $_posts) {
        $this->__context = $_context;
        $this->posts = $_posts;
    }
    
    public function getPostsByProject($project=''){
        return $this->posts->getAllPostsByProject($project);
    }
    
    public function getDifferentTags($project='', $forselect=''){
        return $this->posts->getDifferentTags($project, $forselect);
    }
    
    public function getTagsCloud($project=''){
        return $this->posts->getTagsCloud($project);
    }
    
    public function getPublishedpostsByProject($project='', $page='', $postperpage=10){
        if (empty($page))
            return $this->setPostImages($this->posts->getPublishedPosts($project));
        return $this->setPostImages( $this->posts->getPublishedPostsByPage($project, $page, $postperpage));
    }

    public function getPublishedpostsByTag($tag, $page='', $postperpage=10){
        if (empty($page))
            return $this->setPostImages($this->posts->getPublishedPostsByTag($tag));
        return $this->setPostImages( $this->posts->getPublishedPostsByTagAndPage($tag, $page, $postperpage));
    }
    
    public function getAmountOfPublishedPostsByProject($project){
        return $this->posts->getNumberOfPostsByProject($project);
    }
    
    public function getAmountOfPublishedPostsByTag($tag){
        return $this->posts->getNumberOfPostsByTag($tag);
    }
    
    public function getPostsByExtra($text){
        return $this->posts->getPostsByExtra($text);
    }
    
    protected function setPostImages($posts){
        foreach($posts as $post){
            $post->setDataimages($this->getPostImages($post)['files']);
        }
        
        return $posts;
    }
    
    public function getPost($id){
       $post =$this->posts->getPost($id);
       if (!$this->getPostPerm($post, 'view')) throw new AccessDeniedException('No tienes permiso para ver el post');
      
       return $post;
    }
    
    public function modifyPost($post, $oldtags){
        if (!$this->getPostPerm($post, 'modify')) throw new AccessDeniedException('No tienes permiso para eliminar el post');
        $this->posts->modifyPost($post, $oldtags);
    }
    
    public function insertPost($post, $module, $user){
        if (!$this->getPostPerm($post, 'insert')) throw new AccessDeniedException('No tienes permiso para insertar el post');
        $this->posts->insertPost($post, $user, $module);
    }
    
    public function deletePost($post){
        if (!$this->getPostPerm($post, 'modify')) throw new AccessDeniedException('No tienes permiso para eliminar el post');
        $this->posts->deletePost($post);
    }

    public function getPostPerm($post, $type=''){
        if ($type=='view') return true;
        return $this->__context->isGranted('ROLE_ADMIN');
    }       
    
    public function getPostImages($post){
        if (!is_numeric($post)) $post = $post->getId();
        return $this->posts->getPostImages($post);
    }    
    
    public function getPostDocuments($post){
        if (!is_numeric($post)) $post = $post->getId();
        return $this->posts->getPostDocuments($post);
    }    
}
