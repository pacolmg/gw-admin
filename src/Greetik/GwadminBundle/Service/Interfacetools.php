<?php

namespace Greetik\GwadminBundle\Service;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Debug\Exception\FatalErrorException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Interfacetools
 *
 * @author Paco
 */

class Interfacetools {
    
    private $em;
    private $rootdir;
    private $router;
    private $interfacetools;
    private $posts;
    private $systemusers;
    private $modules;
    private $sections;
    
    public function __construct($_entityManager, $_rootdir, $_router, $_interfacetools,  $_posts, $_systemusers, $_modules, $_treesections)
    {
        $this->em = $_entityManager;
        $this->rootdir = $_rootdir;
        $this->router = $_router;
        $this->interfacetools = $_interfacetools;
        $this->posts = $_posts;
        $this->systemusers = $_systemusers;
        $this->modules = $_modules;
        $this->sections = $_treesections;
    }

   public function getProjectAvatar($id_project, $second='', $path=false, $withroot=false){
       /*
       try{
            $project = $this->modules->getProject($id_project);
            $avatar = ((empty($second) || !$second ) ? $project->getAvatar() : $project->getAvatar2());
       }catch(\Exception $e){
            //if (!$avatar){
                $path=false;
            //}
       }
       if (!isset($avatar) || !$avatar || $avatar == ''){
           $path = false;
           $avatar = '/bundles/gwadmin/images/logo'.((empty($second) || !$second ) ? '' : '2').'.png';
       }*/
       $path=false;
       $avatar = '/bundles/gwadmin/images/logo'.((empty($second) || !$second ) ? '' : '2').'.png';

       return (($withroot!==false)?$this->rootdir.'/../web':'') . (($path!==false)?'/uploads/project/'.$project->getId().'/':'') .  $avatar;
   }

   public function getProjectname($id_project){
       try{
        $project = $this->modules->getProject($id_project);
        $name = $project->getName();
       }catch(\Exception $e){
           $name = 'Greetik Web Admin';
       }
       return $name;
   }   
   
   public function getSystemusername($id_user, $complete=false){
       $user = $this->systemusers->getSystemuser($id_user);
       
       return ($complete) ? $user->getName().' '.$user->getSurname() : $user->getUsername();
   }

   
   public function getPostname($id_post){
       $post = $this->posts->getPost($id_post);
       return $post->getTitle();
   }

   public function getSectionname($id_section){
       $post = $this->sections->getTreesection($id_section);
       return $post->getTitle();
   }

   public function getModulename($id_module){
       $module = $this->modules->getModule($id_module);
       return $module->getName();
   }


}
