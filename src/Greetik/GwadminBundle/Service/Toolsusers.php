<?php

namespace Greetik\GwadminBundle\Service;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User Tools
 *
 * @author Pacolmg
 */
class Toolsusers {
    private $em;
    private $users;
    
    public function __construct($_em, $_users) {
        $this->em  = $_em;
        $this->users = $_users;
    }
    
    public function getAllSystemusers(){
        return $this->users->getAllSystemusers();
    }
    
    public function getUsersByProject($project){
        return $this->users->getSystemusersByProject($project);
    }
    
    public function getAdminsByProject($project){
        return $this->users->getAdminsByProject($project);
    }
    
    public function getSystemuser($id_user){
        $user = $this->users->getSystemuser($id_user);
        
        return $user;
    }
    
    public function getSystemuserPerm($systemuser, $type){
        return $this->users->getSystemuserPerm($type, $systemuser);
    }
    
    public function getUsersByProjectForSelect($project){
        $users = $this->getUsersByProject($project);
        $data = array();
        foreach($users as $user){
            array_push($data,
                    array('id'=>$user->getId(), 'name'=>$user->getUsername()));
        }
        return $data;
    }
    
   
}
