<?php

namespace Greetik\GwadminBundle\Service;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Greetik\TreesectionBundle\Entity\Treesection;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Treesection Tools
 *
 * @author Pacolmg
 */
class Treesections {
    private $__context;
    private $treesections;
    private $modules;
    
    public function __construct($_context, $_treesections, $_modules) {
        $this->__context = $_context;
        $this->treesections = $_treesections;
        $this->modules = $_modules;
    }
    
    
    public function generateAlltree($sections, $idmodule, $keepimages=false){
        if (!$this->getTreesectionPerm('', 'modify', $idmodule)) throw new AccessDeniedException('No tienes permiso para ver la sección');
        
        //$this->treesections->generateAlltree($sections, $idmodule, $this->treesections->getuniqueRoot($idmodule), $keepimages);
        $this->treesections->moveTreesections($sections, $idmodule);
    }
    
    public function getAllTreesections(){
        $modules = $this->modules->getModulesByType(2);
        $data = array();
        foreach($modules as $module){$data = array_merge($data, $this->treesections->getTreesectionsForSelect($module->getId()));}
        
        return $data;
    }
    
    public function getTreesectionsByModule($idmodule, $category='', $formenu=false){
        //if(empty( $category)) $category=$this->treesections->getUniqueRoot($idmodule, $formenu);
        if(empty( $category)) $category=$this->treesections->getUniqueRoot($idmodule);
        if (is_numeric($category)) $category = $this->treesections->getTreesection($category);

        /*
        if ($formenu) $treesections = $this->treesections->getSimpleTreesectionsByProject($idmodule, $category, false);
        else $treesections = $this->treesections->getTreesectionsByProject($idmodule, $category, false);
         */      
        $treesections = $this->treesections->getTreesectionsByProject($idmodule, $category, false);
        
        if ($this->modules->checkIfTreesectionOrFormLinkedToCatalog($this->modules->getModule($idmodule))){
            $this->markTreesectionsAsCatalog($treesections);
        }
        return $treesections;
    }
    
    protected function markTreesectionsAsCatalog(&$treesections){
        foreach($treesections as $k=>$section){
            $treesections[$k]['isacatalog'] = true;
            if (count($section['__children'])){
                $this->markTreesectionsAsCatalog($section['__children']);
            }
        }
    }
    
    public function getTreesectionsForSelect($idmodule){
        return $this->treesections->getTreesectionsForSelect($idmodule);
    }
    
    
    public function getTreesection($id){
       $treesection =$this->treesections->getTreesection($id);
       if (!$this->getTreesectionPerm($treesection, 'view')) throw new AccessDeniedException('No tienes permiso para ver la sección');
       
       return $treesection;
    }
    
    public function modifyTreesection($treesection){
        if (!$this->getTreesectionPerm($treesection, 'modify')) throw new AccessDeniedException('No tienes permiso para eliminar la sección');
        $this->treesections->modifyTreesection($treesection);
    }
    
    public function insertTreesection($treesection, $module, $user){
        if (!$this->getTreesectionPerm($treesection, 'insert')) throw new AccessDeniedException('No tienes permiso para insertar la sección');
        $parent = $treesection->getParent();
        $treesection->setParent(null);
        
        if (!$parent){
            try{
                $parent = $this->treesections->getUniqueRoot();
            }catch(\Exception $e){

             }
             if (!$parent){
                $uniqueroot = new Treesection();
                $uniqueroot->setTitle('Raíz');
                $this->treesections->insertTreesection($uniqueroot, $user, $module, $parent);
                $parent = $this->treesections->getUniqueRoot($module);
             }
            
            
        }
        //dump($parent);
        //throw new AccessDeniedException('No tienes permiso para insertar la sección');
        $this->treesections->insertTreesection($treesection, $user, $module, $parent);
    }
    
    public function deleteTreesection($treesection){
        if (!$this->getTreesectionPerm($treesection, 'delete')) throw new AccessDeniedException('No tienes permiso para eliminar el treesection');
        $this->treesections->deleteTreesection($treesection);
    }

    public function getTreesectionPerm($treesection, $type='', $idmodule=''){
        if ($type=='view') return true;
        if ($type=='delete' && $treesection->getProtected())
            return false;
        return $this->__context->isGranted('ROLE_ADMIN');
    }       
    
    public function getTreesectionImages($section){
        if (!is_numeric($section)) $section = $section->getId();
        return $this->treesections->getTreesectionImages($section);
    }
    
    public function getTreesectionDocuments($section){
        if (!is_numeric($section)) $section = $section->getId();
        return $this->treesections->getTreesectionDocuments($section);
    }
    
    public function getPath($section){
        return $this->treesections->getPath($section);
    }
}
