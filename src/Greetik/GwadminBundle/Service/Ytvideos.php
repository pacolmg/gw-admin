<?php

namespace Greetik\GwadminBundle\Service;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Toolsitems
 *
 * @author Paco
 */

class Ytvideos {
    
    private $_context;
    private $ytvideos;
    private $posts;
    private $treesections;
    private $catalog;
    private $markers;
    
    public function __construct($_context, $_ytvideos, $_posts, $_treesections, $_catalog, $_markers) {
        $this->__context = $_context;
        $this->ytvideos = $_ytvideos;
        $this->posts = $_posts;
        $this->treesections = $_treesections;
        $this->catalog = $_catalog;
        $this->markers = $_markers;
    }
    
    /**
    * Get the videos
    * 
    * @param int $item_id The videos are associated to an item with this id
    * @param string $item_type It's the type of the item that the videos are associated to
    * @return an array with the videos
    * @author Pacolmg
    */    
    public function getVideos($item_id, $item_type){
        switch($item_type){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'modify')) throw new AccessDeniedHttpException('No tienes permiso para modificar el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            case 'product': if (!$this->catalog->getProductPerm($this->catalog->getProduct($item_id), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($item_id), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;
                
            default: throw new AccessDeniedHttpException('No se encuentra el tipo de vídeo');
        }
        
        return $this->ytvideos->getVideos($item_id, $item_type);
    }

  
    /**
    * Get a video
    * 
    * @param int $id The id of the video
    * @return Ytvideo
    * @author Pacolmg
    */    
    public function getVideo($id){
        $video = $this->ytvideos->getVideo($id);
        switch($video->getItemtype()){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($video->getItemid()), 'modify')) throw new AccessDeniedHttpException('No tienes permiso para modificar el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($video->getItemid()), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            case 'product': if (!$this->catalog->getProductPerm($this->catalog->getProduct($video->getItemid()), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($video->getItemid()), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;
                
            default: throw new AccessDeniedHttpException('No se encuentra el tipo de vídeo');
        }
        return $video;
    }
    
    
    /**
    * Persist the new video in the bd and set its order
    * 
    * @param int $url the url of the video
    * @param Ytvideo $ytvideo the video
    * @return string the id of the youtube video
    * @author Pacolmg
    */       
    public function insertVideo($ytvideo, $url, $item_id, $type){
        switch($type){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'modify')) throw new AccessDeniedHttpException('No tienes permiso para modificar el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            case 'product': if (!$this->catalog->getProductPerm($this->catalog->getProduct($item_id), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($item_id), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;
                
            default: throw new AccessDeniedHttpException('No se encuentra el tipo de vídeo');
        }        
        return $this->ytvideos->insertVideo($ytvideo, $url, $item_id, $type);
    }    
    /**
    * Persist the new/modified video in the bd and set its order
    * 
    * @param int $url the url of the video
    * @param Ytvideo $ytvideo the video
    * @return string the id of the youtube video
    * @author Pacolmg
    */       
    public function modifyVideo($ytvideo, $url){
        switch($ytvideo->getItemtype()){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($ytvideo->getItemid()), 'modify')) throw new AccessDeniedHttpException('No tienes permiso para modificar el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($ytvideo->getItemid()), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            case 'product': if (!$this->catalog->getProductPerm($this->catalog->getProduct($ytvideo->getItemid()), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($ytvideo->getItemid()), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;
                
            default: throw new AccessDeniedHttpException('No se encuentra el tipo de imagen');
        }
        
        return $this->ytvideos->modifyVideo($ytvideo,$url);
    }

    /**
    * Remove the video
    * 
    * @param int id the Id of the Ytvideo
    * @author Pacolmg
    */
    public function dropVideo($id){
        $video = $this->getVideo($id);
        return $this->ytvideos->dropVideo($id);
    }

   /**
    * Move an video from its old position to a new position
    * 
    * @param int $id The video
    * @param int $newposition the new position of the video
    * @author Pacolmg
    */    
    public function moveVideo($id, $newposition){
        $video = $this->getVideo($id);
        return $this->ytvideos->moveVideo($id, $newposition);
    }     
    
    /**
    * Order of the videos of an item, from 1 to n
    * 
    * @param int $item_id The images are associated to an item with this id
    * @param string $item_type It's the type of the item that the images are associated to
    * @return true/false
    * @author Pacolmg
    */    
    public function reorderVideos($item_id, $item_type){
        switch($item_type){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'modify')) throw new AccessDeniedHttpException('No tienes permiso para modificar el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            case 'product': if (!$this->catalog->getProductPerm($this->catalog->getProduct($video->getItemid()), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($video->getItemid()), 'modify')) throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;
                
            default: throw new AccessDeniedHttpException('No se encuentra el tipo de imagen');
        }        
        return $this->ytvideos->reorderVideos($item_id, $item_type);
    }
  }
