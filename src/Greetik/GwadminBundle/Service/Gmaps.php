<?php

namespace Greetik\GwadminBundle\Service;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Marker Tools
 *
 * @author Pacolmg
 */
class Gmaps {

    private $__context;
    private $markers;
    private $gpxs;
    private $modules;
    private $rootdir;

    public function __construct($_context, $_markers, $_gpx, $_modules, $_rootdir) {
        $this->__context = $_context;
        $this->markers = $_markers;
        $this->gpxs = $_gpx;
        $this->modules = $_modules;
        $this->rootdir = $_rootdir;
    }

    public function getMapOptions($idmodule) {
        $lat = $this->modules->getOptionModule($idmodule, 'lat');
        if ($lat)
            $lat = $lat->getValuevar();
        $lon = $this->modules->getOptionModule($idmodule, 'lon');
        if ($lon)
            $lon = $lon->getValuevar();
        $zoom = $this->modules->getOptionModule($idmodule, 'zoom');
        if ($zoom)
            $zoom = $zoom->getValuevar();
        $type = $this->modules->getOptionModule($idmodule, 'type');
        if ($type)
            $type = $type->getValuevar();

        return array('lat' => $lat, 'lon' => $lon, 'zoom' => $zoom, 'type' => $type);
    }

    public function getMarkersByModule($idmodule, $format = '') {
        $markers = $this->markers->getMarkersByProject($idmodule);
        if ($format == 'array') {
            foreach ($markers as $marker) {
                $data[] = array(
                    'id' => $marker->getId(),
                    'lat' => $marker->getLat(),
                    'lon' => $marker->getLon(),
                    'title' => $marker->getTitle(),
                    'body' => $marker->getBody(),
                    'content'=> "<h4>".$marker->getTitle(). "</h4> " .$marker->getBody() ,
                    'externalid' => $marker->getExternalid(),
                    'color' => $marker->getColor()
                );
            }
        } else
            $data = $markers;

        return $data;
    }

    public function getMarker($id) {
        $marker = $this->markers->getMarker($id);
        if (!$this->getMarkerPerm($marker, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver el marcador');

        return $marker;
    }

    public function modifyMarker($marker) {
        if (!$this->getMarkerPerm($marker, 'modify'))
            throw new AccessDeniedException('No tienes permiso para eliminar el marcador');
        $this->markers->modifyMarker($marker);
    }

    public function insertMarker($marker, $lat, $lon, $module, $user) {
        $marker->setLat($lat);
        $marker->setLon($lon);
        if (!$this->getMarkerPerm($marker, 'insert'))
            throw new AccessDeniedException('No tienes permiso para insertar el marcador');
        $this->markers->insertMarker($marker, $user, $module);
    }

    public function deleteMarker($marker) {
        if (!$this->getMarkerPerm($marker, 'modify'))
            throw new AccessDeniedException('No tienes permiso para eliminar el marcador');
        $this->markers->deleteMarker($marker);
    }

    public function getMarkerPerm($marker, $type = '') {
        if ($type=='view') return true;
        return $this->__context->isGranted('ROLE_ADMIN');
    }

    public function getGpx($id) {
        $gpx = $this->gpxs->getGpx($id);
        if (!$this->getGpxPerm($gpx, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver el gpx');

        return $gpx;
    }

    public function modifyGpx($gpx) {
        if (!$this->getGpxPerm($gpx, 'modify'))
            throw new AccessDeniedException('No tienes permiso para eliminar el gpx');
        $this->gpxs->modifyGpx($gpx);
    }

    public function insertGpx($gpx, $uploadedfile, $module) {
        if (!$this->getGpxPerm($gpx, 'insert'))
            throw new AccessDeniedException('No tienes permiso para insertar el gpx');

        $auxext = explode('.', $uploadedfile->getClientOriginalName());
        $ext = $auxext[count($auxext) - 1];
        if ($ext != 'gpx')
            throw new \Exception('La extensión es incorrecta');
        $filename = sha1(uniqid(mt_rand(), true)) . '.' . $ext;

        $gpx->setFilename($filename);
        $this->gpxs->insertGpx($gpx, $module);
        
        $path = $this->rootdir . '/../web/uploads/gpx/';

        if (!file_exists($path))
            mkdir($path);
        $path.=$gpx->getId() . '/';
        if (!file_exists($path))
            mkdir($path);

        $uploadedfile->move($path, $filename);
    }

    public function deleteGpx($gpx) {
        if (!$this->getGpxPerm($gpx, 'modify'))
            throw new AccessDeniedException('No tienes permiso para eliminar el gpx');
        $this->gpxs->deleteGpx($gpx);
    }

    public function getGpxPerm($gpx, $type = '') {
        return $this->__context->isGranted('ROLE_ADMIN');
    }

    public function getGpxsByModule($idmodule, $format = '') {
        $gpxs = $this->gpxs->getGpxsByProject($idmodule);
        if ($format == 'array') {
            foreach ($gpxs as $gpx) {
                $data[] = array(
                    'id' => $gpx->getId(),
                    'start' => $gpx->getStart(),
                    'end' => $gpx->getEnd(),
                    'filename' => $gpx->getFilename(),
                    'pathfile' => '/uploads/gpx/'.$gpx->getId().'/'.$gpx->getFilename()
                );
            }
        } else
            $data = $gpxs;

        return $data;
    }

}
