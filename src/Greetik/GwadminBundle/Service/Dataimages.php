<?php

namespace Greetik\GwadminBundle\Service;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Dataimage Tools
 *
 * @author Pacolmg
 */
class Dataimages {

    private $__context;
    private $dataimages;
    private $posts;
    private $treesections;
    private $catalog;
    private $markers;

    public function __construct($_context, $_dataimages, $_posts, $_treesections, $_catalog, $_markers) {
        $this->__context = $_context;
        $this->dataimages = $_dataimages;
        $this->posts = $_posts;
        $this->treesections = $_treesections;
        $this->catalog = $_catalog;
        $this->markers = $_markers;
    }

    public function uploadImage($item_id, $item_type, $filetype = 'image') {
        switch ($item_type) {
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar la sección');
                break;
            case 'product':
                if (intval($item_id) == 0) {
                    if (!$this->catalog->getProductPerm('', 'insert'))
                        throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                }else {
                    if (!$this->catalog->getProductPerm($this->catalog->getProduct($item_id), 'modify'))
                        throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                }
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($item_id), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;
            default: throw new AccesDeniedException('No se encuentra el tipo de imagen');
        }

        return $this->dataimages->uploadImage($item_id, $item_type, $filetype);
    }

    public function getImages($item_id, $item_type, $array_mode = 1, $filetype = "image") {
        switch ($item_type) {
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'view'))
                    throw new AccessDeniedException('No tienes permiso para ver el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'view'))
                    throw new AccessDeniedException('No tienes permiso para modificar la sección');
                break;
            case 'product':
                if (intval($item_id) == 0) {
                    if (!$this->catalog->getProductPerm('', 'insert'))
                        throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                    $images = $this->dataimages->getImages($item_id, $item_type, $array_mode, $filetype);
                    foreach ($images['files'] as $k => $v) {
                        try {
                            $product = $this->catalog->getProductAndData($v['item_id']);
                        } catch (\Exception $e) {
                            $product = array('fields' => array('id' => 0, 'mainfield' => 'Sin Asignar'));
                        }

                        $images['files'][$k]['item'] = array('id' => $product['fields']['id'], 'name' => $product['fields']['mainfield'] . ' (#' . $product['fields']['id'] . ')');
                    }
                    return $images;
                } else {
                    if (!$this->catalog->getProductPerm($this->catalog->getProduct($item_id), 'view'))
                        throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                }
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($item_id), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;
            default: throw new AccesDeniedException('No se encuentra el tipo de las imágenes');
        }

        return $this->dataimages->getImages($item_id, $item_type, $array_mode, $filetype);
    }

    
    public function modifyImage($image, $olditem){
        switch($image->getItemtype()){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($image->getItemid()), 'modify')) throw new \Exception('No tienes permiso para modificar la imagen');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($image->getItemid()), 'modify')) throw new \Exception('No tienes permiso para modificar la imagen');
                break;
            case 'product': if (!$this->catalog->getProductPerm($this->catalog->getProduct($image->getItemid()), 'modify')) throw new \Exception('No tienes permiso para modificar la imagen');
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($image->getItemid()), 'modify')) throw new \Exception('No tienes permiso para modificar la imagen');
                break;
                
            default:  throw new \Exception('No tienes permiso para modificar la imagen');
        }
        if ($olditem != $image->getItemid()){
            $maxorder = $this->dataimages->findMaxNumOrderByElem($image->getItemtype(), $image->getItemid(), $image->getFiletype());
            $this->dataimages->changeDataimageitem($image, $olditem);
            $image->setNumorder($maxorder+1);
        }        
        
        $this->dataimages->modifyImage();
    }
    
    public function getItemImage($image) {
        $item_type = $image->getItemtype();
        $item_id = $image->getItemid();

        switch ($item_type) {
            case 'post': 
                try {
                    $post = $this->posts->getPost($item_id);
                    $post = array('id' => $post->getId(), 'name' => $post->getTitle());
                } catch (\Exception $e) {
                    $post = array('id' => 0, 'name' => 'Sin Asignar');
                }
                return $post;
            case 'treesection':
                try {
                    $section = $this->treesections->getTreesection($item_id);
                    $section = array('id' => $section->getId(), 'name' => $section->getTitle());
                } catch (\Exception $e) {
                    $section = array('id' => 0, 'name' => 'Sin Asignar');
                }
                return $section;
            case 'product':
                try {
                    $product = $this->catalog->getProductAndData($item_id);
                    $product = array('id' => $product['fields']['id'], 'name' => $product['fields']['mainfield']);
                } catch (\Exception $e) {
                    
                    $product = array('id' => 0, 'name' => 'Sin Asignar');
                }
                return $product;
            case 'marker':
                try {
                    $marker = $this->markers->getMarker($item_id);
                    $marker = array('id' => $marker->getId(), 'name' => $marker->getTitle());
                } catch (\Exception $e) {
                    $marker = array('id' => 0, 'name' => 'Sin Asignar');
                }
                return $marker;
            default: throw new AccesDeniedException('No se encuentra el tipo de las imágenes');
        }

        return $image;
    }

    public function dropImage($id) {
        $image = $this->dataimages->getImage($id);
        if (!$image)
            throw new NotFoundHttpException('No se encuentra la imagen');

        switch ($image->getItemtype()) {
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($image->getItemid()), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($image->getItemid()), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar la sección');
                break;
            case 'product': if (!$this->catalog->getProductPerm($this->catalog->getProduct($image->getItemid()), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($image->getItemid()), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;

            default: throw new AccesDeniedException('No se encuentra el tipo de la imagen');
        }

        return $this->dataimages->dropImage($id);
    }

    public function moveImage($id, $newposition, $filetype = '') {
        $image = $this->dataimages->getImage($id);
        if (!$image)
            throw new NotFoundHttpException('No se encuentra la imagen');

        switch ($image->getItemtype()) {
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($image->getItemid()), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($image->getItemid()), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar la sección');
                break;
            case 'product': if (!$this->catalog->getProductPerm($this->catalog->getProduct($image->getItemid()), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el ítem');
                break;
            case 'marker': if (!$this->markers->getMarkerPerm($this->markers->getMarker($image->getItemid()), 'modify'))
                    throw new AccessDeniedException('No tienes permiso para modificar el marcador');
                break;

            default: throw new AccesDeniedException('No se encuentra el tipo de la imagen');
        }

        return $this->dataimages->moveImage($id, $newposition, $filetype);
    }

    public function getDataimagePath($type, $id, $filename, $withroot = 0) {
        return $this->dataimages->getDataimagePath($type, $id, $filename, $withroot);
    }

}
