<?php

namespace Greetik\GwadminBundle\Service;

use Symfony\Component\Finder\Exception\AccessDeniedException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Sectionmodule Tools
 *
 * @author Pacolmg
 */
class Sectionmodules {

    private $em;
    private $__context;
    private $sections;
    private $modules;
    private $beinterface;

    public function __construct($_entityManager, $_context, $_sections, $_modules,$_beinterface) {
        $this->em = $_entityManager;
        $this->__context = $_context;
        $this->sections = $_sections;
        $this->modules = $_modules;
        $this->beinterface= $_beinterface;
    }

    public function getSectionmodulesByModule($module) {
        if (is_numeric($module)) $module = $this->modules->getModule($module);
        
        if (!$this->modules->getModulePerm($module, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver el Módulo');
        return $this->em->getRepository('GwadminBundle:Sectionmodule')->findBy(array('module' => $module));
    }

    public function getSectionsByModule($module) {
        $sectionmodules = $this->getSectionmodulesByModule($module);
        $data = array();
        foreach ($sectionmodules as $sectionmodule) {
            array_push($data, $this->sections->getTreesection($sectionmodule->getSection()));
        }
        return $data;
    }

    public function getSectionmodulesBySection($section) {
        if (!$this->getSectionmodulePerm($section, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver la sección');
        return $this->em->getRepository('GwadminBundle:Sectionmodule')->findBy(array ('section'=>$section), array('numorder'=>'ASC'));
    }

    public function getSectionmodule($id) {
        $sectionmodule = $this->em->getRepository('GwadminBundle:Sectionmodule')->findOneById($id);
        if (!$this->getSectionmodulePerm($sectionmodule, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver la sección');

        return $sectionmodule;
    }

    public function modifySectionmodule($sectionmodule) {
        if ($this->checkifexist($sectionmodule->getModule()->getId(), $sectionmodule->getSection(), $sectionmodule->getId()))
            throw new AccessDeniedException('Ya está asociado el módulo a esa sección');
        if (!$this->getSectionmodulePerm($sectionmodule->getSection(), 'modify'))
            throw new AccessDeniedException('No tienes permiso para editar la sección');
        if ($this->checkIfSectionEqualsToModule($sectionmodule))
            throw new AccessDeniedException('No puedes modificar a la propia sección');
        $this->em->persist($sectionmodule);
        $this->em->flush();
    }

    public function insertSectionmodule($sectionmodule) {
        if ($this->checkifexist($sectionmodule->getModule()->getId(), $sectionmodule->getSection()))
            throw new AccessDeniedException('Ya está asociado el módulo a esa sección');
        if (!$this->getSectionmodulePerm($sectionmodule->getSection(), 'modify'))
            throw new AccessDeniedException('No tienes permiso para editar la sección');
        if ($this->checkIfSectionEqualsToModule($sectionmodule))
            throw new AccessDeniedException('No puedes incluir a la propia sección');
        $sectionmodule->setNumorder($this->em->getRepository('GwadminBundle:Sectionmodule')->getNumelemsBySection($sectionmodule->getSection())+1);
        $this->em->persist($sectionmodule);
        $this->em->flush();
    }

    protected function checkifexist($module, $section, $id = '') {
        $auxsectionmodule = $this->em->getRepository('GwadminBundle:Sectionmodule')->findBy(array('module' => $module, 'section' => $section));
        if (count($auxsectionmodule) > 0) {
            if (!empty($id) && $auxsectionmodule[0]->getId() != $id)
                return true;
        }
        return false;
    }

    public function deleteSectionmodule($sectionmodule) {
        if (!$this->getSectionmodulePerm($sectionmodule->getSection(), 'modify'))
            throw new AccessDeniedException('No tienes permiso para modificar la sección');
        $this->em->remove($sectionmodule);
        $this->em->flush();
        
        $this->beinterface->reorderItemElems($this->getSectionmodulesBySection($sectionmodule->getSection()));
    }

    public function getSectionmodulePerm($section, $type = '') {
        if ($type=='view') return true;
        return $this->__context->isGranted('ROLE_ADMIN');
    }

    protected function checkIfSectionEqualsToModule($sectionmodule) {

        $section = $this->sections->getTreesection($sectionmodule->getSection())->getProject();
        return ($section == $sectionmodule->getModule()->getId());
    }

    public function putupSectionmodule($sectionmodule){
        $this->beinterface->moveItemElem('GwadminBundle:Sectionmodule', $sectionmodule->getId(), $sectionmodule->getNumorder()-2, $sectionmodule->getNumorder(), $this->em->getRepository('GwadminBundle:Sectionmodule')->getNumelemsBySection($sectionmodule->getSection()), '', $sectionmodule->getSection() );
    }
    
    public function putdownSectionmodule($sectionmodule){
        if ($sectionmodule->getNumorder() < $this->em->getRepository('GwadminBundle:Sectionmodule')->getNumelemsBySection($sectionmodule->getSection()))
        $this->beinterface->moveItemElem('GwadminBundle:Sectionmodule', $sectionmodule->getId(), $sectionmodule->getNumorder(), $sectionmodule->getNumorder(), $this->em->getRepository('GwadminBundle:Sectionmodule')->getNumelemsBySection($sectionmodule->getSection()), '', $sectionmodule->getSection() );
    }
    
    
}
