<?php

namespace Greetik\GwadminBundle\Service;

use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Greetik\TreesectionBundle\Entity\Treesection;
use Greetik\GwadminBundle\Entity\Moduleoptions;
use Greetik\GwadminBundle\Entity\Module;
use Greetik\GwadminBundle\Entity\Moduletype;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Module Tools
 *
 * @author Pacolmg
 */
class Modules {

    private $__context;
    private $em;
    private $sections;
    private $posts;
    private $dataimages;

    public function __construct($_context, $_em, $_sections, $_posts, $_dataimages) {
        $this->__context = $_context;
        $this->em = $_em;
        $this->sections = $_sections;
        $this->posts = $_posts;
        $this->dataimages = $_dataimages;
    }

    public function getModules() {
        return $this->em->getRepository('GwadminBundle:Module')->findAll();
    }

    public function getModulesByType($type) {
        if (is_numeric($type))
            $type = $this->getModuletype($type);
        
        $modules = $this->em->getRepository('GwadminBundle:Module')->findByModuletype($type);
        
        if ($type->getType()=='contactform' || $type->getType()=='treesection'){ //avoid sections or forms from catalog
            foreach($modules as $k=>$v){
                if ($this->checkIfTreesectionOrFormLinkedToCatalog($v) != '' ) unset ($modules[$k]);
            }
        }
        
        return $modules;
    }

    public function getModuletypes() {
        return $this->em->getRepository('GwadminBundle:Moduletype')->findAll();
    }

    public function getModuletype($id) {
        $module = $this->em->getRepository('GwadminBundle:Moduletype')->findOneById($id);
        if (!$module)
            throw new NotFoundHttpException('No se encontró el tipo de módulo');
        if (!$this->getModulePerm($module, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver el tipo de módulo');

        return $module;
    }

    public function getModule($id) {
        $module = $this->em->getRepository('GwadminBundle:Module')->findOneById($id);
        if (!$module)
            throw new NotFoundHttpException('No se encontró el módulo');
        if (!$this->getModulePerm($module, 'view'))
            throw new AccessDeniedException('No tienes permiso para ver el módulo');

        return $module;
    }

    public function modifyModule($module, $oldname) {
        if (!$this->getModulePerm($module, 'modify'))
            throw new AccessDeniedException('No tienes permiso para modificar el módulo');
        if ($oldname != $module->getName())
            $this->modifyTreesectionAndContactformModulesNameForCatalog($module);
        $this->em->persist($module);
        $this->em->flush();
    }

    protected function insertTreesectionAndContactformModulesForCatalog($modulecatalog) {
        $name = $modulecatalog->getName();
        $module = new Module();

        $module->setName($name);
        $module->setModuletype($this->getModuletype(2));
        $this->insertModule($module);
        $this->editOptionModule($modulecatalog, 'catalog_section', $module->getId());

        $module = new Module();
        $module->setName($name);
        $module->setModuletype($this->getModuletype(3));
        $this->insertModule($module);
        $this->editOptionModule($modulecatalog, 'catalog_form', $module->getId());
    }

    public function insertModule($module) {
        if (!$this->getModulePerm($module, 'insert'))
            throw new AccessDeniedException('No tienes permiso para insertar el módulo');
        $this->em->persist($module);
        $this->em->flush();

        switch ($module->getModuletype()->getId()) {
            case 2: $root = new Treesection();
                $root->setTitle('Raíz');
                $this->sections->insertTreesection($root, $this->__context->getToken()->getUser()->getId(), $module->getId());
                break;
            case 5: $this->insertTreesectionAndContactformModulesForCatalog($module);
                break;
        }
    }

    public function deleteModule($module, $condition) {
        if (!$this->getModulePerm($module, 'delete'))
            throw new AccessDeniedException('No tienes permiso para eliminar el módulo');
        if (!$condition) {
            if ($this->checkIfTreesectionOrFormLinkedToCatalog($module)!='')
                throw new AccessDeniedException('No tienes permiso para eliminar el módulo');
            if ($module->getModuletype()->getId() == 5)
                $this->deleteTreesectionAndContactformModulesForCatalog($module);
        }
        $this->em->remove($module);
        $this->em->flush();
    }

    public function getOptionModule($module, $varname) {
        if ($module && is_numeric($module))
            $module = $this->getModule($module);
        
        $module = $this->em->getRepository('GwadminBundle:Moduleoptions')->findOneBy(array('module' => $module, 'namevar' => $varname));

        return $module;
    }

    public function editOptionModule($module, $varname, $varvalue) {
        if (is_numeric($module))
            $module = $this->getModule($module);

        $option = $this->getOptionModule($module, $varname);
        if (!$option) {
            $option = new Moduleoptions();
            $option->setModule($module);
            $option->setNamevar($varname);
        }
        $option->setValuevar($varvalue);

        $this->em->persist($option);
        $this->em->flush();
    }

    public function getModulePerm($module, $type = '') {
        if ($type=='view') return true;
        return $this->__context->isGranted('ROLE_ADMIN');
    }

    protected function modifyTreesectionAndContactformModulesNameForCatalog($module) {

        if ($module->getModuletype()->getId() == 5) {
            $id_tree = $this->getOptionModule($module, 'catalog_section')->getValuevar();
            $id_form = $this->getOptionModule($module, 'catalog_form')->getValuevar();

            $option_tree = $this->getModule($id_tree);
            $option_tree->setName($module->getName());

            $option_form = $this->getModule($id_form);
            $option_form->setName($module->getName());
        }
    }

    public function checkIfTreesectionOrFormLinkedToCatalog($module) {
        $idcatalog = '';
        if ($module->getModuleType()->getId() == 2) {
            $idcatalog = $this->em->getRepository('GwadminBundle:Moduleoptions')->findOneBy(array('valuevar' => $module->getId(), 'namevar' => 'catalog_section'));
        }
        else if ($module->getModuleType()->getId() == 3) {
             $idcatalog = $this->em->getRepository('GwadminBundle:Moduleoptions')->findOneBy(array('valuevar' => $module->getId(), 'namevar' => 'catalog_form'));
        }
        
        if ( $idcatalog != NULL && $idcatalog!='') return $idcatalog->getModule()->getId();
        
        return '';
    }

    protected function deleteTreesectionAndContactformModulesForCatalog($module) {

        $id_tree = $this->getOptionModule($module, 'catalog_section')->getValuevar();
        $option_tree = $this->getModule($id_tree);
        
        $this->deleteModule($option_tree, true);

        $id_form = $this->getOptionModule($module, 'catalog_form')->getValuevar();
        $option_form = $this->getModule($id_form);
        $this->deleteModule($option_form, true);
    }

    
    protected function importIdentifyTitle($cell){
        $cell = trim($cell);
        switch($cell){
            case 'SECCIONES': return $cell;
            case 'NOTICIAS': return $cell;
            case 'IMÁGENES': return $cell;
            case 'DOCUMENTOS': return $cell;
        }
        return false;
    }
    
    protected function importLine($line, $type, $i, $edittitle=false){
        if (trim($line[0])=='') return false;
        if (!is_numeric(trim($line[0]))) return false;
        if ($type=='IMÁGENES') $type='DOCUMENTOS';
        
        switch($type){
            case 'SECCIONES': if (count($line)!=6) throw new \Exception('El número de celdas de la línea '.$i.' es incorrecto');
                try{
                    $section = $this->sections->getTreesection($line[0]);
                    if ($edittitle && $line[1]!='') $section->setTitle($line[1]);
                    if ($line[2]!='') $section->setMetatitle($line[2]);
                    if ($line[3]!='') $section->setMetadescription($line[3]);
                    if ($line[4]!='') $section->setTags($line[4]);
                }catch(\Exception $e){
                    return false;
                }
                return true;
            case 'NOTICIAS': if (count($line)!=6) throw new \Exception('El número de celdas de la línea '.$i.' es incorrecto');
                try{
                    $post = $this->posts->getPost($line[0]);
                    if ($edittitle && $line[1]!='') $post->setTitle($line[1]);
                    if ($line[2]!='') $post->setMetatitle($line[2]);
                    if ($line[3]!='') $post->setMetadescription($line[3]);
                    if ($line[4]!=''){
                        $oldtags = $post->getTags();
                        
                        $post->setTags(explode(',', $line[4]));
                        foreach ($tags = $post->getTags() as $tag) {
                            if (!in_array($tag, $oldtags))
                                $this->posts->inserttag($tag, $post->getProject(), true);
                        }
                        foreach ($oldtags as $tag) {
                            if (!in_array($tag, $tags))
                                $this->posts->deletetag($tag, $post->getProject());
                        }
                    }
                }catch(\Exception $e){
                    return false;
                }
                return true;
            case 'DOCUMENTOS': if (count($line)< 3) throw new \Exception('El número de celdas de la línea '.$i.' es incorrecto. Tiene '.count($line).' celdas y se esperaban 3.');
                try{
                    $datadocument = $this->dataimages->getImage(intval($line[0]));
                    if ($datadocument && $line[1]!='') $datadocument->setTitle($line[1]);
                }catch(\Exception $e){
                    return false;
                }
                return true;
        }
        return false;
    }
    public function importweb($filename, $edittitle=false){
        if (!$this->getModulePerm('')) throw new \Exception('No tiene permiso para hacer esta operación');
        
        try{
            $content = file_get_contents($filename);
            if (!$content) throw new \Exception('Ha ocurrido un error cargando el fichero, cárguelo de nuevo, por favor.');
        }catch(\Exception $e){
            throw new \Exception('Ha ocurrido un error cargando el fichero, cárguelo de nuevo, por favor.');
        }
                
        if (!mb_detect_encoding($content, 'UTF-8', true)) throw new \Exception('El fichero debe estar guardado en UTF-8');
        
        $data = array('numimported'=>0, 'warnings'=>'');
        $lines = explode("\n", $content);
        
        $i=0;$j=0;$type='';
        foreach($lines as $line){
            if ($i>10000) throw new \Exception('El límite del fichero son 10000 líneas');
            
            $i++; $j++;
            $fields = explode(';', $line);
            if ($newtype = $this->importIdentifyTitle($fields[0])){
                $j=0;
                $type=$newtype;
            }
            
            if ($j<2) continue;
            
            if (!$this->importLine($fields, $type, $i, $edittitle)) $data['warnings'].=$data['warnings']=="" ? "":"\n" . "La fila ".$i." no se pudo importar";
        }
        
        $this->em->flush();
        
        return $data;
    }
}
