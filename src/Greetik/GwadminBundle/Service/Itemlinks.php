<?php

namespace Greetik\GwadminBundle\Service;

use Symfony\Component\Finder\Exception\AccessDeniedException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Itemlink Tools
 *
 * @author Pacolmg
 */
class Itemlinks {

    private $em;
    private $__context;
    private $treesections;
    private $posts;
    private $beinterface;

    public function __construct($_entityManager, $_context, $_sections, $_posts,$_beinterface) {
        $this->em = $_entityManager;
        $this->__context = $_context;
        $this->treesections = $_sections;
        $this->posts = $_posts;
        $this->beinterface= $_beinterface;
    }

   
    public function getItemlinksByItem($item_id, $item_type) {
        switch($item_type){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'view')) throw new AccessDeniedException('No tienes permiso para ver el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'view')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            default: throw new AccesDeniedException('No se encuentra el tipo de las imágenes');
        }
        
        return $this->em->getRepository('GwadminBundle:Itemlink')->findBy(array('itemtype'=>$item_type, 'itemid'=>$item_id), array('numorder'=>'ASC'));
    }

    public function getItemlink($id) {
            return $this->em->getRepository('GwadminBundle:Itemlink')->findOneById($id);
    }

    public function getItemlinkByLink($link) {
            return $this->em->getRepository('GwadminBundle:Itemlink')->findOneByLink($link);
    }

    public function modifyItemlink($itemlink) {
        $item_type = $itemlink->getItemtype();
        $item_id = $itemlink->getItemid();
        
        switch($item_type){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'view')) throw new AccessDeniedException('No tienes permiso para ver el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'view')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            default: throw new AccesDeniedException('No se encuentra el tipo de las imágenes');
        }

        $this->em->persist($itemlink);
        $this->em->flush();
    }

    public function insertItemlink($itemlink) {
        $item_type = $itemlink->getItemtype();
        $item_id = $itemlink->getItemid();
        
        switch($item_type){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'view')) throw new AccessDeniedException('No tienes permiso para ver el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'view')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            default: throw new AccesDeniedException('No se encuentra el tipo de las imágenes');
        }

        $itemlink->setNumorder($this->em->getRepository('GwadminBundle:Itemlink')->getNumelemsByItem($itemlink->getItemid(), $itemlink->getItemtype())+1);
        $this->em->persist($itemlink);
        $this->em->flush();
    }

    public function deleteItemlink($itemlink) {
        $item_type = $itemlink->getItemtype();
        $item_id = $itemlink->getItemid();
        
        switch($item_type){
            case 'post': if (!$this->posts->getPostPerm($this->posts->getPost($item_id), 'view')) throw new AccessDeniedException('No tienes permiso para ver el post');
                break;
            case 'treesection': if (!$this->treesections->getTreesectionPerm($this->treesections->getTreesection($item_id), 'view')) throw new AccessDeniedException('No tienes permiso para modificar el recinto');
                break;
            default: throw new AccesDeniedException('No se encuentra el tipo de las imágenes');
        }
        $this->em->remove($itemlink);
        $this->em->flush();
        
        $this->beinterface->reorderItemElems($this->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()));
    }


    public function putupItemlink($itemlink){
        $this->beinterface->moveItemElem('GwadminBundle:Itemlink', $itemlink->getId(), $itemlink->getNumorder()-2, $itemlink->getNumorder(), $this->em->getRepository('GwadminBundle:Itemlink')->getNumelemsByItem($itemlink->getItemid(), $itemlink->getItemtype()), $itemlink->getItemtype(), $itemlink->getItemid() );
    }
    
    public function putdownItemlink($itemlink){
        if ($itemlink->getNumorder() < $this->em->getRepository('GwadminBundle:Itemlink')->getNumelemsByItem($itemlink->getItemid(), $itemlink->getItemtype()))
        $this->beinterface->moveItemElem('GwadminBundle:Itemlink', $itemlink->getId(), $itemlink->getNumorder(), $itemlink->getNumorder(), $this->em->getRepository('GwadminBundle:Itemlink')->getNumelemsByItem($itemlink->getItemid(), $itemlink->getItemtype()), $itemlink->getItemtype(), $itemlink->getItemid() );
    }
    
    
}
