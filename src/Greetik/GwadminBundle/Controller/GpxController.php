<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\GmapBundle\Entity\Gpx;
use Greetik\GmapBundle\Form\Type\GpxType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class GpxController extends Controller {

    public function getgpxsAction($idproject) {
        return new Response(json_encode(array('errorCode' => 0, 'data' => $this->get('gwadmin.gmaps')->getGpxsByModule($idproject, 'array'))), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * View an individual gpx, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Jaime
     */
    public function viewAction($id, $editForm = '') {
        $gpx = $this->get('gwadmin.gmaps')->getGpx($id);
        if (!$editForm)
            $editForm = $this->createForm(GpxType::class, $gpx);
        return $this->render('GmapBundle:Gpx:view.html.twig', array(
                    'item' => $gpx,
                    'new_form' => $editForm->createView(),
                    'modifyAllow' => $this->get('gwadmin.gmaps')->getGpxPerm($gpx, 'modify')
        ));
    }

    /**
     * Get the data of a new Gpx by Gpx and persis it
     * 
     * @param Gpx $item is received by Gpx Request
     * @author Jaime
     */
    public function insertAction(Request $request, $idproject) {
        $gpx = new Gpx();
        $newForm = $this->createForm(GpxType::class, $gpx);

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    //    dump($request->files->all()['Gpx']['filename']);
                    $this->get('gwadmin.gmaps')->insertGpx($gpx, $request->files->all()['Gpx']['filename'], $idproject);
                } catch (\Exception $e) {
                    $newForm->addError(new FormError($e->getMessage()));
                    return $this->render('GmapBundle:Gpx:insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $idproject)));
            }
        }

        return $this->render('GmapBundle:Gpx:insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an Gpx
     * 
     * @param int $id is received by Get Request
     * @param Gpx $item is received by Gpx Request
     * @author Jaime
     */
    public function modifyAction(Request $request, $id) {
        $gpx = $this->get('gwadmin.gmaps')->getGpx($id);

        $editForm = $this->createForm(GpxType::class, $gpx);


        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get('gwadmin.gmaps')->modifyGpx($gpx);
                } catch (\Exception $e) {
                    $editForm->addError(new FormError($e->getMessage()));
                    return $this->viewAction($id, $editForm);
                }
                return $this->redirect($this->generateUrl('gmap_viewgpx', array('id' => $id)));
            }
        }
        return $this->viewAction($id, $editForm);
    }

    public function deleteAction(Request $request, $id) {
        $gpx = $this->get('gwadmin.gmaps')->getGpx($id);
        if ($request->getMethod() == "POST") {
            $this->get("gwadmin.gmaps")->deleteGpx($gpx);
            return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $gpx->getProject())));
        }
        return $this->viewAction($id);
    }

}
