<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\CatalogBundle\Entity\Product;
use Greetik\CatalogBundle\Form\Type\ProductType;
use Greetik\CatalogBundle\Form\Type\ViewfieldsType;
use Symfony\Component\HttpFoundation\Response;
use Greetik\CatalogBundle\Form\Type\ImportcatalogType;
use Symfony\Component\HttpFoundation\Request;

class CatalogController extends Controller {
    /* Export the items to a csv */

    public function exportcatalogAction(Request $request, $idproject) {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv; charset="UTF-8"');
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $this->get('beinterface.tools')->spaceencodeurl($this->get('gwadmin.toolsinterface')->getModulename($idproject), true) . '.csv"');
        $response->headers->set('Pragma', "no-cache");
        $response->headers->set('Expires', "0");
        $response->headers->set('Content-Transfer-Encoding', "binary");

        return $this->render('CatalogBundle:Import:export.csv.twig', array(
                    'data' => $this->get('gwadmin.catalog')->getFriendlyProductsByProject($idproject),
                    'productfields' => $this->get('gwadmin.catalog')->getCatalogForm($idproject),
                    'hostname' => $request->getSchemeAndHttpHost()
                        ), $response);
    }

    /*render form to import the catalog*/
    public function importcatalogformAction($idproject) {
        $formcsv = $this->createForm(ImportcatalogType::class);

        return $this->render('CatalogBundle:Import:index.html.twig', array(
                    'new_form' => $formcsv->createView(),
                    'idproject' => $idproject
        ));
    }    
    
    /* import the catalog from a csv */

    public function importcatalogAction(Request $request, $idproject) {

        $formcsv = $this->createForm(ImportcatalogType::class);
        $formcsv->handleRequest($request);
        
        try {
            if (count($request->files) <= 0 || !$request->files->get('importcatalog')) {
                $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error cargando fichero 1, cárguelo de nuevo, por favor ')));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $data = $this->get('gwadmin.catalog')->importcatalog($request->files->get('importcatalog')['csv'], $idproject);
            $response = new Response(json_encode(array('errorCode' => 0, 'data' => $data)));
        } catch (\Exception $e) {
            $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() /*. ' (' . $e->getFile().' - '. $e->getLine() . ')'*/)));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }    

    
    /**
     * Render all the products of the user
     * 
     * @author Pacolmg
     */
    public function indexAction($idproject) {
        $productfields = $this->get('gwadmin.catalog')->getCatalogForm($idproject);
        $fieldstoview = $this->get('gwadmin.modules')->getOptionModule($idproject, 'catalog_fieldstoview');
        if ($fieldstoview)
            $fieldstoview = explode(',', $fieldstoview->getValuevar());
        else
            $fieldstoview = array();

        $productfieldstoview = array();
        foreach ($productfields as $pf) {
            if (in_array($pf->getId(), $fieldstoview))
                array_push($productfieldstoview, $pf);
        }
        

        return $this->render('CatalogBundle:Product:index.html.twig', array(
                    'data' => $this->get('gwadmin.catalog')->getFriendlyProductsByProject($idproject),
                    'idproject' => $idproject,
                    'productfields' => $productfieldstoview,
                    'insertAllow' => $this->get('gwadmin.catalog')->getProductPerm('', 'insert'),
                    'formid' => $this->get('gwadmin.catalog')->getCatalogForm($idproject, true),
                    'treesectionid' => $this->get('gwadmin.catalog')->getSectionForm($idproject, '', true),
                    'new_form' => $this->createForm(ViewfieldsType::class, null, array('_formfields'=>$productfields, '_values'=>$fieldstoview))->createView()
        ));
    }

    /**
     * View an individual product, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id, $editForm = '') {
        $product = $this->get('gwadmin.catalog')->getProductAndData($id);
        $productfields = $this->get('gwadmin.catalog')->getCatalogForm($product['product']->getProject());
        if (!$editForm)
            $editForm = $this->createForm(ProductType::class, $product['product'], array('_formfields'=>$productfields, '_sections'=>$this->get('gwadmin.catalog')->getSectionFormForSelect($product['product']->getProject()), '_values'=>$product['raw_fields']));

        return $this->render('CatalogBundle:Product:view.html.twig', array(
                    'item' => $product,
                    'new_form' => $editForm->createView(),
                    'option' => '',
                    'productfields' => $productfields,
                    'modifyAllow' => $this->get('gwadmin.catalog')->getProductPerm($product['product'], 'modify')
        ));
    }

    
    /**
     * View an individual product, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function imagesAction($idproject) {
        
        return $this->render('CatalogBundle:Images:view.html.twig', array( 'idproject' => $idproject,
                'modifyAllow' => true
        ));
    }

    
    /**
     * Get the data of a new Product by Product and persis it
     * 
     * @param Product $item is received by Product Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject) {
        $product = new Product();
        $productfields = $this->get('gwadmin.catalog')->getCatalogForm($idproject);

        $newForm = $this->createForm(ProductType::class, $product, array('_formfields'=>$productfields, '_sections'=>$this->get('gwadmin.catalog')->getSectionFormForSelect($idproject)));
        
        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {

                    $this->get('gwadmin.catalog')->insertProduct($product, $idproject, $newForm);
                } catch (\Exception $e) {
                    $this->get('session')->getFlashBag()->set('error', $e->getMessage());
                    return $this->render('CatalogBundle:Product:insert.html.twig', array('idproject' => $idproject, 'productfields' => $productfields, 'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('catalog_viewcatalog', array('idproject' => $idproject)));
            }
        }

        return $this->render('CatalogBundle:Product:insert.html.twig', array('idproject' => $idproject, 'productfields' => $productfields, 'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an product
     * 
     * @param int $id is received by Get Request
     * @param Product $item is received by Product Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {

        $product = $this->get('gwadmin.catalog')->getProduct($id);

        $editForm = $this->createForm(ProductType::class, 
                $product, array(
            '_formfields'=>$this->get('gwadmin.catalog')->getCatalogForm($product->getProject()), 
            '_sections'=>$this->get('gwadmin.catalog')->getSectionFormForSelect($product->getProject())));
                

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get('gwadmin.catalog')->modifyProduct($product, $editForm);
                } catch (\Exception $e) {
                    $this->get('session')->getFlashBag()->set('error', $e->getMessage());
                    return $this->viewAction($id, $editForm);
                }
                return $this->redirect($this->generateUrl('catalog_viewproduct', array('id' => $id)));
            }
        }
        return $this->viewAction($id, $editForm);
    }

    public function deleteAction($id) {
        $product = $this->get('gwadmin.catalog')->getProduct($id);
        try{
            $this->get("gwadmin.catalog")->deleteProduct($product);
            $this->addFlash('success', 'Eliminado con éxito');
        }catch(\Exception $e){
            $this->addFlash('error', $e->getMessage());
        }            
        return $this->redirect($this->generateUrl('catalog_viewcatalog', array('idproject' => $product->getProject())));
    }
    
    public function masivedeleteAction(Request $request, $idproject) {
        $ids = $request->get('ids');
                
        try{
            $data = $this->get("gwadmin.catalog")->masivedeleteProduct($ids);
            return new Response(json_encode(array('errorCode' => 0, 'warning'=>$data['warnings'],'data' => $data)), 200, array('Content-Type' => 'application/json'));
        }catch(\Exception $e){
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage().' - '.$e->getLine().' - '.$e->getFile() )), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido' )), 200, array('Content-Type' => 'application/json'));
    }

    public function modifyfieldstoviewAction(Request $request, $idproject) {

        $editForm = $this->createForm(ViewfieldsType::class, null, array('_formfields'=>$this->get('gwadmin.catalog')->getCatalogForm($idproject)));

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get('gwadmin.modules')->editOptionModule($idproject, 'catalog_fieldstoview', implode(',', $editForm->get('fieldstoview')->getData()));
                } catch (\Exception $e) {
                    $this->get('session')->getFlashBag()->set('error', $e->getMessage());
                }
            }
        }
        return $this->redirect($this->generateUrl('catalog_viewcatalog', array('idproject' => $idproject)));
    }

}
