<?php
namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\GwadminBundle\Entity\Itemlink;
use Greetik\GwadminBundle\Form\Type\ItemlinkType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Manage the itemlinks defined for item
 *
 * @author Pacolmg
 */
class ItemlinkController extends Controller{
    /**
    * Show the new link insert form
    * 
    * @author Pacolmg
    */
     public function insertformAction($item_id, $item_type, $id='')
     {
        
         if (!empty($id)){$itemlink=$this->get('gwadmin.itemlinks')->getItemlink($id);}
         else{
            $itemlink=new Itemlink();
         }
         
         $newForm = $this->createForm(ItemlinkType::class);

        return $this->render('GwadminBundle:Itemlink:insert.html.twig',array('new_form' => $newForm->createView(), 'item_id' => $item_id, 'item_type'=>$item_type,'id' =>$id));
    }

    /**
    * Edit the data of an itemlink or insert a new one
    * 
    * @param int $id is received by Get Request
    * @param Itemlink $item is received by Post Request
    * @author Pacolmg
    */
     public function insertmodifyAction(Request $request, $item_id, $item_type, $id=''){
        $item = $request->get('Itemlink');
      
        if (!empty($id)){
            $itemlink=$this->get('gwadmin.itemlinks')->getItemlink($id);
            $editing = true;
        }else{ $itemlink = new Itemlink(); $editing=false;}

         $id_project = $this->getUser()->getProject();
         
         $editForm = $this->createForm(ItemlinkType::class, $itemlink);
         $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            if ($editing){
                try{
                    $this->get('gwadmin.itemlinks')->modifyItemlink($itemlink);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }
            }else{
                try{
                    $itemlink->setItemtype($item_type);
                    $itemlink->setItemid($item_id);
                    //$itemlink->setNumorder(0);
                    $this->get('gwadmin.itemlinks')->insertItemlink($itemlink);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }                
            }
            
            return $this->render('GwadminBundle:Itemlink:index.html.twig', array('itemlinks' => $this->get('gwadmin.itemlinks')->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()), '_itemid'=>$itemlink->getItemid(), '_itemtype'=>$itemlink->getItemtype(), 'modifyAllow'=>true));
        }else{
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$errors)), 200, array('Content-Type'=>'application/json'));
        }
         return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Error Desconocido')), 200, array('Content-Type'=>'application/json'));
     }

     /**
    * Delete a itemlink
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function dropAction($id)
     {
        $itemlink = $this->get('gwadmin.itemlinks')->getItemlink($id);
        
       try{
        $this->get('gwadmin.itemlinks')->deleteItemlink($itemlink);
       }catch(\Exception $e){
           return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
       }
       
        return $this->render('GwadminBundle:Itemlink:index.html.twig', array('itemlinks' => $this->get('gwadmin.itemlinks')->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()), '_itemid'=>$itemlink->getItemid(), '_itemtype'=>$itemlink->getItemtype(), 'modifyAllow'=>true));
     }
     
     public function putupAction(Request $request){
        
         $itemlink=$this->get('gwadmin.itemlinks')->getItemlink($request->get('id'));
         try{
            $this->get('gwadmin.itemlinks')->putupItemlink($itemlink);
         }catch(\Exception $e){
             return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
         }
         return $this->render('GwadminBundle:Itemlink:index.html.twig', array('itemlinks' => $this->get('gwadmin.itemlinks')->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()), '_itemid'=>$itemlink->getItemid(), '_itemtype'=>$itemlink->getItemtype(), 'modifyAllow'=>true));
     }
     
     public function putdownAction(Request $request){
         $itemlink=$this->get('gwadmin.itemlinks')->getItemlink($request->get('id'));
         try{
            $this->get('gwadmin.itemlinks')->putdownItemlink($itemlink);
         }catch(\Exception $e){
             return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
         }
          return $this->render('GwadminBundle:Itemlink:index.html.twig', array('itemlinks' => $this->get('gwadmin.itemlinks')->getItemlinksByItem($itemlink->getItemid(), $itemlink->getItemtype()), '_itemid'=>$itemlink->getItemid(), '_itemtype'=>$itemlink->getItemtype(), 'modifyAllow'=>true));
     }
     
}
