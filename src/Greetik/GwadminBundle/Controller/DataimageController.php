<?php
namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use UploadHandler;
use Greetik\DataimageBundle\Form\Type\DataimageType;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Controller to upload images using the jquery-upload blueimp
 *
 * @author Paco
 */
class DataimageController extends controller{
    
    public function indexAction($type, $id, $filetype="image"){
        //use the interface
            return $this->render('DataimageBundle:Dataimage:index.html.twig', array(
                     'configFiles'=>array('modifyAllow'=>true, 'id'=>$id, 'type'=>$type, 'filetype'=>$filetype)
                 ));        
    }

    
    /**
    * If the method is POST, upload a new image. If it's GET get all the images from the id pass by parameter
    * 
    * @param int $id is received by Get Request and it's the id of the item that the image is associated to
    * @param string $type is received by Get Request and it's the type of the item that the image is associated to
    * @return the data of the uploaded image, or a images array
    * @author Pacolmg
    */
    public function uploadAction(Request $request, $type, $id, $filetype="image"){
        if ($request->getMethod() == "POST") {
           return new Response(json_encode($this->get('gwadmin.dataimages')->uploadImage($id, $type, $filetype)), 200, array('Content-Type'=>'application/json'));
        }else {
             if ($request->getMethod() == "GET") return new Response(json_encode($this->get('gwadmin.dataimages')->getImages($id, $type, 1, $filetype)), 200, array('Content-Type'=>'application/json'));        
        }
         return new Response(json_encode(array('files'=>array())), 200, array('Content-Type'=>'application/json'));
    }
 
    /**
    * Delete an image
    * 
    * @param int $id It's the id of the image
    * @author Pacolmg
    */
    public function dropAction(Request $request){
        if ($request->getMethod() != "POST") return new Response(json_encode(array( 'error'=>'No puede hacer esta operación')), 200, array('Content-Type'=>'application/json'));
        if (!$request->get('id')) return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'No se encontró la imagen')), 200, array('Content-Type'=>'application/json'));
        return new Response(json_encode($this->get('gwadmin.dataimages')->dropImage($request->get('id'))), 200, array('Content-Type'=>'application/json'));         
    }
      

    /**
    * Show the edit image form
    * 
    * @author Pacolmg
    */
     public function modifyformAction(Request $request)
     {
        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) $image=$em->getRepository('DataimageBundle:Dataimage')->findOneById($request->get('id'));
        else $image=new Dataimage();
        
        $newForm = $this->createForm(DataimageType::class, $image);

        return $this->render('DataimageBundle:Dataimage:insert.html.twig',array('new_form' => $newForm->createView(),'id' => $request->get('id'), 'linkfield'=>$image->getFiletype() == 'image' ));
     }
     
     
    /**
    * Show the edit image form with a selector to change the item
    * 
    * @author Pacolmg
    */
     public function modifymoveformAction(Request $request, $id, $idproject='')
     {
        $em = $this->getDoctrine()->getManager();

        if ($request->get('id')) $image=$em->getRepository('DataimageBundle:Dataimage')->findOneById($request->get('id'));
        else $image=new Dataimage();
        
        $data = $this->getData($image);
        $newForm = $this->createForm(DataimageType::class, $image, array('_items'=>$data));

        return $this->render('DataimageBundle:Dataimage:insert.html.twig',array('new_form' => $newForm->createView(),'id' => $request->get('id'), 'linkfield'=>$image->getFiletype() == 'image' ));
     }
     
     
     protected function getData($image){
        switch($image->getItemtype()){
            case 'product': $products = $this->get('gwadmin.catalog')->getFriendlyProductsByProject();
                    $data = array();
                    foreach($products as $k=>$v){
                        $data[$v['mainfield']] = $v['id'];
                    }
                 break;
            default: throw new \Exception('Comando equivocado');
        }         
        
        return $data;
     }
   /**
    * Edit the data of image
    * 
    * @param int $id is received by Get Request
    * @param Image $item is received by Post Request
    * @author Pacolmg
    */
     public function modifyAction(Request $request){
        $item = $request->get('dataimage');
        $em = $this->getDoctrine()->getManager();
        if (@$item['id'] !==null){
           $image = $em->getRepository('DataimageBundle:Dataimage')->findOneById($item['id']);
        }else return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'No se encontró la imagen a modificar')), 200, array('Content-Type'=>'application/json'));

        $olditem = $image->getItemid();
        
        if (isset($item['item_id'])){
            $data = $this->getData($image);
        }else $data=array();
        
        $editForm = $this->createForm(DataimageType::class, $image, array('_items'=>$data));
        $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            try{
                $this->get('gwadmin.dataimages')->modifyImage($image, $olditem);
                return new Response(json_encode(array('errorCode'=>0, 'data'=>array('item'=>$this->get('gwadmin.dataimages')->getItemImage($image)))), 200, array('Content-Type'=>'application/json'));
            }catch(\Exception $e){
                return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
            }
        }else{
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$errors)), 200, array('Content-Type'=>'application/json'));
        }
         return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Error Desconocido')), 200, array('Content-Type'=>'application/json'));
     }    
    
     
    /**
    * move the image
    * 
    * @param int $id is received by Get Request
    * @param Image $item is received by Post Request
    * @author Pacolmg
    */
     public function moveAction(Request $request){
        if (!$request->get('id')) return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'No se encontró la imagen')), 200, array('Content-Type'=>'application/json'));
        return new Response(json_encode($this->get('gwadmin.dataimages')->moveImage($request->get('id'), $request->get('newposition'))), 200, array('Content-Type'=>'application/json'));
     }
}
