<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\YtvideoBundle\Entity\Ytvideo;
use Greetik\YtvideoBundle\Form\Type\YtvideoType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class YtvideoController extends Controller
{
    public function indexAction($type, $id){
        return $this->render('YtvideoBundle:Ytvideo:index.html.twig', array( 'ytvideos'=>$this->get('gwadmin.ytvideos')->getVideos($id, $type), 'configFiles'=>array('modifyAllow'=>true, 'id'=>$id, 'type'=>$type)));        
    }    
    
    /**
    * View an individual ytvideo, if it doesn't belong to the connected user or doesn't exist launch an exception
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function viewAction($id)
     {
         $ytvideo = $this->get('gwadmin.ytvideos')->getVideo($id);
         if (!$ytvideo) throw $this->createNotFoundException('No se ha encontrado el vídeo');
         
         return $this->render('YtvideoBundle:Ytvideo:view.html.twig', array( 'item' => $ytvideo,'new_form' => $this->createForm(YtvideoType::class)->createView()));
     }

    /**
    * Show the new video insert form
    * 
    * @author Pacolmg
    */
     public function insertformAction(Request $request)
     {
        $id = $request->get('id');
        if ($id){
            $ytvideo = $this->get('gwadmin.ytvideos')->getVideo($id);
        }else{ $ytvideo=new Ytvideo(); }
         
        $newForm = $this->createForm(YtvideoType::class, $ytvideo);
         return $this->render('YtvideoBundle:Ytvideo:insert.html.twig',array('new_form' => $newForm->createView(),'id_item' => $request->get('id_item'),'type' => $request->get('type'),'id' => $request->get('id')));
     }

    /**
    * Edit the data of an ytvideo or insert a new one
    * 
    * @param int $id is received by Get Request
    * @param Ytvideo $item is received by Post Request
    * @author Pacolmg
    */
     public function insertmodifyAction(Request $request){
        $item = $request->get('ytvideo');
        
        $id = @$item['id'];
        if ($id){
            $ytvideo = $this->get('gwadmin.ytvideos')->getVideo($id);
        }else{
            $ytvideo=new Ytvideo();
            $insertvideo = true;
        }

        $editForm = $this->createForm(YtvideoType::class, $ytvideo);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if (isset($insertvideo) && $insertvideo){
              try{
                $this->get('gwadmin.ytvideos')->insertVideo($ytvideo, @$item['path'], @$item['id_item'], @$item['type']);
              }catch(\Exception $e){
                  return new Response(json_encode(array('errorCode'=>1, "errorDescription"=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));  
              }
            }else{
              try{
                  $this->get('gwadmin.ytvideos')->modifyVideo($ytvideo, @$item['path']);
              }  catch (\Exception $e){
                  return new Response(json_encode(array('errorCode'=>1, "errorDescription"=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));  
              }
            }
            
            
           return $this->render('YtvideoBundle:Ytvideo:ytvideo.html.twig', array('ytvideo' => $ytvideo, 'configFiles'=>array('modifyAllow'=>true, 'id'=>$ytvideo->getItemid(), 'type'=>$ytvideo->getItemtype())));            
        }else{
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$errors)), 200, array('Content-Type'=>'application/json'));
        }
         return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Error Desconocido')), 200, array('Content-Type'=>'application/json'));
     }

     public function dropAction(Request $request)
     {
         $item = $request->get('ytvideo');
         if (!$item['id'])  return new Response(json_encode(array("errorCode"=>1, "errorDescription"=>"No se encontró el vídeo.")), 200, array('Content-Type'=>'application/json'));        
         return new Response(json_encode($this->get('gwadmin.ytvideos')->dropVideo($item['id'])), 200, array('Content-Type'=>'application/json'));        
     }
     
     
    /**
    * move the image
    * 
    * @param int $id is received by Get Request
    * @param Ytvideo $item is received by Post Request
    * @author Pacolmg
    */
     public function moveAction(Requuest $request){
        if (!$request->get('id')) return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'No se encontró el vídeo')), 200, array('Content-Type'=>'application/json'));
        return new Response(json_encode($this->get('gwadmin.ytvideos')->moveVideo($request->get('id'), $request->get('newposition'))), 200, array('Content-Type'=>'application/json'));
     }}
