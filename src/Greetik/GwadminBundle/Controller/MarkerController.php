<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\GmapBundle\Entity\Marker;
use Greetik\GmapBundle\Form\Type\MarkerType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;

class MarkerController extends Controller {

    public function getmarkersAction($idproject) {
        return new Response(json_encode(array('errorCode' => 0, 'data' => $this->get('gwadmin.gmaps')->getMarkersByModule($idproject, 'array'))), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * View an individual marker, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction(Request $request, $id, $editForm = '') {
        $marker = $this->get('gwadmin.gmaps')->getMarker($id);
        if (!$editForm)
            $editForm = $this->createForm(MarkerType::class, $marker, array('_treesections'=>$this->get('gwadmin.treesections')->getAllTreesections()));
        return $this->render('GmapBundle:Marker:view.html.twig', array(
                    'item' => $marker,
                    'new_form' => $editForm->createView(),
                    'option' => $request->get('option'),
                    'modifyAllow' => $this->get('gwadmin.gmaps')->getMarkerPerm($marker, 'modify')
        ));
    }

    public function moveAction(Request $request) {
        if (!$request->get('id'))
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Falta el Identificador del marcador')), 200, array('Content-Type' => 'application/json'));

        $marker = $this->get('gwadmin.gmaps')->getMarker($request->get('id'));
        $marker->setLat($request->get('lat'));
        $marker->setLon($request->get('lon'));

        try {
            $this->get('gwadmin.gmaps')->modifyMarker($marker);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Get the data of a new Marker by Marker and persis it
     * 
     * @param Marker $item is received by Marker Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject) {
        $marker = new Marker();
        $newForm = $this->createForm(MarkerType::class, $marker, array('_treesections'=>$this->get('gwadmin.treesections')->getAllTreesections()));

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    $this->get('gwadmin.gmaps')->insertMarker($marker, $this->get('gwadmin.modules')->getOptionModule($idproject, 'lat')->getValuevar(), $this->get('gwadmin.modules')->getOptionModule($idproject, 'lon')->getValuevar(), $idproject, $this->getUser()->getId());
                } catch (\Exception $e) {
                    $newForm->addError(new FormError($e->getMessage()));
                    return $this->render('GmapBundle:Marker:insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $idproject)));
            }
        }

        return $this->render('GmapBundle:Marker:insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an marker
     * 
     * @param int $id is received by Get Request
     * @param Marker $item is received by Marker Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $marker = $this->get('gwadmin.gmaps')->getMarker($id);

        $editForm = $this->createForm(MarkerType::class, $marker, array('_treesections'=>$this->get('gwadmin.treesections')->getAllTreesections()));


        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get('gwadmin.gmaps')->modifyMarker($marker);
                } catch (\Exception $e) {
                    $editForm->addError(new FormError($e->getMessage()));
                    return $this->viewAction($id, $editForm);
                }
                return $this->redirect($this->generateUrl('gmap_viewmarker', array('id' => $id)));
            }
        }
        return $this->viewAction($id, $editForm);
    }

    public function deleteAction(Request $request, $id) {
        $marker = $this->get('gwadmin.gmaps')->getMarker($id);
        if ($request->getMethod() == "POST") {
            $this->get("gwadmin.gmaps")->deleteMarker($marker);
            return $this->redirect($this->generateUrl('gmap_viewmap', array('idproject' => $marker->getProject())));
        }
        return $this->viewAction($id);
    }

}
