<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\ContactformBundle\Entity\Formfield;
use Greetik\ContactformBundle\Entity\Formfieldoption;
use Symfony\Component\Config\Definition\Exception\Exception;
use Greetik\ContactformBundle\Form\Type\FormfieldsType;
use Greetik\ContactformBundle\Form\Type\FormfieldoptionType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Greetik\ContactformBundle\Form\Type\SendtoType;
use Symfony\Component\HttpFoundation\Request;

class ContactformController extends Controller {

    public function indexAction($idproject) {

        $email = $this->get('gwadmin.modules')->getOptionModule($idproject, 'email');
        if ($email) $email = $email->getValuevar();
        else $email='';
         
        return $this->render('ContactformBundle:Formfield:index.html.twig', array(
                    'data' => $this->get('gwadmin.contactforms')->getFormfieldsByModule($idproject),
                    'item' => $this->get('gwadmin.contactforms')->getContactformOptions($idproject),
                    'is_catalog' => $this->get('gwadmin.modules')->checkIfTreesectionOrFormLinkedToCatalog($this->get('gwadmin.modules')->getModule($idproject)),
                    'idproject' => $idproject,
                    'insertAllow' => $this->get('gwadmin.contactforms')->getFormfieldPerm('', 'insert'),
                     'new_form' =>  $this->createForm(SendtoType::class, null, array('_email'=>$email))->createView()
        ));
    }

    /**
     * View an individual formfield, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewfieldAction($id, $editForm = '') {
        $formfield = $this->get('gwadmin.contactforms')->getFormfield($id);
        if (!$editForm)
            $editForm = $this->createForm(FormfieldsType::class, $formfield);
        return $this->render('ContactformBundle:Formfield:view.html.twig', array(
                    'item' => $formfield,
                    'new_form' => $editForm->createView(),
                    'is_catalog' => $this->get('gwadmin.modules')->checkIfTreesectionOrFormLinkedToCatalog($this->get('gwadmin.modules')->getModule($formfield->getProject())),
                    'idproject' => $formfield->getProject(),
                    'modifyAllow' => $this->get('gwadmin.contactforms')->getFormfieldPerm($formfield, 'modify')
        ));
    }

    /**
     * Get the data of a new Formfield by Formfield and persis it
     * 
     * @param Formfield $item is received by Formfield Request
     * @author Pacolmg
     */
    public function insertfieldAction(Request $request, $idproject) {

        $formfield = new Formfield();
        $newForm = $this->createForm(FormfieldsType::class, $formfield);
        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    $this->get('gwadmin.contactforms')->insertFormfield($formfield, $idproject);
                } catch (\Exception $e) {
                    $newForm->addError(new FormError($e->getMessage()));
                    return $this->render('ContactformBundle:Formfield:insert.html.twig', array('idproject' => $idproject, 'is_catalog' => $this->get('gwadmin.modules')->checkIfTreesectionOrFormLinkedToCatalog($this->get('gwadmin.modules')->getModule($idproject)),'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('contactform_listformfields', array('idproject' => $idproject)));
            }
        }

        return $this->render('ContactformBundle:Formfield:insert.html.twig', array(
            'idproject' => $idproject, 
            'is_catalog' => $this->get('gwadmin.modules')->checkIfTreesectionOrFormLinkedToCatalog($this->get('gwadmin.modules')->getModule($idproject)),
            'new_form' => $newForm->createView()));
    }

    
    /**
     * Edit the data of an formfield
     * 
     * @param int $id is received by Get Request
     * @param Formfield $item is received by Formfield Request
     * @author Pacolmg
     */
    public function modifyfieldAction(Request $request, $id) {
        $formfield = $this->get('gwadmin.contactforms')->getFormfield($id);

        $editForm = $this->createForm(FormfieldsType::class, $formfield);
        $oldposition = $formfield->getNumorder();


        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get('gwadmin.contactforms')->modifyFormfield($formfield, $oldposition);
                } catch (\Exception $e) {
                    $editForm->addError(new FormError($e->getMessage()));
                    return $this->viewfieldAction($id, $editForm);
                }
                return $this->redirect($this->generateUrl('contactform_viewformfield', array('id' => $id)));
            }
        }
        return $this->viewfieldAction($id, $editForm);
    }    
    
    
   public function deleteAction($id) {
        $formfield = $this->get('gwadmin.contactforms')->getFormfield($id);
        $idproject = $formfield->getProject();
        $error=null;
        
        try{
            $this->get('gwadmin.contactforms')->deleteFormField($formfield);
        }catch( \Exception $e){
            $error=$e->getMessage();
            $this->get('session')->getFlashBag()->set('error',$error);
            return $this->redirect($this->generateUrl('contactform_viewformfield', array('id' => $id)));
        }
        return $this->redirect($this->generateUrl('contactform_listformfields', array('idproject' => $idproject)));

    }
    
    
    /******** OPTIONS *******************/
    
    public function insertformfieldoptionAction($id_field = '', $id = '') {

        if (!empty($id)) {
                $formfieldoption = $this->get('gwadmin.contactforms')->getFormfieldoption($id);
        } else {
            $formfieldoption = new Formfieldoption();
        }

        $newForm = $this->createForm(FormfieldoptionType::class, $formfieldoption);

        return $this->render('ContactformBundle:Options:insert.html.twig', array('new_form' => $newForm->createView(), 'id' => $id, 'id_field' => $id_field));
    }

    public function insertmodifyfieldoptionAction(Request $request, $id_field = '', $id = '') {

        $item = $request->get('Formfieldoption');

        if (!empty($id)) {
            $formfieldoption = $this->get('gwadmin.contactforms')->getFormfieldoption($id);
            $editing = true;
        } else {
            $formfieldoption = new Formfieldoption();
            $editing = false;
        }

        $editForm = $this->createForm(FormfieldoptionType::class, $formfieldoption);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($editing) {
                try {
                    $this->get('gwadmin.contactforms')->modifyFormfieldoption($formfieldoption);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
                }
            } else {
                try {
                    $formfieldoption->setFormfield($this->get('gwadmin.contactforms')->getFormfield($id_field));
                    $this->get('gwadmin.contactforms')->insertFormfieldoption($formfieldoption);
                } catch (\Exception $e) {
                    return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
                }
            }

            return $this->render('ContactformBundle:Options:index.html.twig', array('item' => $formfieldoption->getFormfield(), 'modifyAllow' => true));
        } else {
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $errors)), 200, array('Content-Type' => 'application/json'));
        }
        return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error Desconocido')), 200, array('Content-Type' => 'application/json'));
    }



    public function dropfieldoptionAction($id) {
        $formfieldoption = $this->get('gwadmin.contactforms')->getFormfieldoption($id);

        try {
            $this->get('gwadmin.contactforms')->deleteFormfieldoption($formfieldoption);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }

        return $this->render('ContactformBundle:Options:index.html.twig', array('item' => $formfieldoption->getFormfield(), 'modifyAllow' => true));
    }
    
     public function changesendtoAction(Request $request, $idproject){
        $editForm = $this->createForm(SendtoType::class);        
        
        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try{
                    $this->get('gwadmin.modules')->editOptionModule($idproject, 'email', $editForm->get('sendto')->getData());
                }catch(\Exception $e){
                     $this->get('session')->getFlashBag()->set('error',$e->getMessage());
                }
            }
         }
         return $this->redirect($this->generateUrl('contactform_listformfields',array('idproject'=>$idproject)));
     }    

}
