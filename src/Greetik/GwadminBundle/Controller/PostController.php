<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\BlogBundle\Entity\Post;
use Greetik\BlogBundle\Form\Type\PostType;
use Symfony\Component\HttpFoundation\Request;


class PostController extends Controller {

    /**
     * Render all the posts of the user
     * 
     * @author Pacolmg
     */
    public function indexAction($idproject) {

        return $this->render('BlogBundle:Post:index.html.twig', array(
                    'data' => $this->get('gwadmin.blog')->getPostsByProject($idproject),
                    'idproject' => $idproject,
                    'insertAllow' => true
        ));
    }

    /**
     * View an individual post, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id, $editForm = '') {
        $post = $this->get('gwadmin.blog')->getPost($id);
        if (!$editForm)
            $editForm = $this->createForm(PostType::class, $post, array('_tags'=>$post->getTags()));
        return $this->render('BlogBundle:Post:view.html.twig', array(
                    'option'=>'',
                    'item' => $post,
                    'new_form' => $editForm->createView(),
                    'itemlinks' => $this->get('gwadmin.itemlinks')->getItemlinksByItem($post->getId(), 'post'),
                    'modifyAllow' => $this->get('gwadmin.blog')->getPostPerm($post, 'modify'),
                    'tags'=>$this->get('gwadmin.blog')->getDifferentTags($post->getProject(), true)
        ));
    }

    /**
     * Get the data of a new Post by Post and persis it
     * 
     * @param Post $item is received by Post Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject) {
        $post = new Post();
        
        $newForm = $this->createForm(PostType::class, $post);

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try{
                    $post->setTags(explode(',', $newForm->get('tags')->getData()));
                    $this->get('gwadmin.blog')->insertPost($post, $idproject, $this->getUser()->getId());
                    return $this->redirect($this->generateUrl('blog_listposts', array('idproject' => $idproject)));
                }catch(\Exception $e){
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        
        return $this
                        ->render('BlogBundle:Post:insert.html.twig', array('idproject' => $idproject, 'new_form' => $newForm->createView(),
                    'tags'=>$this->get('gwadmin.blog')->getDifferentTags($idproject, true)));
    }

    /**
     * Edit the data of an post
     * 
     * @param int $id is received by Get Request
     * @param Post $item is received by Post Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $post = $this->get('gwadmin.blog')->getPost($id);
        $oldtags = $post->getTags();
        
        $editForm = $this->createForm(PostType::class, $post);

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            
            if ($editForm->isValid()) {
                try{
                    $post->setTags(explode(",",$editForm->get('tags')->getData()));
                    $this->get('gwadmin.blog')->modifyPost($post, $oldtags);
                    return $this->redirect($this->generateUrl('blog_viewpost', array('id' => $id)));
                }catch(\Exception $e){
                    $this->addFlash('error', $e->getMessage());
                }
            }
        }
        return $this->viewAction($id, $editForm);
    }

    public function deleteAction(Request $request, $id) {
        
        $post = $this->get('gwadmin.blog')->getPost($id);
        if ($request->getMethod() == "POST") {
            $this->get("gwadmin.blog")->deletePost($post);
            return $this->redirect($this->generateUrl('blog_listposts', array('idproject' => $post->getProject())));
        }
        return $this->viewAction($id, $post);
    }

}
