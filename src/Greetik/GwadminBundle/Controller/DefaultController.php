<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Greetik\GwadminBundle\Form\Type\LoadwebType;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('GwadminBundle:_Default:index.html.twig', array());
    }
    
    public function exportwebAction() {
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv; charset="UTF-8"');
        $response->headers->set('Content-Disposition', 'attachment; filename="'.$this->get('beinterface.tools')->spaceencodeurl($this->getRequest()->getHost(), true).'.csv"');
        $response->headers->set('Pragma', "no-cache");
        $response->headers->set('Expires', "0");
        $response->headers->set('Content-Transfer-Encoding', "binary");
        
        $images = array();
        $documents = array();
        
        $sections = array();
        $sectionmodules = $this->get('gwadmin.modules')->getModulesByType(2);
        foreach($sectionmodules as $sm){$sections = array_merge($sections, $this->get('gwadmin.treesections')->getTreesectionsForSelect($sm->getId()));}
        
        foreach($sections as $section){
            $images = array_merge($images, $this->get('gwadmin.dataimages')->getImages($section->getId(), 'treesection')['files']);
            $documents = array_merge($documents, $this->get('gwadmin.dataimages')->getImages($section->getId(), 'treesection', true, 'document')['files']);            
        }
        
        $posts = array();
        $postmodules = $this->get('gwadmin.modules')->getModulesByType(1);
        foreach($postmodules as $pm){$posts = array_merge($posts, $this->get('gwadmin.blog')->getPostsByProject($pm->getId()));}
        foreach($posts as $post){
            $images = array_merge($images, $this->get('gwadmin.dataimages')->getImages($post->getId(), 'post')['files']);
            $documents = array_merge($documents, $this->get('gwadmin.dataimages')->getImages($post->getId(), 'post', true, 'document')['files']); 
        }
        
        return $this->render('GwadminBundle:_Default:exportweb.csv.twig', array(
                    'sections' => $sections,
                    'posts' => $posts,
                    'images' => $images,
                    'documents' => $documents,
                    'hostname' => $this->getRequest()->getSchemeAndHttpHost()
                        ), $response);
    }
    
    public function importwebformAction() {
        $formcsv = $this->createForm(new LoadwebType());

        return $this->render('GwadminBundle:Import:index.html.twig', array(
                    'new_form' => $formcsv->createView()
        ));
    }

    /* import the web from a csv */

    public function importwebAction() {
        $request = $this->getRequest();

        $formcsv = $this->createForm(new LoadwebType());
        $formcsv->bind($request);
        
        try {
            if (count($request->files) <= 0 || !$request->files->get('loadweb')) {
                $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'Error cargando fichero, cárguelo de nuevo, por favor')));
                $response->headers->set('Content-Type', 'application/json');
                return $response;
            }

            $data = $this->get('gwadmin.modules')->importWeb($request->files->get('loadweb')['csv'], $formcsv->get('edittitle')->getData());
            $response = new Response(json_encode(array('errorCode' => 0, 'data' => $data)));
        } catch (\Exception $e) {
            $response = new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage() /*. ' (' . $e->getFile().' - '. $e->getLine() . ')'*/)));
        }

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }    
    
}
