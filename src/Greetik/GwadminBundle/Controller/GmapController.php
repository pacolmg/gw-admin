<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class GmapController extends Controller
{
    
    /**
    * Render all the posts of the user
    * 
    * @author Pacolmg
    */
    public function indexAction($idproject)
     {
         
         return $this->render('GmapBundle:Gmap:index.html.twig', array(
                     'data' => $this->get('gwadmin.gmaps')->getMarkersByModule($idproject),
                     'gpxs' => $this->get('gwadmin.gmaps')->getGpxsByModule($idproject),
                     'item'=>$this->get('gwadmin.gmaps')->getMapOptions($idproject),
                     'idproject'=>$idproject,
                     'insertAllow' => $this->get('gwadmin.gmaps')->getMarkerPerm('', 'insert')
                 ));
     }

     public function saveAction(Request $request, $idproject){
         try{
             $this->get('gwadmin.modules')->editOptionModule($idproject, 'lat', $request->get('lat'));
             $this->get('gwadmin.modules')->editOptionModule($idproject, 'lon', $request->get('lon'));
             $this->get('gwadmin.modules')->editOptionModule($idproject, 'zoom', $request->get('zoom'));
             $this->get('gwadmin.modules')->editOptionModule($idproject, 'type', $request->get('type'));
         }catch(\Exception $e){
             return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
         }
         
         return new Response(json_encode(array('errorCode'=>0)), 200, array('Content-Type'=>'application/json'));
     }
   


}
