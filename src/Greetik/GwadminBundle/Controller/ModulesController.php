<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\GwadminBundle\Entity\Module;
use Greetik\GwadminBundle\Form\Type\ModulesType;
use Symfony\Component\HttpFoundation\Request;

class ModulesController extends Controller {

    /**
     * Render all the modules of the user
     * 
     * @author Pacolmg
     */
    public function indexAction() {

        return $this->render('GwadminBundle:Module:index.html.twig', array(
                    'data' => $this->get('gwadmin.modules')->getModules(),
                    'insertAllow' => $this->get('gwadmin.modules')->getModulePerm('', 'insert')
        ));
    }

    /**
     * View an individual module, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id, $editForm = '') {
        $module = $this->get('gwadmin.modules')->getModule($id);
        if (!$editForm)
            $editForm = $this->createForm(ModulesType::class, $module);
        return $this->render('GwadminBundle:Module:view.html.twig', array(
                    'item' => $module,
                    'new_form' => $editForm->createView(),
                    'modifyAllow' => $this->get('gwadmin.modules')->getModulePerm($module, 'modify')
        ));
    }

    /**
     * Get the data of a new Module by Module and persis it
     * 
     * @param Module $item is received by Module Request
     * @author Pacolmg
     */
    public function insertAction(Request $request) {
        $module = new Module();

        $newForm = $this->createForm(ModulesType::class, $module);

        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                $this->get('gwadmin.modules')->insertModule($module);
                return $this->redirect($this->generateUrl('module_listmodules'));
            }
        }

        return $this->render('GwadminBundle:Module:insert.html.twig', array('new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an module
     * 
     * @param int $id is received by Get Request
     * @param Module $item is received by Module Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $module = $this->get('gwadmin.modules')->getModule($id);

        $editForm = $this->createForm(ModulesType::class, $module);

        $oldname = $module->getName();

        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                $this->get('gwadmin.modules')->modifyModule($module, $oldname);
                return $this->redirect($this->generateUrl('module_viewmodule', array('id' => $id)));
            }
        }
        return $this->viewAction($id, $editForm);
    }

    public function deleteAction($id) {

        $module = $this->get('gwadmin.modules')->getModule($id);
        $this->get("gwadmin.modules")->deleteModule($module, false);

        return $this->redirect($this->generateUrl('module_listmodules'));
    }

}
