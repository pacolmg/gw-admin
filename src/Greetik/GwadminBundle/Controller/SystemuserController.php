<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\SystemuserBundle\Entity\User;
use Greetik\SystemuserBundle\Form\Type\RegistrationFormType;
use Greetik\SystemuserBundle\Form\Type\SystemuserType;
use FOS\UserBundle\Form\Type\ChangePasswordFormType;
use FOS\UserBundle\Form\Type\ResettingFormType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Request;

class SystemuserController extends Controller
{
    
    /**
    * Render all the systemusers of the user
    * 
    * @author Pacolmg
    */
    public function indexAction()
     {
        if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPERADMIN')){
            if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) throw new AccessDeniedException('No tiene permiso para ver el listado de usuarios');
        }
        
         
         return $this->render('SystemuserBundle:Systemuser:index.html.twig', array(
                     'data' => $this->get('systemuser.tools')->getAllSystemusers(),
                    'insertAllow'=>$this->get('systemuser.tools')->getSystemuserPerm('insert')
                 ));
     }

    /**
    * View an individual systemuser, if it doesn't belong to the connected user or doesn't exist launch an exception
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function viewAction($id, $editForm='', $changepassform='')
     {
        $systemuser = $this->get('systemuser.tools')->getSystemuser($id);
        if (!$editForm) $editForm = $this->createForm(SystemuserType::class, $systemuser);
        
        if (!$changepassform) $changepassform = $this->createForm(ChangePasswordFormType::class); 
        return $this->render('SystemuserBundle:Systemuser:view.html.twig', array(
                     'item' => $systemuser,
                     'new_form' => $editForm->createView(),
                     'new_form_pass' => $changepassform->createView(),
                     'modifyAllow'=>$this->get('systemuser.tools')->getSystemuserPerm('modify', $systemuser)
                 ));
     }

    /**
    * Get the data of a new Systemuser by Post and persis it
    * 
    * @param Systemuser $item is received by Post Request
    * @author Pacolmg
    */
     public function insertAction(Request $request){
        $systemuser = $this->get('fos_user.user_manager')->createUser();
        $newForm = $this->createForm(RegistrationFormType::class, $systemuser);

         if ($request->getMethod() == "POST") {
             $newForm->handleRequest($request);
        
             if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPERADMIN')){
                 $rol = $newForm->get('roles')->getData();
                 $insertingadmin=false;
             }else{
                 $insertingadmin=true;
                 $rol = 'ROLE_ADMIN';
             }
             
             
             if ($newForm->isValid()) {
                 try{
                    $this->get('systemuser.tools')->insertSystemuser($systemuser,0, true, array($rol));
                 }catch(\Exception $e){
                     $newForm->addError(new FormError($e->getMessage()));
                     return $this->render('SystemuserBundle:Systemuser:insert.html.twig',array('id_project'=>0, 'new_form' => $newForm->createView()));
                 }
                 if ($insertingadmin) return $this->redirect($this->generateUrl ('club_viewclub', array('id'=>0)));
                 return $this->redirect($this->generateUrl('systemuser_listsystemusers', array('id_project'=>0)));
             }
         }

         return $this->render('SystemuserBundle:Systemuser:insert.html.twig',array('id_project'=>0, 'new_form' => $newForm->createView()));
     }

     public function insertadminAction($id){
         if (!$this->get('security.authorization_checker')->isGranted('ROLE_SUPERADMIN')) throw $this->createAccessDeniedException('No tienes permiso para insertar un administrador');        
         return $this->insertAction($id);
     }
     
    /**
    * Edit the password of an systemuser
    * .
    * @param int $id is received by Get Request
    * @param Systemuser $item is received by Post Request
    * @author Pacolmg
    */
     public function modifypasswordAction(Request $request,$id){
        
        $systemuser = $this->get('systemuser.tools')->getSystemuser($id);
        //$auxsystemuser = $this->get('fos_user.user_manager')->createUser();
        
        $editForm = $this->createForm(ChangePasswordFormType::class); 
        
        if ($request->getMethod() == "POST") {
            try{
                $editForm->handleRequest($request);
            }catch(\Exception $e){
                throw new Exception($e->getMessage());                
            }
            if ($editForm->isValid()) {
                
                
                $systemuser->setPlainPassword($editForm->get('plainPassword')->getData());
                $this->get('systemuser.tools')->modifySystemuser($systemuser);
                return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id'=>$id)));
            }
         }
         return $this->viewAction($id, '', $editForm);
     }

     
    /**
    * Edit the data of an systemuser
    * 
    * @param int $id is received by Get Request
    * @param Systemuser $item is received by Post Request
    * @author Pacolmg
    */
     public function modifyAction(Request $request, $id){

        $systemuser = $this->get('systemuser.tools')->getSystemuser($id);
        $oldroles = $systemuser->getRoles();
        $editForm = $this->createForm(SystemuserType::class, $systemuser);
        
        
        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            $rol = $editForm->get('roles')->getData();
            
            if ($editForm->isValid()) {
                try{
                    $this->get('systemuser.tools')->modifySystemuser($systemuser, array($rol), $oldroles);
                }catch(\Exception $e){
                    $editForm->addError(new FormError($e->getMessage()));
                    return $this->viewAction($id, $editForm);
                }
                return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id'=>$id)));
            }
         }
         return $this->viewAction($id, $editForm);
     }


}
