<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\TreesectionBundle\Entity\Treesection;
use Greetik\TreesectionBundle\Form\Type\TreesectionType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;

class TreesectionController extends Controller {

    /**
     * Render all the treesections of the user
     * 
     * @author Pacolmg
     */
    public function indexAction($idproject) {

        return $this->render('TreesectionBundle:Treesection:index.html.twig', array(
                    'data' => $this->get('gwadmin.treesections')->getTreesectionsByModule($idproject),
                    'is_catalog' => $this->get('gwadmin.modules')->checkIfTreesectionOrFormLinkedToCatalog($this->get('gwadmin.modules')->getModule($idproject)),
                    'insertAllow' => $this->get('gwadmin.treesections')->getTreesectionPerm('', 'insert'),
                    'idproject' => $idproject
        ));
    }

    /**
     * View an individual treesection, if it doesn't belong to the connected user or doesn't exist launch an exception
     * 
     * @param int $id is received by Get Request
     * @author Pacolmg
     */
    public function viewAction($id, $editForm = '') {
        $treesection = $this->get('gwadmin.treesections')->getTreesection($id);
        if (!$editForm)
            $editForm = $this->createForm( TreesectionType::class, $treesection, array('_project'=>$treesection->getProject(), '_uniqueroot'=>true));
        return $this->render('TreesectionBundle:Treesection:view.html.twig', array(
                    'item' => $treesection,
                    'new_form' => $editForm->createView(),
                    'is_catalog' => $this->get('gwadmin.modules')->checkIfTreesectionOrFormLinkedToCatalog($this->get('gwadmin.modules')->getModule($treesection->getProject())),
                    'sectionmodules' => $this->get('gwadmin.sectionmodules')->getSectionmodulesBySection($treesection->getId()),
                    'itemlinks' => $this->get('gwadmin.itemlinks')->getItemlinksByItem($treesection->getId(), 'treesection'),
                    'modifyAllow' => $this->get('gwadmin.treesections')->getTreesectionPerm($treesection, 'modify')
        ));
    }

    /**
     * Get the data of a new Treesection by Treesection and persis it
     * 
     * @param Treesection $item is received by Treesection Request
     * @author Pacolmg
     */
    public function insertAction(Request $request, $idproject) {
        $treesection = new Treesection();
        $newForm = $this->createForm( TreesectionType::class, $treesection, array('_project'=>$idproject, '_uniqueroot'=>true));
        
        if ($request->getMethod() == "POST") {
            $newForm->handleRequest($request);
            if ($newForm->isValid()) {
                try {
                    $this->get('gwadmin.treesections')->insertTreesection($treesection, $idproject, $this->getUser()->getId());
                } catch (\Exception $e) {
                    $newForm->addError(new FormError($e->getMessage()));
                    return $this->render('TreesectionBundle:Treesection:insert.html.twig', array('idproject' => $idproject, 'is_catalog' => $this->get('gwadmin.modules')->checkIfTreesectionOrFormLinkedToCatalog($this->get('gwadmin.modules')->getModule($idproject)),'new_form' => $newForm->createView()));
                }
                return $this->redirect($this->generateUrl('treesection_listtreesections', array('idproject' => $idproject)));
            }
        }

        return $this
                        ->render('TreesectionBundle:Treesection:insert.html.twig', array(
                            'idproject' => $idproject, 
                            'is_catalog' => $this->get('gwadmin.modules')->checkIfTreesectionOrFormLinkedToCatalog($this->get('gwadmin.modules')->getModule($idproject)),
                            'new_form' => $newForm->createView()));
    }

    /**
     * Edit the data of an treesection
     * 
     * @param int $id is received by Get Request
     * @param Treesection $item is received by Treesection Request
     * @author Pacolmg
     */
    public function modifyAction(Request $request, $id) {
        $treesection = $this->get('gwadmin.treesections')->getTreesection($id);

        $editForm = $this->createForm( TreesectionType::class, $treesection, array('_project'=>$treesection->getProject(), '_uniqueroot'=>true, '_notparent'=>true));


        if ($request->getMethod() == "POST") {
            $editForm->handleRequest($request);
            if ($editForm->isValid()) {
                try {
                    $this->get('gwadmin.treesections')->modifyTreesection($treesection);
                } catch (\Exception $e) {
                    $editForm->addError(new FormError($e->getMessage()));
                    return $this->viewAction($id, $editForm);
                }
                return $this->redirect($this->generateUrl('treesection_viewtreesection', array('id' => $id)));
            }
        }
        return $this->viewAction($id, $editForm);
    }

    public function ordertreeAction(Request $request, $idproject) {
        $treesection = $request->get('sections');

        if ($request->getMethod() != "POST")
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => 'No tiene permiso para hacer esta operación')), 200, array('Content-Type' => 'application/json'));
        try {
            $this->get('gwadmin.treesections')->generateAlltree(json_decode($treesection, 1), $idproject, true);
        } catch (\Exception $e) {
            return new Response(json_encode(array('errorCode' => 1, 'errorDescription' => $e->getMessage())), 200, array('Content-Type' => 'application/json'));
        }

        return new Response(json_encode(array('errorCode' => 0)), 200, array('Content-Type' => 'application/json'));
    }

    /**
     * Delete a section from the data of a treesection
     * 
     * @param int $id is received by Get Request
     * @param Treesection $item is received by Treesection Request
     */
    public function deleteAction($id) {

        $treesection = $this->get('gwadmin.treesections')->getTreesection($id);
        $idproject = $treesection->getProject();
        $error=null;
       
        try{
            $this->get('gwadmin.treesections')->deleteTreesection($treesection);
        }catch( \Exception $e){
            $error=$e->getMessage();
            $this->get('session')->getFlashBag()->set('error',$error);
            return $this->redirect($this->generateUrl('treesection_viewtreesection', array('id' => $id)));
        }
        return $this->redirect($this->generateUrl('treesection_listtreesections', array('idproject' => $idproject)));
    }

}
