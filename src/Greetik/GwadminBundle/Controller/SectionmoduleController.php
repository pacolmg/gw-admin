<?php
namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\GwadminBundle\Entity\Sectionmodule;
use Greetik\GwadminBundle\Form\Type\SectionmoduleType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Manage the sectionmodules defined for section
 *
 * @author Pacolmg
 */
class SectionmoduleController extends Controller{
    /**
    * Show the new module insert form
    * 
    * @author Pacolmg
    */
     public function insertformAction($id_section, $id='')
     {
        
         if (!empty($id)){$sectionmodule=$this->get('gwadmin.sectionmodules')->getSectionmodule($id);}
         else{
            $sectionmodule=new Sectionmodule();
         }
         
         $newForm = $this->createForm(SectionmoduleType::class, $sectionmodule);

        return $this->render('GwadminBundle:Sectionmodule:insert.html.twig',array('new_form' => $newForm->createView(), 'id_section' => $id_section,'id' =>$id));
    }

    protected function getSectionsandModules($id_project, $id_section=''){
         if ($id_section){
             $sections = array($this->get('gwadmin.treesections')->getTreesection($id_section));
         }else $sections = $this->get('gwadmin.treesections')->getAllTreesectionsByProject($id_project);
         
         $modules = $this->get('gwadmin.modules')->getModules();
         
         return array($sections, $modules);
    }
    
    /**
    * Edit the data of an sectionmodule or insert a new one
    * 
    * @param int $id is received by Get Request
    * @param Sectionmodule $item is received by Post Request
    * @author Pacolmg
    */
     public function insertmodifyAction(Request $request, $id_section, $id=''){
        $item = $request->get('Sectionmodule');
      
        if (!empty($id)){
            $sectionmodule=$this->get('gwadmin.sectionmodules')->getSectionmodule($id);
            $editing = true;
        }else{ $sectionmodule = new Sectionmodule(); $editing=false;}

         $id_project = $this->getUser()->getProject();
         list($sections, $modules) = $this->getSectionsandModules($id_project, $id_section);
         
         $editForm = $this->createForm(SectionmoduleType::class, $sectionmodule);
         $editForm->handleRequest($request);
        
        if ($editForm->isValid()) {
            
            if ($editing){
                try{
                    $this->get('gwadmin.sectionmodules')->modifySectionmodule($sectionmodule);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }
            }else{
                try{
                    $sectionmodule->setSection($id_section);
                    $sectionmodule->setNumorder(0);
                    $this->get('gwadmin.sectionmodules')->insertSectionmodule($sectionmodule);
                }catch(\Exception $e){
                    return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
                }                
            }
            
            return $this->render('GwadminBundle:Sectionmodule:index.html.twig', array('sectionmodules' => $this->get('gwadmin.sectionmodules')->getSectionmodulesBySection($sectionmodule->getSection()), 'item'=>$this->get('gwadmin.treesections')->getTreesection($sectionmodule->getSection()), 'modifyAllow'=>true));
        }else{
            $errors = $editForm->getErrorsAsString();
            return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$errors)), 200, array('Content-Type'=>'application/json'));
        }
         return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>'Error Desconocido')), 200, array('Content-Type'=>'application/json'));
     }

     /**
    * Delete a sectionmodule
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function dropAction($id)
     {
        $sectionmodule = $this->get('gwadmin.sectionmodules')->getSectionmodule($id);
        
       try{
        $this->get('gwadmin.sectionmodules')->deleteSectionmodule($sectionmodule);
       }catch(\Exception $e){
           return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
       }
       
       return $this->render('GwadminBundle:Sectionmodule:index.html.twig', array('item' => $this->get('gwadmin.treesections')->getTreesection($sectionmodule->getSection()), 'modifyAllow'=>true, 'sectionmodules'=>$this->get('gwadmin.sectionmodules')->getSectionmodulesBySection($sectionmodule->getSection())));
     }
     
     public function putupAction(Request $request){
         //return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$request->get('id'))), 200, array('Content-Type'=>'application/json'));
         $sectionmodule=$this->get('gwadmin.sectionmodules')->getSectionmodule($request->get('id'));
         try{
         $this->get('gwadmin.sectionmodules')->putupSectionmodule($sectionmodule);
         }catch(\Exception $e){
             return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
         }
         return $this->render('GwadminBundle:Sectionmodule:index.html.twig', array('item' => $this->get('gwadmin.treesections')->getTreesection($sectionmodule->getSection()), 'modifyAllow'=>true, 'sectionmodules'=>$this->get('gwadmin.sectionmodules')->getSectionmodulesBySection($sectionmodule->getSection())));
     }
     
     public function putdownAction(Request $request){
         //return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$request->get('id'))), 200, array('Content-Type'=>'application/json'));
         $sectionmodule=$this->get('gwadmin.sectionmodules')->getSectionmodule($request->get('id'));
         try{
         $this->get('gwadmin.sectionmodules')->putdownSectionmodule($sectionmodule);
         }catch(\Exception $e){
             return new Response(json_encode(array('errorCode'=>1, 'errorDescription'=>$e->getMessage())), 200, array('Content-Type'=>'application/json'));
         }
         return $this->render('GwadminBundle:Sectionmodule:index.html.twig', array('item' => $this->get('gwadmin.treesections')->getTreesection($sectionmodule->getSection()), 'modifyAllow'=>true, 'sectionmodules'=>$this->get('gwadmin.sectionmodules')->getSectionmodulesBySection($sectionmodule->getSection())));
     }
     
}
