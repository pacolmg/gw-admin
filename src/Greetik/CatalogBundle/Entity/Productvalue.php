<?php

namespace Greetik\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productvalue
 *
 * @ORM\Table(name="productvalue", indexes={
 *      @ORM\Index(name="productfield", columns={"productfield"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\CatalogBundle\Entity\ProductvalueRepository")
 */
class Productvalue
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="productfield", type="integer")
     */
    private $productfield;

    /**
     * @var string
     *
     * @ORM\Column(name="productvalue", type="text", nullable=true)
     */
    private $productvalue;

    /**
    * @ORM\ManyToOne(targetEntity="Product", cascade={"persist", "remove"})
    * @ORM\JoinColumn(name="product", referencedColumnName="id", onDelete="CASCADE")
    */
    private $product;    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set productfield
     *
     * @param integer $productfield
     *
     * @return Productvalue
     */
    public function setProductfield($productfield)
    {
        $this->productfield = $productfield;

        return $this;
    }

    /**
     * Get productfield
     *
     * @return integer
     */
    public function getProductfield()
    {
        return $this->productfield;
    }

    /**
     * Set productvalue
     *
     * @param string $productvalue
     *
     * @return Productvalue
     */
    public function setProductvalue($productvalue)
    {
        $this->productvalue = $productvalue;

        return $this;
    }

    /**
     * Get productvalue
     *
     * @return string
     */
    public function getProductvalue()
    {
        return $this->productvalue;
    }

    /**
     * Set product
     *
     * @param \Greetik\CatalogBundle\Entity\Product $product
     *
     * @return Productvalue
     */
    public function setProduct(\Greetik\CatalogBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Greetik\CatalogBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
