<?php

namespace Greetik\CatalogBundle\Entity;

/**
 * ProductRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProductRepository extends \Doctrine\ORM\EntityRepository
{
    
    public function findByProjectAndCategories($project, $categories){
        return $this->getEntityManager()
            ->createQuery('SELECT p FROM CatalogBundle:Product p WHERE p.section IN ('.  implode(',', $categories).')  AND p.project=:id_project'  )
            ->setParameter('id_project', $project)
            ->getResult();
    }
    
}
