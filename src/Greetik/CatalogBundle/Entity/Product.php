<?php

namespace Greetik\CatalogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @ORM\Table(name="product", indexes={
 *      @ORM\Index(name="project", columns={"project"}),  @ORM\Index(name="section", columns={"section"})
 * })
 * @ORM\Entity(repositoryClass="Greetik\CatalogBundle\Entity\ProductRepository")
 */
class Product
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="project", type="integer")
     */
    private $project;

    /**
     * @var integer
     *
     * @ORM\Column(name="section", type="integer")
     */
    private $section;

    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=255, nullable=true)
     */
    private $tags;
    
    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="metatitle", type="string", length=255, nullable=true)
     */
    private $metatitle;
    
    /**
     * @Assert\Length(max=255)
     * @var string
     *
     * @ORM\Column(name="metadescription", type="string", length=255, nullable=true)
     */
    private $metadescription;

    /* @var integer
     *
     * @ORM\Column(name="numorder", type="integer")
     */
    private $numorder;

    /**
     * @ORM\OneToMany(targetEntity="Productvalue", mappedBy="product")
     */
    private $productvalues;      

    
    private $ytvideos;
    
    private $dataimages;

    public function __construct() {
        $this->productvalues = new ArrayCollection();
    }

     /**
     * Set ytvideos
     *
     * @param array $ytvideos
     * @return Post
     */
    public function setYtvideos($ytvideos)
    {
        $this->ytvideos = $ytvideos;

        return $this;
    }

    /**
     * Get ytvideos
     *
     * @return array
     */
    public function getYtvideos()
    {
        return $this->ytvideos;
    }
    
    
         /**
     * Set dataimages
     *
     * @param array $dataimages
     * @return Product
     */
    public function setDataimages($dataimages)
    {
        $this->dataimages = $dataimages;

        return $this;
    }

    /**
     * Get dataimage
     *
     * @return array
     */
    public function getDataimages()
    {
        return $this->dataimages;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project
     *
     * @param integer $project
     *
     * @return Product
     */
    public function setProject($project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return integer
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set section
     *
     * @param integer $section
     *
     * @return Product
     */
    public function setSection($section)
    {
        $this->section = $section;

        return $this;
    }

    /**
     * Get section
     *
     * @return integer
     */
    public function getSection()
    {
        return $this->section;
    }


    /**
     * Set numorder
     *
     * @param integer $numorder
     *
     * @return Product
     */
    public function setNumorder($numorder)
    {
        $this->numorder = $numorder;

        return $this;
    }

    /**
     * Get numorder
     *
     * @return integer
     */
    public function getNumorder()
    {
        return $this->numorder;
    }



    /**
     * Add productvalue
     *
     * @param \Greetik\CatalogBundle\Entity\Productvalue $productvalue
     *
     * @return Product
     */
    public function addProductvalue(\Greetik\CatalogBundle\Entity\Productvalue $productvalue)
    {
        $this->productvalues[] = $productvalue;

        return $this;
    }

    /**
     * Remove productvalue
     *
     * @param \Greetik\CatalogBundle\Entity\Productvalue $productvalue
     */
    public function removeProductvalue(\Greetik\CatalogBundle\Entity\Productvalue $productvalue)
    {
        $this->productvalues->removeElement($productvalue);
    }

    /**
     * Get productvalues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProductvalues()
    {
        return $this->productvalues;
    }

    /**
     * Set tags
     *
     * @param string $tags
     *
     * @return Product
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set metatitle
     *
     * @param string $metatitle
     *
     * @return Product
     */
    public function setMetatitle($metatitle)
    {
        $this->metatitle = $metatitle;

        return $this;
    }

    /**
     * Get metatitle
     *
     * @return string
     */
    public function getMetatitle()
    {
        return $this->metatitle;
    }

    /**
     * Set metadescription
     *
     * @param string $metadescription
     *
     * @return Product
     */
    public function setMetadescription($metadescription)
    {
        $this->metadescription = $metadescription;

        return $this;
    }

    /**
     * Get metadescription
     *
     * @return string
     */
    public function getMetadescription()
    {
        return $this->metadescription;
    }
}
