<?php

namespace Greetik\CatalogBundle\Service;
use Greetik\CatalogBundle\Entity\Product;
use Greetik\CatalogBundle\Entity\Productvalue;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */

class Catalogtools {
    
    private $em;
    private $dataimage;
    private $ytvideo;
    private $interfacetools;
    
    public function __construct($_entityManager, $_dataimage, $_ytvideo, $_interfacetools)
    {
        $this->em = $_entityManager;
        $this->dataimage = $_dataimage;
        $this->ytvideo = $_ytvideo;
        $this->interfacetools = $_interfacetools;
    }

    public function getAllProductsByProject($project='', $categories=array()){
        if (empty($project)){
            if (count($categories)==0) return $this->em->getRepository('CatalogBundle:Product')->findAll();
            return $this->em->getRepository('CatalogBundle:Product')->findByCategories($categories);
        }
        if (count($categories)==0)return $this->em->getRepository('CatalogBundle:Product')->findByProject($project);
        
        return $this->em->getRepository('CatalogBundle:Product')->findByProjectAndCategories($project, $categories);
    }
    
    public function getProductsByPage($project='', $page, $productperpage=10){
        return $this->em->getRepository('CatalogBundle:Product')->findProductsByPage($project, $page, $productperpage);
    }
    
    public function getProduct($id){
       $product = $this->em->getRepository('CatalogBundle:Product')->findOneById($id);
     
       if (!$product) throw new NotFoundHttpException('No se encuentra el producto');
       $product->setYtvideos($this->ytvideo->getVideos($product->getId(), 'product'));
       
       return $product; 
    }
    
    
    public function modifyProduct($product, $fields){
        $this->em->persist($product);
        
        foreach($fields as $k=>$v){
            if ($v['value'] instanceof \DateTime) {
                $v['value'] = $v['value']->format('Y-m-d');
            }
            
            $productvalue = $this->em->getRepository('CatalogBundle:Productvalue')->findOneBy(array('productfield'=>$v['field'], 'product'=>$product));
            if (!$productvalue) $productvalue = new Productvalue();
            
            $productvalue->setProduct($product);
            $productvalue->setProductfield($v['field']);
            $productvalue->setProductvalue($v['value']);
            $this->em->persist($productvalue);
        }
        
        $this->em->persist($product);
        $this->em->flush();
    }
    
    public function insertProduct($product, $project, $fields){
        $product->setProject($project);
        $this->em->persist($product);
        
        foreach($fields as $k=>$v){
            if ($v['value'] instanceof \DateTime) {
                $v['value'] = $v['value']->format('Y-m-d');
            }

            $productvalue = new Productvalue();
            $productvalue->setProduct($product);
            $productvalue->setProductfield($v['field']);
            $productvalue->setProductvalue($v['value']);
            $this->em->persist($productvalue);
        }
        
        $this->em->persist($product);
        $this->em->flush();
    }
    
    
    public function deleteProduct($product){
        $ytvideos = $this->ytvideo->getVideos($product->getId(), 'product');
        foreach($ytvideos as $ytvideo) $this->ytvideo->dropVideo($ytvideo->getId());
        $dataimages = $this->dataimage->getImages($product->getId(), 'product');      
        
        $_SERVER['REQUEST_METHOD'] = 'POST';
        foreach($dataimages['files'] as $image){ $this->dataimage->dropImage($image['id']);}
        
        $this->em->remove($product);
        $this->em->flush();
    }
    
    public function getNumberOfProductsByProject($project){
        return $this->em->getRepository('CatalogBundle:Product')->findNumberOfProductsByProject($project);
    }
    
    public function getProductImages($product){
        if (!is_numeric($product)) $product = $product->getId();
        return $this->dataimage->getImages($product, 'product');
    }    
    
    public function getProductDocuments($product){
        if (!is_numeric($product)) $product = $product->getId();
        return $this->dataimage->getDocuments($product, 'product');
    }
    
}
