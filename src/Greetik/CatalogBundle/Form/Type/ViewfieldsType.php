<?php
    namespace Greetik\CatalogBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\OptionsResolver\OptionsResolver;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ViewfieldsType
 *
 * @author Paco
 */
class ViewfieldsType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options){
        
        $formfields = array();
        foreach($options['_formfields'] as $ff){ $formfields[$ff->getName()] = $ff->getId();}  
        
        $values = array();
        if ($options['_values'])
            foreach($options['_values'] as $k=>$v){ $values[$k]=intval($v); }
        
            
        $builder
            ->add('fieldstoview', ChoiceType::class, array(
                'choices' => $formfields,
                'required'=>true,
                'expanded'=>false,
                'multiple'=>true,
                'data'=>$values
            ));
        
                            
    }
    
    public function getName(){
        return 'Viewfields';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            '_formfields' => array(),
            '_values' => null
        ));
    }
}

