<?php
    namespace Greetik\CatalogBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
    use Symfony\Component\Form\Extension\Core\Type\TextareaType;
    use Symfony\Component\Form\Extension\Core\Type\TextType;
    use Symfony\Component\Form\Extension\Core\Type\MoneyType;
    use Symfony\Component\Form\Extension\Core\Type\EmailType;
    use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
    use Symfony\Component\Form\Extension\Core\Type\DateType;
    use Symfony\Component\Form\Extension\Core\Type\NumberType;
    use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
    use Symfony\Component\OptionsResolver\OptionsResolver;
    
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProductType
 *
 * @author Paco
 */
class ProductType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options){
        
        $sections = array();
        foreach($options['_sections'] as $s) $sections[$s->getName()] = $s->getId();
        
        $builder
            ->add('section', ChoiceType::class, array(
                'choices' => $sections,
                'required'=>true,
                'expanded'=>false,
                'multiple'=>false 
            ))
                ->add('metatitle')
                ->add('metadescription')
                ->add('tags');
        
        foreach($options['_formfields'] as $formfield){
            
            $value = '';
            if (!empty($options['_values'])){
                switch($formfield->getFormfieldtype()->getCode()){
                    case 6: $value = explode(',', $options['_values']['field_'.$formfield->getId()]); break;
                    case 9: $value = new \Datetime($options['_values']['field_'.$formfield->getId()]); break;
                    default: $value = $options['_values']['field_'.$formfield->getId()]; break;
                }
            }
            
            $arrayoptions = array('label'=>$formfield->getName(),'required'=>$formfield->getOblig(),'mapped'=>false);
            if ($value) $arrayoptions = array_merge( array('data'=>$value),$arrayoptions);
            
            switch($formfield->getFormfieldtype()->getType()){
                case 'text': $classname = TextType::class; break;
                case 'textarea': $classname = TextareaType::class; break;
                case 'email': $classname = EmailType::class; break;
                case 'checkbox': $classname = CheckboxType::class; break;
                case 'money': $classname = MoneyType::class; break;
                case 'number': $classname = NumberType::class; break;
                case 'choice': $classname = ChoiceType::class; $arrayoptions = array_merge($arrayoptions, array('attr'=>array('class'=>'select2'), 'choices'=>$this->getChoicesFromField($formfield), 'multiple'=>$formfield->getFormfieldtype()->getCode()==6)); break;
                case 'date': $classname = DateType::class;$arrayoptions = array_merge($arrayoptions, array('attr'=>array('class'=>'datepicker'), 'widget' => 'single_text', 'format' => 'dd/MM/yyyy')); break;
                case 'datetime': $classname = DatetimeType::class; $arrayoptions = array_merge($arrayoptions, array('attr'=>array('class'=>'datetimepicker'),'widget' => 'single_text', 'format' => 'dd/MM/yyyy - HH:mm')); break;
            }
            
        $builder->add('field_'.$formfield->getId(), $classname, $arrayoptions);
           
        }
                            
    }
    
    protected function getChoicesFromField($formfield){
        $data = array();
        foreach($options = $formfield->getFormfieldoptions() as $option)
            $data[$option->getId()] = $option->getName(); 
        return $data;
    }
    
    public function getName(){
        return 'Product';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Greetik\CatalogBundle\Entity\Product',
            '_formfields' => array(),
            '_sections' => array(),
            '_values' => null
        ));
    }    
}

