<?php

namespace Greetik\BlogBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PostRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PostRepository extends EntityRepository {

    public function findAll() {
        return $this->findBy(array(), array('publishdate' => 'DESC'));
    }

    public function findAllByProject($project) {
        return $this->findBy(array('project' => $project), array('publishdate' => 'DESC'));
    }

    protected function findPublishedPostsByProject($project) {
        return $this->getEntityManager()
                        ->createQuery('SELECT p FROM BlogBundle:Post p WHERE p.publishdate<=CURRENT_TIMESTAMP() AND (p.enddate>CURRENT_TIMESTAMP() OR p.enddate IS NULL)  AND p.project=:id_project  ORDER BY p.publishdate DESC')
                        ->setParameter('id_project', $project)
                        ->getResult();
    }

    public function findPublishedPosts($project = '') {
        if (!empty($project))
            return $this->findPublishedPostsByProject($project);

        return $this->getEntityManager()
                        ->createQuery('SELECT p FROM BlogBundle:Post p WHERE p.publishdate<=CURRENT_TIMESTAMP() AND (p.enddate>CURRENT_TIMESTAMP() OR p.enddate IS NULL)  ORDER BY p.publishdate DESC')
                        ->getResult();
    }

    public function findPublishedPostsByPage($project = '', $page, $postperpage = 10) {
        if (empty($page) || !$page || $page == 0)
            $page = 1;
        return $this->getEntityManager()
                        ->createQuery('SELECT p FROM BlogBundle:Post p WHERE p.publishdate<=CURRENT_TIMESTAMP() AND (p.enddate>CURRENT_TIMESTAMP() OR p.enddate IS NULL) ' . ((!empty($project)) ? ' AND p.project=' . $project : '') . '  ORDER BY p.publishdate DESC')
                        ->setMaxResults($postperpage)
                        ->setFirstResult(intval(($page - 1) * $postperpage))
                        ->getResult();
    }

    public function findNumberOfPostsByProject($project) {
        $value = $this->getEntityManager()
                ->createQuery("SELECT COUNT(p) AS num_result FROM BlogBundle:Post p WHERE p.project=:id_project AND (p.publishdate<CURRENT_TIMESTAMP()) AND (p.enddate>CURRENT_TIMESTAMP() OR p.enddate IS NULL)")
                ->setParameter('id_project', $project)
                ->getResult();
        return $value[0]["num_result"];
    }

    public function getPublishedPostsByTagAndPage($tag, $page, $postperpage = 10) {
        if (empty($page) || !$page || $page == 0)
            $page = 1;

        return $this->getEntityManager()
                        ->createQuery('SELECT p FROM BlogBundle:Post p WHERE p.publishdate<=CURRENT_TIMESTAMP() AND (p.enddate>CURRENT_TIMESTAMP() OR p.enddate IS NULL)  AND p.tags LIKE :tag ORDER BY p.publishdate DESC')
                        ->setMaxResults($postperpage)
                        ->setFirstResult(intval(($page - 1) * $postperpage))
                        ->setParameter('tag', '%"'.$tag.'"%')
                        ->getResult();
    }

    public function findNumberOfPostsByTag($tag) {
        $value = $this->getEntityManager()
                ->createQuery("SELECT COUNT(p) AS num_result FROM BlogBundle:Post p WHERE p.tags LIKE :tag AND (p.publishdate<CURRENT_TIMESTAMP()) AND (p.enddate>CURRENT_TIMESTAMP() OR p.enddate IS NULL)")
                ->setParameter('tag', '%"'.$tag.'"%' )
                ->getResult();
        return $value[0]["num_result"];
    }

}
