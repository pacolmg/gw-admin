<?php

namespace Greetik\GwadminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\BlogBundle\Entity\Post;
use Greetik\BlogBundle\Form\Type\PostType;

class PostController extends Controller
{
    
    /**
    * Render all the posts of the user
    * 
    * @author Pacolmg
    */
    public function indexAction($idproject)
     {
         
         return $this->render('BlogBundle:Post:index.html.twig', array(
                     'data' => $this->get('blog.tools')->getAllPostsByProject($idproject),
                     'insertAllow' => true
                 ));
     }

    /**
    * View an individual post, if it doesn't belong to the connected user or doesn't exist launch an exception
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function viewAction($id, $editForm='')
     {
        $post = $this->get('blog.tools')->getPost($id);
        if (!$editForm) $editForm = $this->createForm(new PostType(), $post);
        return $this->render('BlogBundle:Post:view.html.twig', array(
                     'item' => $post,
                     'new_form' => $editForm->createView(),
                     'option' => $this->getRequest()->get('option'),
                     'modifyAllow' => 1
                 ));
     }

    /**
    * Get the data of a new Post by Post and persis it
    * 
    * @param Post $item is received by Post Request
    * @author Pacolmg
    */
     public function insertAction()
     {
        $request = $this->getRequest();
        $post=new Post();
        $newForm = $this->createForm(new PostType(), $post);

         if ($request->getMethod() == "POST") {
             $newForm->bind($request);             
             if ($newForm->isValid()) {
                 $this->get('blog.tools')->insertPost($post,1, 1);
                 return $this->redirect($this->generateUrl('blog_listposts'));
             }
         }

         return $this
           ->render('BlogBundle:Post:insert.html.twig',array('new_form' => $newForm->createView()));
     }

    /**
    * Edit the data of an post
    * 
    * @param int $id is received by Get Request
    * @param Post $item is received by Post Request
    * @author Pacolmg
    */
     public function modifyAction($id){
        $request = $this->getRequest();

        $post = $this->get('blog.tools')->getPost($id);
        
        $auxForm = $request->get('Post');
        $newgames = isset($auxForm['games']) ? $auxForm['games'] : array();
        $oldgames = $post->getGames()->toArray();
        
        $editForm = $this->createForm(new PostType(), $post);
        
        
        if ($request->getMethod() == "POST") {
            $editForm->bind($request);
            if ($editForm->isValid()) {
                $this->get('blog.tools')->modifyPost($post, $oldgames, $newgames);
                return $this->redirect($this->generateUrl('blog_viewpost', array('id'=>$id)));
            }
         }
         return $this->viewAction($id, $editForm);
     }


}
