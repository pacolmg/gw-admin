<?php

namespace Greetik\BlogBundle\Service;

use Greetik\BlogBundle\Entity\Post;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Greetik\BlogBundle\Entity\Tag;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Blogtools {

    private $em;
    private $dataimage;
    private $ytvideo;
    private $interfacetools;

    public function __construct($_entityManager, $_dataimage, $_ytvideo, $_interfacetools) {
        $this->em = $_entityManager;
        $this->dataimage = $_dataimage;
        $this->ytvideo = $_ytvideo;
        $this->interfacetools = $_interfacetools;
    }

    public function getAllPostsByProject($project = '') {
        if (empty($project))
            return $this->em->getRepository('BlogBundle:Post')->findAll();
        return $this->em->getRepository('BlogBundle:Post')->findAllByProject($project);
    }

    public function getPublishedPosts($project = '') {
        return $this->em->getRepository('BlogBundle:Post')->findPublishedPosts($project);
    }

    public function getPublishedPostsByPage($project = '', $page, $postperpage = 10) {
        return $this->em->getRepository('BlogBundle:Post')->findPublishedPostsByPage($project, $page, $postperpage);
    }

    public function getPublishedPostsByTagAndPage($tag = '', $page, $postperpage = 10) {
        if (!$this->em->getRepository('BlogBundle:Tag')->findByName($this->cleantag($tag))) throw new NotFoundHttpException('La etiqueta no existe');
        return $this->em->getRepository('BlogBundle:Post')->getPublishedPostsByTagAndPage($tag, $page, $postperpage);
    }
    
    public function getPostsByExtra($text){
        return $this->em->getRepository('BlogBundle:Post')->findByExtra($text);
    }
    
    public function getPost($id) {
        $post = $this->em->getRepository('BlogBundle:Post')->findOneById($id);

        if (!$post)
            throw new NotFoundHttpException('No se encuentra el post');
        $post->setYtvideos($this->ytvideo->getVideos($post->getId(), 'post'));

        return $post;
    }

    public function modifyPost($post, $oldtags) {
        $post->setLastedit(new \DateTime());

        foreach ($tags = $post->getTags() as $tag) {
            if (!in_array($tag, $oldtags))
                $this->inserttag($tag, $post->getProject());
        }
        foreach ($oldtags as $tag) {
            if (!in_array($tag, $tags))
                $this->deletetag($tag, $post->getProject());
        }


        $this->em->persist($post);
        $this->em->flush();
    }

    public function deletetag($name, $project) {
        $name = $this->cleantag($name);
        if (!$name) return;
        
        $tag = $this->em->getRepository('BlogBundle:Tag')->findBy(array('name' => $name, 'project' => $project));
        if (isset($tag[0]))
            $tag = $tag[0];
        else
            return;

        $tag->setRepetitions($tag->getRepetitions() - 1);
        if ($tag->getRepetitions() <= 0)
            $this->em->remove($tag);
        else
            $this->em->persist($tag);
    }

    protected function cleantag($name){
        if (!$name || trim($name) == '' ) return '';
        $auxname = explode(' ', $name);
        $name = '';
        foreach($auxname as $k=>$word){
            $auxword = str_replace("\n", "", str_replace("\r", "", trim($word)));
            
            if (!$word) unset($auxname[$k]);
            else $auxname[$k] = $auxword;
        }
        $auxname = array_values($auxname);
        
        $numwords = count($auxname);
        if (!$numwords || $numwords<=0) return '';
        if ($numwords == 1) return $auxname[0];
        return implode(' ', $auxname);
    }
    public function inserttag($name, $project, $flush=false) {
        $name = $this->cleantag($name);
        if (!$name) return;
        
        $tag = $this->em->getRepository('BlogBundle:Tag')->findBy(array('name' => $name, 'project' => $project));
        if (isset($tag[0]))
            $tag = $tag[0];
        else {
            $tag = new Tag();
            $tag->setName($name);
            $tag->setProject($project);
            $tag->setRepetitions(0);
            $this->em->persist($tag);
            if ($flush) $this->em->flush();
        }

        $tag->setRepetitions($tag->getRepetitions() + 1);
    }

    public function insertPost($post, $user, $project) {
        $post->setCreatedat(new \DateTime());
        $post->setProject($project);
        $post->setUser($user);

        foreach ($tags = $post->getTags() as $tag) {
            $this->inserttag($tag, $project);
        }

        $this->em->persist($post);
        //$this->em->getClassMetaData(get_class($post))->setIdGeneratorType(\Doctrine\ORM\Mapping\ClassMetadata::GENERATOR_TYPE_NONE);
        $this->em->flush();
    }

    public function deletePost($post) {
        $ytvideos = $this->ytvideo->getVideos($post->getId(), 'post');
        foreach ($ytvideos as $ytvideo)
            $this->ytvideo->dropVideo($ytvideo->getId());
        $dataimages = $this->dataimage->getImages($post->getId(), 'post');

        $_SERVER['REQUEST_METHOD'] = 'POST';
        foreach ($dataimages['files'] as $image) {
            $this->dataimage->dropImage($image['id']);
        }

        $this->em->remove($post);
        $this->em->flush();
    }

    public function getNumberOfPostsByProject($project) {
        return $this->em->getRepository('BlogBundle:Post')->findNumberOfPostsByProject($project);
    }

    public function getNumberOfPostsByTag($tag) {
        return $this->em->getRepository('BlogBundle:Post')->findNumberOfPostsByTag($tag);
    }

    public function getPostImages($post) {
        if (!is_numeric($post))
            $post = $post->getId();
        return $this->dataimage->getImages($post, 'post');
    }

    public function getPostDocuments($post) {
        if (!is_numeric($post))
            $post = $post->getId();
        return $this->dataimage->getDocuments($post, 'post');
    }

    protected function getTags($project = '') {
        if (!empty($project))
            return $this->em->getRepository('BlogBundle:Tag')->findByProject($project);
        else
            return $this->em->getRepository('BlogBundle:Tag')->findAll();
    }

    public function getDifferentTags($project = '', $forselect = false) {
        $tags = $this->getTags($project);

        if (!$forselect)
            return $tags;

        $arraytags = array();
        foreach ($tags as $tag) {
            $name = $tag->getName();
            if (!in_array($name, $arraytags))
                array_push($arraytags, $name);
        }

        return $arraytags;
    }

    public function getTagsCloud($project) {
        return $this->em->getRepository('BlogBundle:Tag')->findCloud($project);
    }

}
