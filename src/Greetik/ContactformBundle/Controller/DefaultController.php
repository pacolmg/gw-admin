<?php

namespace Greetik\ContactformBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('ContactformBundle:Default:index.html.twig', array('name' => $name));
    }
}
