<?php

namespace Greetik\ContactformBundle\Service;

use Greetik\ContactformBundle\Entity\Formfield;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */
class Formfieldtools {

    private $em;
    private $interfacetools;

    public function __construct($_entityManager, $_interfacetools) {
        $this->em = $_entityManager;
        $this->interfacetools = $_interfacetools;
    }

    public function getFormfieldsByProject($project) {
        return $this->em->getRepository('ContactformBundle:Formfield')->findBy(array('project'=>$project), array('numorder'=>'ASC'));
    }

    public function getFormfield($id) {
        $formfield = $this->em->getRepository('ContactformBundle:Formfield')->findOneById($id);

        if (!$formfield)
            throw new NotFoundHttpException('No se encuentra el campo');

        return $formfield;
    }

    public function modifyFormfield($formfield, $oldposition) {
        if ($formfield->getNumorder() != $oldposition) {
            $project_id=$formfield->getProject();
            $this->interfacetools->moveitemElem('ContactformBundle:Formfield', $formfield->getId(), $formfield->getNumorder()-1, $oldposition, $this->em->getRepository('ContactformBundle:Formfield')->findMaxNumOrderByProject($project_id), 's', $project_id, '');
        } else {
            $this->em->persist($formfield);
            $this->em->flush();
        }
    }

    public function insertFormfield($formfield, $project) {
        $formfield->setProject($project);
        if (!$formfield->getOblig())
            $formfield->setOblig(false);
        if (!$formfield->getDisab())
            $formfield->setDisab(false);

        $formfield->setNumorder($this->em->getRepository('ContactformBundle:Formfield')->findMaxNumOrderByProject($project) + 1);

        $this->em->persist($formfield);
        $this->em->flush();
    }

    public function deleteFormfield($formfield) {
        $project = $formfield->getProject();
        $this->em->remove($formfield);
        $this->em->flush();
        $this->interfacetools->reorderItemElems($this->getFormfieldsByProject($project));
    }

}
