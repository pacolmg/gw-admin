<?php

namespace Greetik\ContactformBundle\Service;
use Greetik\ContactformBundle\Entity\Formfieldoption;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */

class Formfieldoptiontools {
    
    private $em;
    
    public function __construct($_entityManager)
    {
        $this->em = $_entityManager;
    }

    public function getFormfieldoptionsByFormfield($formfield){
        return $this->em->getRepository('ContactformBundle:Formfieldoption')->findByFormfield($formfield);
    }
    
    public function getFormfieldoption($id){
       $formfieldoption = $this->em->getRepository('ContactformBundle:Formfieldoption')->findOneById($id);
     
       if (!$formfieldoption) throw new NotFoundHttpException('No se encuentra la opción '.$id);
       
       return $formfieldoption; 
    }
    
    public function modifyFormfieldoption($formfieldoption){       
       $this->em->persist($formfieldoption);
       $this->em->flush();
    }
    
    public function insertFormfieldoption($formfieldoption){
        $this->em->persist($formfieldoption);
        $this->em->flush();
    }
    
    public function deleteFormfieldoption($formfieldoption){
        $this->em->remove($formfieldoption);
        $this->em->flush();
    }
    
}
