<?php

namespace Greetik\SystemuserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // add your custom field
        $builder->add('name');
        $builder->add('surname');
        $builder->add('comments');

        $builder->add('roles', ChoiceType::class, array(
                'choices' => array(
                    'Usuario' => 'ROLE_USER',
                    'Administrador'   => 'ROLE_ADMIN',             
                ),
                'expanded' => false,
                'multiple' => false,
                'mapped' => false,
            ));        
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
        // Or for Symfony < 2.8
        // return 'fos_user_registration';
    }

    public function getName()
    {
        return 'systemuser_registration';
    }
}