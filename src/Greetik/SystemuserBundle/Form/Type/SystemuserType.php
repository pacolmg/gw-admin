<?php
    namespace Greetik\SystemuserBundle\Form\Type;
    
    use Symfony\Component\Form\AbstractType;
    use Symfony\Component\Form\FormBuilderInterface;
    use Doctrine\ORM\EntityRepository;
    use Symfony\Component\Security\Core;
    use Symfony\Component\Security\Core\User\User;
    use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SystemuserType
 *
 * @author Paco
 */
class SystemuserType extends AbstractType{
    
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
        ->add('name')
        ->add('surname')
        ->add('username')
        ->add('email')
        ->add('comments');
        
        $builder->add('roles', ChoiceType::class, array(
                'choices' => array(
                    'Usuario' => 'ROLE_USER',
                    'Administrador'   => 'ROLE_ADMIN',             
                ),
                'expanded' => false,
                'multiple' => false,
                'mapped' => false,
            ));
    }
    
    public function getName(){
        return 'Systemuser';
    }
    
    public function getDefaultOptions(array $options){
        return array( 'data_class' => 'Greetik\SystemuserBundle\Entity\Systemuser');
    }
}

