<?php

namespace Greetik\SystemuserBundle\Service;
use Greetik\SystemuserBundle\Entity\Systemuser;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Tools
 *
 * @author Paco
 */

class Systemusertools {
    
    private $em;
    private $usermanager;
    private $_context;
    
    public function __construct($_entityManager, $_usermanager, $__context)
    {
        $this->em = $_entityManager;
        $this->usermanager = $_usermanager;
        $this->_context = $__context;
    }

    protected function checkUsername($username, $id=''){
        $systemuser = $this->usermanager->findUserByUsername($username);
        if ($systemuser){
            if (!empty($id)){
                if ($id==$systemuser->getId()) return true;
            }
            return false;
        }
        return true;
    }
    
    protected function checkEmail($email, $id=''){
        $systemuser = $this->usermanager->findUserByEmail($email);
        if ($systemuser){
            if (!empty($id)){
                if ($id==$systemuser->getId()) return true;
            }
            return false;
        }
        return true;
    }


    public function getAllSystemusers(){
        return $this->em->getRepository('SystemuserBundle:User')->findAll();
    }
    
    public function getSystemusersByProject($project){
        return $this->em->getRepository('SystemuserBundle:User')->findByProject($project);
    }
    
    public function getAdminsByProject($project){
        return $this->em->getRepository('SystemuserBundle:User')->findByProjectAndRole($project, 'ROLE_ADMIN');
    }
    
    public function getSystemuser($id){
       $systemuser = $this->usermanager->findUserBy(array('id'=>$id));
     
       if (!$systemuser) throw new NotFoundHttpException('No se encuentra el usuario');
       if (!$this->getSystemuserPerm('view', $systemuser)) throw new Exception('No tiene permiso para ver el usuario');  
      
      return $systemuser; 
    }
    
    public function modifySystemuser($systemuser, $roles=array(), $oldroles=array()){
        if (!$this->getSystemuserPerm('modify', $systemuser)) throw new Exception('No tiene permiso para modificar el usuario');
        if (!$this->checkEmail($systemuser->getEmail(), $systemuser->getId()))  throw new Exception('El email ya está en uso');
        if (!$this->checkUsername($systemuser->getUsername(), $systemuser->getId())) throw new Exception('El nombre de usuario ya está en uso');
        
        $this->editRoles($systemuser, $roles, $oldroles);

        
        $this->usermanager->updateUser($systemuser);
    }
    
    protected function editRoles($systemuser, $roles, $oldroles){
        foreach($roles as $role){
            if (!in_array($role, $oldroles)) $systemuser->addRole($role);
        }
        
        foreach($oldroles as $role){
            if (!in_array($role, $roles)) $systemuser->removeRole($role);
        }        
    }
    
    public function insertSystemuser($systemuser, $project, $enable=false, $roles=array()){
        if (!$this->checkEmail($systemuser->getEmail()))  throw new Exception('El email ya está en uso');
        if (!$this->checkUsername($systemuser->getUsername())) throw new Exception('El nombre de usuario ya está en uso');
        
        $systemuser->setCreatedat(new \DateTime());
        $systemuser->setProject($project);
        $systemuser->setEnabled($enable);
        if (!$this->getSystemuserPerm('insert', $systemuser)) throw new Exception('No tiene permiso para insertar el usuario');
        
        $this->editRoles($systemuser, $roles, array());
        
        $this->em->persist($systemuser);
        $this->em->flush();
        
    }
    
    public function deleteSystemuser($systemuser){
         if (!$this->getSystemuserPerm('modify', $systemuser)) throw new Exception('No tiene permiso para eliminar el usuario');
        $this->em->remove($systemuser);
        $this->em->flush();
    }
    
    public function getSystemuserPerm($type, $systemuser=''){
        return true; 
        if ($this->_context->isGranted('ROLE_SUPERADMIN')) return true;
        
        if ($type=='insert'){
            return $this->_context->isGranted('ROLE_ADMIN');
        }
        
        if ($type=='modify') return ($systemuser->getProject() === $this->_context->getToken()->getUser()->getProject() && $this->_context->isGranted('ROLE_ADMIN'));
        
        if ($type=='view') return ($systemuser->getProject() === $this->_context->getToken()->getUser()->getProject());
        
        return false;
    }

}
