<?php

namespace Greetik\SystemuserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SystemuserBundle extends Bundle
{
    public function getParent() {
        return "FOSUserBundle";
    }
}
