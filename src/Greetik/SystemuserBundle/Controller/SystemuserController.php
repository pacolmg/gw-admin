<?php

namespace Greetik\SystemuserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Greetik\SystemuserBundle\Entity\User;
use Greetik\SystemuserBundle\Form\Type\RegistrationFormType;
use Greetik\SystemuserBundle\Form\Type\SystemuserType;
use FOS\UserBundle\Form\Type\ChangePasswordFormType;
use FOS\UserBundle\Form\Type\ResettingFormType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

class SystemuserController extends Controller
{
    
    /**
    * Render all the systemusers of the user
    * 
    * @author Pacolmg
    */
    public function indexAction($id_project='')
     {
        if (!$this->get('security.context')->isGranted('ROLE_SUPERADMIN')){
            if (!$this->get('security.context')->isGranted('ROLE_ADMIN')) throw new AccessDeniedException('No tiene permiso para ver el listado de usuarios');
            $id_project = $this->get('security.context')->getToken()->getUser()->getProject();
        }
        if (!$id_project) throw new AccessDeniedException('No tienes permiso para hacer esta acción');  
        
         
         return $this->render('SystemuserBundle:Systemuser:index.html.twig', array(
                     'data' => $this->get('systemuser.tools')->getSystemusersByProject($id_project),
                    'insertAllow'=>$this->get('systemuser.tools')->getSystemuserPerm('insert')
                 ));
     }

    /**
    * View an individual systemuser, if it doesn't belong to the connected user or doesn't exist launch an exception
    * 
    * @param int $id is received by Get Request
    * @author Pacolmg
    */
     public function viewAction($id, $editForm='', $changepassform='')
     {
        $systemuser = $this->get('systemuser.tools')->getSystemuser($id);
        if (!$editForm) $editForm = $this->createForm(new SystemuserType(), $systemuser);
        
        if (!$changepassform) $changepassform = $this->createForm(new ChangePasswordFormType(\Greetik\SystemuserBundle\Entity\User::class)); 
        return $this->render('SystemuserBundle:Systemuser:view.html.twig', array(
                     'item' => $systemuser,
                     'new_form' => $editForm->createView(),
                     'new_form_pass' => $changepassform->createView(),
                     'modifyAllow'=>$this->get('systemuser.tools')->getSystemuserPerm('modify', $systemuser)
                 ));
     }

    /**
    * Get the data of a new Systemuser by Post and persis it
    * 
    * @param Systemuser $item is received by Post Request
    * @author Pacolmg
    */
     public function insertAction($id_project='')
     {
        $request = $this->getRequest();
        $systemuser = $this->get('fos_user.user_manager')->createUser();
        $newForm = $this->createForm(new RegistrationFormType(), $systemuser);

         if ($request->getMethod() == "POST") {
             $newForm->bind($request);
            
              
             if (!$this->get('security.context')->isGranted('ROLE_SUPERADMIN')){
                 $id_project = $this->get('security.context')->getToken()->getUser()->getProject();
                 $rol = $newForm->get('roles')->getData();
             }else $rol = 'ROLE_ADMIN';
             if (!$id_project) throw new AccessDeniedException('No tienes permiso para hacer esta acción');     
             
             
             if ($newForm->isValid()) {
                 try{
                    $this->get('systemuser.tools')->insertSystemuser($systemuser,$id_project, true, array($rol));
                 }catch(\Exception $e){
                     $newForm->addError(new FormError($e->getMessage()));
                     return $this->render('SystemuserBundle:Systemuser:insert.html.twig',array('id_project'=>$id_project, 'new_form' => $newForm->createView()));
                 }
                 return $this->redirect($this->generateUrl('systemuser_listsystemusers', array('id_project'=>$id_project)));
             }
         }

         return $this->render('SystemuserBundle:Systemuser:insert.html.twig',array('id_project'=>$id_project, 'new_form' => $newForm->createView()));
     }

     public function insertadminAction($id){
         if (!$this->get('security.context')->isGranted('ROLE_SUPERADMIN')) throw $this->createAccessDeniedException('No tienes permiso para insertar un administrador');        
         return $this->insertAction($id);
     }
     
    /**
    * Edit the password of an systemuser
    * .
    * @param int $id is received by Get Request
    * @param Systemuser $item is received by Post Request
    * @author Pacolmg
    */
     public function modifypasswordAction($id){
        $request = $this->getRequest();
        
        $systemuser = $this->get('systemuser.tools')->getSystemuser($id);
        //$auxsystemuser = $this->get('fos_user.user_manager')->createUser();
        
        $editForm = $this->createForm(new ChangePasswordFormType(\Greetik\SystemuserBundle\Entity\User::class)); 
        
        if ($this->getRequest()->getMethod() == "POST") {
            try{
                $editForm->bind($request);
            }catch(\Exception $e){
                throw new Exception($e->getMessage());                
            }
            if ($editForm->isValid()) {
                
                
                $systemuser->setPlainPassword($editForm->get('plainPassword')->getData());
                $this->get('systemuser.tools')->modifySystemuser($systemuser);
                return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id'=>$id)));
            }
         }
         return $this->viewAction($id, '', $editForm);
     }

     
    /**
    * Edit the data of an systemuser
    * 
    * @param int $id is received by Get Request
    * @param Systemuser $item is received by Post Request
    * @author Pacolmg
    */
     public function modifyAction($id){
        $request = $this->getRequest();

        $systemuser = $this->get('systemuser.tools')->getSystemuser($id);
        $oldroles = $systemuser->getRoles();
        $editForm = $this->createForm(new SystemuserType(), $systemuser);
        
        
        if ($request->getMethod() == "POST") {
            $editForm->bind($request);
            $rol = $editForm->get('roles')->getData();
            
            if ($editForm->isValid()) {
                try{
                    $this->get('systemuser.tools')->modifySystemuser($systemuser, array($rol), $oldroles);
                }catch(\Exception $e){
                    $editForm->addError(new FormError($e->getMessage()));
                    return $this->viewAction($id, $editForm);
                }
                return $this->redirect($this->generateUrl('systemuser_viewsystemuser', array('id'=>$id)));
            }
         }
         return $this->viewAction($id, $editForm);
     }


}
